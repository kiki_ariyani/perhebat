<?php
class Activity extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('activity');
	}
	
	function get_business_activity($where = NULL,$type = NULL){
		$this->db->select('person.*,activity.*');
		$this->db->from('business_activity as activity');
		$this->db->join('business_person as person','person.business_id=activity.business_id',$type);
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('activity.business_id','DESC');
		return $this->db->get();
	}
	
	function get_business_activity_for_excel($where = NULL,$type = NULL){
		$this->db->select('activity.business_id,business.business_name,person.name as person_name,person.category,business.registration_no,business.email_web,person.state_id,state.name');
		$this->db->from('business_activity as activity');
		$this->db->join('business_person as person','person.business_id=activity.business_id',$type);
		$this->db->join('business as business','person.business_id=business.business_id',$type);
		$this->db->join('state','person.state_id=state.state_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('business_id','DESC');
		$this->db->group_by('business_id');
		$query = $this->db->get()->result_array();
		$result = array();
		$categories = unserialize(BUSINESS_PERSON_CATEGORY);
		foreach($query as $row){
			if($row['category'] > 0){
				$row['category'] = $categories[$row['category']-1]['name'];
			}else{
				$row['category'] = "";
			}
			$activity_arr = $this->get_activity_name(array('business_id' => $row['business_id']));
			$row['activities'] = $activity_arr;
			unset($row['business_id']);
			unset($row['state_id']);
			$result[] = $row;
		}
		return $result;
	}
	
	function get_activity_name($where = NULL,$type = NULL){
		$this->db->select('business_activity.*,activity.name');
		$this->db->from('business_activity');
		$this->db->join('activity','business_activity.activity_id = activity.activity_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$query = $this->db->get();
		$result = $query->result_array();
		
		$value = "";
		for($i=0;$i<count($result);$i++){
			$value .= $result[$i]['name'].", ";
		}
		return $value;
	}
}
