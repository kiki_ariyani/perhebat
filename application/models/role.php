<?php
class Role extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('role');
	}
	
	function add($data){
		$this->db->insert('role',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('role_id',$id);
		$this->db->update('role',$data);
	}
	
	function delete($id){
		$this->db->where('role_id',$id);
		$this->db->delete('role');
		return true;
	}
	
	function get_role_setting($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('role_setting');
	}	
	
	function add_role_setting($data){
		$this->db->insert('role_setting',$data);
	}
	
	function delete_role_setting($where){
		$this->db->where($where);
		$this->db->delete('role_setting');
		return true;
	}
}
