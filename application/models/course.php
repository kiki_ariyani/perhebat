<?php
class Course extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		$this->db->select('courses.*,trainer.name as trainer_name,trainer.code');
		$this->db->from('courses');
		$this->db->join('trainer','trainer.trainer_id = courses.trainer_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function edit($id, $data){
		$this->db->where('courses_id', $id);
		$this->db->update('courses', $data);
		return true;
	}
	
	function add($data){
		$this->db->insert('courses',$data);
		return $this->db->insert_id();
	}
	
	function delete($id){
		$this->db->where('courses_id',$id);
		$this->db->delete('courses');
		return true;
	}
}
