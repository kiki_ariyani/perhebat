<?php
class Business_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('business');
	}
	
	function get_with_person($where = NULL,$type = NULL){
		$this->db->select('business.*,person.name as person_name, person.category,person.state_id,user.user_id');
		$this->db->from('business');
		$this->db->join('business_person as person','person.business_id=business.business_id',$type);
		$this->db->join('user as user','user.business_id=business.business_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('business_id','DESC');
		return $this->db->get();
	}
	
	function edit($id, $data){
		$this->db->where('business_id', $id);
		$this->db->update('business', $data);
		return true;
	}
		
	function get_business_activity($where){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('business_activity');
	}
	
	function add_business_activity($data){
		$this->db->insert('business_activity', $data);
		return true;
	}
	
	function delete_business_activity($business_id){
		$this->db->delete('business_activity', array('business_id' => $business_id));
	}
	
	function add($data){
		return $this->db->insert('business',$data);
	}
	
	function delete($business_id){
		$this->db->where('business_id',$business_id);
		$this->db->delete('business');
	}
	
	function search($category='',$state='',$activities=array(0),$financing='',$keyword='',$work_status='',$trainer=''){
		$select = 'business.business_id,business.business_name,business.registration_no,business.email_web,person.name as person_name, person.category,person.state_id';
		if($activities[0] != 0){
			$select .= ',activity.activity_id';
		}
		$this->db->select($select);
		$this->db->from('business');
		$this->db->join('business_person as person','person.business_id=business.business_id');
		if($activities[0] != 0){
			$this->db->join('business_activity as activity','activity.business_id=business.business_id');
			foreach($activities as $activity){
				if($activity != 0){
					if($activity == $activities[0]){
						$activities_where = "activity.activity_id=$activity";
					}else{
						$activities_where .= " OR activity.activity_id=$activity";
					}
				}
			}
			$this->db->where('('.$activities_where.')');
		}
		$this->db->join('business_courses as bus_courses','bus_courses.business_id=business.business_id', 'LEFT');
		$this->db->join('courses','courses.courses_id=bus_courses.courses_id', 'LEFT');
		if(!empty($trainer)){
			$this->db->where('courses.trainer_id',$trainer);
		}
		if(!empty($category)){
			$this->db->where('person.category',$category);
		}
		if(!empty($state)){
			$this->db->where('person.state_id',$state);
		}
		if(!empty($financing)){
			$this->db->where('business.financing_by',$financing);
		}
		if(!empty($work_status)){
			$this->db->where('business.work_status',$work_status);
		}
		if(!empty($keyword)){
			$keyword_where = "(person.name LIKE '%$keyword%' OR person.ic_no LIKE '%$keyword%' OR person.age LIKE '%$keyword%' OR person.army_no LIKE '%$keyword%' 
							  OR person.address LIKE '%$keyword%' OR person.telp_no LIKE '%$keyword%' OR person.geo_location LIKE '%$keyword%'
							  OR business.registration_no LIKE '%$keyword%' OR business.registration_date LIKE '%$keyword%' OR business.business_name LIKE '%$keyword%' 
							  OR business.business_address LIKE '%$keyword%' OR business.email_web LIKE '%$keyword%' OR business.main_product LIKE '%$keyword%' 
							  OR business.account_type LIKE '%$keyword%')";
			$this->db->where($keyword_where);
		}
		$this->db->group_by('business.business_id');
		$this->db->order_by('business.business_id','DESC');
		$business = $this->db->get()->result_array();
		$this->load->model('business_courses');
		for($i=0;$i<count($business);$i++){
			$business[$i]['trainers'] = "";
			unset($bus_courses);
			$bus_courses = $this->business_courses->get($business[$i]['business_id'],'YES')->result_array();
			foreach($bus_courses as $course){
				$business[$i]['trainers'] .= "- ".$course['trainer_name']."<br/> ";
			}
		}
		return $business;		
	}
	
	function get_for_excel($where = NULL,$type = NULL,$filter = NULL){
		$this->db->select('business.business_id,business.business_name,person.name as person_name,person.category,business.registration_no,business.email_web,person.state_id,state.name,business.work_status,person.gender,business.financing_by');
		$this->db->from('business');
		$this->db->join('business_person as person','person.business_id=business.business_id',$type);
		$this->db->join('state','person.state_id=state.state_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('business_id','DESC');
		$query = $this->db->get()->result_array();
		$result = array();
		$categories = unserialize(BUSINESS_PERSON_CATEGORY);
		array_push($categories,array('category_id' => 0, 'name' => ''));
		foreach($query as $row){
			if($row['category'] > 0){
				$row['category'] = $categories[$row['category']-1]['name'];
			}else{
				$row['category'] = "";
			}
			unset($row['business_id']);
			unset($row['state_id']);
			if($filter == "work_status"){
				unset($row['gender']);
				unset($row['financing_by']);
			}else if($filter == "gender"){
				unset($row['work_status']);
				unset($row['financing_by']);
			}else if($filter == "finance"){
				unset($row['work_status']);
				unset($row['gender']);
			}else if($filter == ""){
				unset($row['work_status']);
				unset($row['gender']);
				unset($row['financing_by']);
			}
			$result[] = $row;
		}
		return $result;
	}

	function get_row_business_activity($where = NULL){
		$this->db->select('activity.name');
		$this->db->from('business_activity');
		$this->db->join('activity','business_activity.activity_id = activity.activity_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$result = $this->db->get()->result_array();
		foreach($result as $key=>$value){
			$result[$key] = $value['name'];
		}

		$response = implode(",",$result);
		return $response;
	}

	/*@author: Kiki
	@Desc : Temporary function for manual export all user data to excel*/
	function get_for_excel_manual(){
		$this->db->select('business_person.*, business.*, state.name as state_name,category.name as category_name');
		$this->db->from('business_person');
		$this->db->join('business','business_person.business_id = business.business_id');
		$this->db->join('state','business_person.state_id = state.state_id');
		$this->db->join('category','business_person.category = category.category_id','LEFT');
		$result = $this->db->get()->result_array();

		foreach($result as $key=>$value){
			$result[$key]['activity'] = $this->get_row_business_activity(array('business_activity.business_id' => $value['business_id']));
			$result[$key]['courses'] = $this->get_row_business_courses(array('business_courses.business_id' => $value['business_id']));
		}
		return $result;
	}

	function get_row_business_courses($where = NULL){
		$this->db->select('courses.name');
		$this->db->from('business_courses');
		$this->db->join('courses','business_courses.courses_id = courses.courses_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$result = $this->db->get()->result_array();
		foreach($result as $key=>$value){
			$result[$key] = $value['name'];
		}

		$response = implode(",",$result);
		return $response;
	}
}
