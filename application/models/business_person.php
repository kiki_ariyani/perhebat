<?php
class Business_person extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('business_person');
	}
	
	function get_current_age($where = NULL){
		$year = date("Y");
		$this->db->select("*,{$year} - CONCAT('19',SUBSTRING(ic_no,1,2)) AS current_age ", FALSE);
		if($where != NULL){
			$this->db->having($where);
		}
		return $this->db->get('business_person');
	}
	
	function get_current_age_for_excel($where = NULL){
		$year = date("Y");
		$this->db->select("business.business_id,business.business_name,person.name as person_name,person.category,business.registration_no,business.email_web,person.state_id,state.name,{$year} - CONCAT('19',SUBSTRING(ic_no,1,2)) AS current_age ", FALSE);
		$this->db->from('business_person as person');
		$this->db->join('business as business','person.business_id=business.business_id');
		$this->db->join('state','person.state_id=state.state_id');
		if($where != NULL){
			$this->db->having($where);
		}
		$this->db->order_by('business_id','DESC');
		$query = $this->db->get()->result_array();
		$result = array();
		$categories = unserialize(BUSINESS_PERSON_CATEGORY);
		foreach($query as $row){
			if($row['category'] > 0){
				$row['category'] = $categories[$row['category']-1]['name'];
			}else{
				$row['category'] = "";
			}
			unset($row['business_id']);
			unset($row['state_id']);
			$result[] = $row;
		}
		return $result;
	}
	
	function edit($id, $data){
		$this->db->where('business_id', $id);
		$this->db->update('business_person', $data);
		return true;
	}
	
	function get_state($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('state');
	}
	
	function add($data){
		$this->db->insert('business_person',$data);
		return $this->db->insert_id();
	}
	
	function delete($business_id){
		$this->db->where('business_id',$business_id);
		$this->db->delete('business_person');
	}
	
	function check_bin_al($person_name){
		$person_name = strtolower($person_name);
		if(strpos($person_name,'bin') !== false){
			return 'bin';
		}elseif(strpos($person_name,'a/l') !== false){
			return 'al';
		}else{
			return 'no';
		}
	}
	
	function person_gender_religion($person_name){
		if($this->check_bin_al($person_name) == 'bin'){
			$result = array('gender' => 1, 'religion' => 1);
		}elseif($this->check_bin_al($person_name) == 'al'){
			$result = array('gender' => 1, 'religion' => 0);
		}else{
			$result = array('gender' => 0, 'religion' => 0);
		}
		return $result;
	}
	
}
