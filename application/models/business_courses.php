<?php
class Business_courses extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($business_id = NULL, $search_type = NULL){
		$this->db->select('courses.*,bc.business_id,trainer.name as trainer_name, trainer.code as code');
		$this->db->from('business_courses as bc');
		$this->db->join('courses','courses.courses_id=bc.courses_id');
		$this->db->join('trainer','courses.trainer_id=trainer.trainer_id');
		if($business_id != NULL){
			$this->db->where('bc.business_id',$business_id);
		}
		if($search_type == 'YES'){
			$this->db->group_by('courses.trainer_id');
		}
		return $this->db->get();
	}
	
	function get_all($where = NULL,$group_by = 0){
		$this->db->select('courses.*,bc.business_id,trainer.name as trainer_name, business_person.state_id,business_person.category,business_person.name as person_name,business_person.business_id');
		$this->db->from('business_courses as bc');
		$this->db->join('courses','courses.courses_id=bc.courses_id');
		$this->db->join('trainer','courses.trainer_id=trainer.trainer_id');
		$this->db->join('business_person','bc.business_id=business_person.business_id');
		if($where != NULL){
			$this->db->where($where);
		}
		if($group_by == 1){
			$this->db->group_by('bc.business_id');
		}
		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('business_courses',$data);
	}
	
	function delete($business_id){
		$this->db->where('business_id',$business_id);
		$this->db->delete('business_courses');
	}
	
	function get_business_course_for_excel($where = NULL,$type = NULL){
		$this->db->select('courses.business_id,courses.courses_id,business.business_name,person.name as person_name,person.category,business.registration_no,business.email_web,person.state_id,state.name');
		$this->db->from('business_courses as courses');
		$this->db->join('business_person as person','person.business_id=courses.business_id',$type);
		$this->db->join('business as business','person.business_id=business.business_id',$type);
		$this->db->join('state','person.state_id=state.state_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('business_id','DESC');
		$this->db->group_by('business_id');
		$query = $this->db->get()->result_array();
		$result = array();
		$categories = unserialize(BUSINESS_PERSON_CATEGORY);
		foreach($query as $row){
			if($row['category'] > 0){
				$row['category'] = $categories[$row['category']-1]['name'];
			}else{
				$row['category'] = "";
			}
			$courses_arr = $this->get_trainer_name($row['business_id']);
			$row['courses'] = $courses_arr;
			unset($row['business_id']);
			unset($row['state_id']);
			unset($row['courses_id']);
			$result[] = $row;
		}
		return $result;
	}
	
	function get_trainer_name($business_id = NULL){
		$courses = $this->get($business_id)->result_array();
		$value = "";
		for($i=0;$i<count($courses);$i++){
			if($courses[$i] == end($courses)){
				$value .= $courses[$i]['code'];
			}else{
				$value .= $courses[$i]['code'].", ";
			}
		}
		return $value;
	}

	function get_provider_amount_for_excel($req_states = array(0),$req_provider = 0, $multi_provider = 0){
		$this->load->model('business_model');
		$this->db->select('courses.courses_id,courses.name as courses_name, courses.cost,courses.trainer_id,bc.business_id,trainer.name as trainer_name,business_person.name as person_name,business_person.business_id,'.($multi_provider == 0 ? 'state.name as state_name,' : '').' business.business_name,business.registration_no, business.email_web');
		$this->db->from('business_courses as bc');
		$this->db->join('courses','courses.courses_id=bc.courses_id');
		$this->db->join('trainer','courses.trainer_id=trainer.trainer_id');
		$this->db->join('business_person','bc.business_id=business_person.business_id');
		$this->db->join('business','business_person.business_id = business.business_id');
		if($multi_provider == 0){
			$this->db->join('state','business_person.state_id=state.state_id');
		}
		
		if (!in_array(0, $req_states)) {
			$where_state = '(';
			for($i=0;$i<count($req_states);$i++){
				$where_state .= 'business_person.state_id = '.$req_states[$i].' '.($i == (count($req_states)-1) ? '' : 'OR ');
			}
			$where_state .= ')';
			$this->db->where($where_state);
		}

		if($multi_provider == 0){
			if($req_provider != 0){
				$this->db->where('courses.trainer_id',$req_provider);
			}
		}else{
			if (!in_array(0, $req_provider)) {
				$where_provider = '(';
				for($i=0;$i<count($req_provider);$i++){
					$where_provider .= 'courses.trainer_id = '.$req_provider[$i].' '.($i == (count($req_provider)-1) ? '' : 'OR ');
				}
				$where_provider .= ')';
				$this->db->where($where_provider);
			}			
		}

		$this->db->group_by('bc.business_id');
		$result = $this->db->get()->result_array();
		foreach($result as $key=>$value){
			$result[$key]['courses'] = $this->business_model->get_row_business_courses(array('business_courses.business_id' => $value['business_id']));
		}

		return $result;
	}
	/*temporary function, just for filter data*/
	function get_person_courses($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('business_courses');
	}
}
