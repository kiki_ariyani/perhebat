<?php
class Site_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function login($username, $password) {
		$this->load->model('user');
		$data = $this->user->get(array('email' => $username))->row_array();
		$where	= array('email' 			=> $username, 
						//'password' 			=> md5($password), 
						'password' 			=> $this->get_hash($data['business_id'],$username,$password),
						'activation_status' => ACTIVE_ACCOUNT,
						'role_id' => COMPANY_ADMIN);
		$query 	= $this->db->get_where('user', $where)->row_array();
		return $query;
	}
	
	function get_hash($business_id,$username, $password) {
		return md5($business_id.";".$username.':'.$password);
	}
	
	function insert_user_upt($data){
		$this->db->insert('user_upt',$data);
	}

}