<?php
class Classified extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where=null, $order_by=null, $limit=null, $offset=0){
		$this->db->select('*');
		$this->db->from('user_classified');
		if($where != NULL){
			$this->db->where($where);
		}
		if ($order_by != NULL)
			$this->db->order_by($order_by);
		return $this->db->get();
	}
	
	function edit($id, $data){
		$this->db->where('classified_id', $id);
		$this->db->update('user_classified', $data);
		return true;
	}
	
	function add($data){
		$this->db->insert('user_classified',$data);
		return $this->db->insert_id();
	}
	
	function delete($id){
		$this->db->where('classified_id',$id);
		$this->db->delete('user_classified');
		return true;
	}
	
	function get_notexpire($where = null){
		$this->db->select('*');
		$this->db->from('user_classified');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where("end_date > NOW()");
		return $this->db->get();
	}
	
	function get_classified_with_person(){
		$this->db->select('person.name,class.classified_id,class.type,class.title,class.description,class.contact_person,class.contact_no');
		$this->db->from('user_classified as class');
		$this->db->join('business_person as person','person.business_id=class.user_id');
		$this->db->where('class.end_date > NOW()');
		return $this->db->get()->result_array();
	}
}
