<?php
class Mapping extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function find_search($keyword, $state){
		$this->db->select('b.*, c.*, t.name as state_name');
		$this->db->from('business_person as b');
		$this->db->join('business as c', 'c.business_id=b.business_id');
		$this->db->join('state as t', 't.state_id=b.state_id');
		if(!empty($keyword)){
			$where = "( c.business_name LIKE '%$keyword%' OR b.name LIKE '%$keyword%')";
			$this->db->where($where);
		} 
		if(!empty($state)){
			$this->db->where('b.state_id',$state);
		}
		$this->db->order_by('b.business_id', 'ASC');
		return $this->db->get()->result();
	}
	
	function get_state($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('state');
	}
	
	function locators_by_activity($keyword, $state){
		$this->db->select('activity.*, person.name, person.category, person.geo_location, person.ic_no, person.state_id, person.telp_no, business.business_address as address, business.business_name, business.email_web, state.name as state_name, act.name as activity_name');
		$this->db->from('business_activity as activity');
		$this->db->join('business_person as person','person.business_id = activity.business_id');
		$this->db->join('business', 'activity.business_id=business.business_id');
		$this->db->join('state', 'state.state_id=person.state_id');
		$this->db->join('activity as act', 'activity.activity_id=act.activity_id');
		if(!empty($keyword)){
			$where = "( business.business_name LIKE '%$keyword%' OR person.name LIKE '%$keyword%')";
			$this->db->where($where);
		} 
		if(!empty($state)){
			$this->db->where('person.state_id',$state);
		}
		$this->db->order_by('person.business_id', 'ASC');
		return $this->db->get()->result();
		//$this->db->get();
		//echo $this->db->last_query();
	}
	
	function edit_add_address($id, $data){
		$this->db->where('id', $id);
		$this->db->update('additional_address', $data);
	}
	
	function locators_additional_address($where = NULL){
		$this->db->select('*');
		$this->db->from('additional_address');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get()->result();
	}

}
