<?php
class Trainer extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('trainer');
	}
	
	function edit($id, $data){
		$this->db->where('trainer_id', $id);
		$this->db->update('trainer', $data);
		return true;
	}
	
	function add($data){
		$this->db->insert('trainer',$data);
		return $this->db->insert_id();
	}
	
	function delete($id){
		$this->db->where('trainer_id',$id);
		$this->db->delete('trainer');
		return true;
	}
}
