<?php
class Business_comment extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('business_comment');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('comment_id','ASC');
		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('business_comment',$data);
		return $this->db->insert_id();
	}
	
	function delete($business_id){
		$this->db->where('business_id',$business_id);
		$this->db->delete('business_comment');
		return true;
	}
}
