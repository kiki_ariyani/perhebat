<?php
class Menu extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('menu');
	}
	
	function get_menu_access($menu_id,$where = NULL){
		$this->load->model('role');
		$roles  = $this->role->get($where)->result_array();
		$access = array();
		foreach($roles as $role){
			$query = $this->role->get_role_setting(array('role_id'=>$role['role_id'],'menu_id'=>$menu_id))->row_array();
			if(count($query) > 0){
				$access[] = $query;
			}else{
				$access[] = array('view'=>0,'create'=>0,'edit'=>0);
			}
		}
		return $access;
	}
	
	function get_sorted_menu($where = NULL, $where_type = NULL){
		$menus = $this->db->get_where('menu',$where)->result_array();
		for($i=0;$i<count($menus);$i++){
			$menus[$i]['childs'] = $this->get_sorted_menu(array('parent_id'=>$menus[$i]['menu_id']),$where_type);
			$menus[$i]['access'] = $this->get_menu_access($menus[$i]['menu_id'],$where_type);
		}
		return $menus;
	}
	
	function delete($where){
		$this->db->where($where);
		return $this->db->delete('menu');
	}
	
	function edit($id, $data){
		$this->db->where('menu_id', $id);
		$this->db->update('menu', $data);
		return true;
	}
	
	function add($data){
		$this->db->insert('menu',$data);
		return $this->db->insert_id();
	}
	
	function get_menu_parent(){
		$list = $this->get(array('parent_id' => 0))->result_array();
		$menus = array();
		foreach($list as $item){
			$item_container = array (
					"menu_id" => $item['menu_id'],
					"parent_id" => $item['parent_id'],
					"name" => $item['name'],
					"description" => $item['description'],
					"url"	=> $item['url'],
 					"child"	=> $this->get_parent_child($item['menu_id']),
			);
			$menus[] = $item_container;
			unset($item_container);
		}
		
		return $menus;
	}
	
	function get_parent_child($menu_id){
		$menus = array();
		$result = $this->get(array('parent_id' => $menu_id))->result_array();
		
		if($result){
			foreach($result as $item){
				$item_container = array (
					"parent_id" => $item['parent_id'],
					"menu_id" => $item['menu_id'],
					"name" => $item['name'],
					"description" => $item['description'],
					"url"	=> $item['url'],
				);
				$menus[] = $item_container;
				unset($item_container);
			}
		}
		
		return $menus;
	}
	
	function list_menu_by_role($role_id,$parent_id = 0){
		$this->db->select('role.*,menu.*');
		$this->db->from('role_setting as role');
		$this->db->join('menu','role.menu_id = menu.menu_id');
		$this->db->where('menu.parent_id = '.$parent_id.' AND (role.view = 1 OR role.create = 1 OR role.edit = 1) AND role.role_id ='.$role_id);
		$query = $this->db->get();
		return $query->result_array();
		
	}
	
	function get_menu_by_role($role_id){
		$list = $this->list_menu_by_role($role_id);
		
		$menus = array();
		foreach($list as $item){
			$item_container = array (
					"menu_id" => $item['menu_id'],
					"parent_id" => $item['parent_id'],
					"name" => $item['name'],
					"description" => $item['description'],
					"url"	=> $item['url'],
 					"child"	=> $this->list_menu_by_role($role_id,$item['menu_id']),
			);
			$menus[] = $item_container;
			unset($item_container);
		}
		
		return $menus;
	}

}
