<?php
class Highlight extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('highlight');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function edit($id, $data){
		$this->db->where('highlight_id', $id);
		$this->db->update('highlight', $data);
		return true;
	}
	
	function add($data){
		$this->db->insert('highlight',$data);
		return $this->db->insert_id();
	}
	
	function delete($id){
		$this->db->where('highlight_id',$id);
		$this->db->delete('highlight');
		return true;
	}
}
