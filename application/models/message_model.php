<?php
class Message_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get_name($user_id){
		$this->db->select('user.*,person.name as person_name, person.category,person.state_id,user.user_id');
		$this->db->from('user');
		$this->db->join('business_person as person','person.business_id=user.business_id');
		$this->db->where('user.user_id',$user_id);
		return $this->db->get();
	}
	
	function get($where=null, $order_by=null, $list=NULL){
		$this->db->select('message.*,person.name as person_name');
		$this->db->from('message');
		if($list == 'inbox'){
			$this->db->join('user','user.user_id=message.from_user_id');
		}else{
			$this->db->join('user','user.user_id=message.to_user_id');
		}
		$this->db->join('business_person as person','person.business_id=user.business_id');
		if($where != NULL){
			$this->db->where($where);
		}
		if ($order_by != NULL)
			$this->db->order_by($order_by);
		return $this->db->get();
	}
	
	function edit($id, $data){
		$this->db->where('message_id', $id);
		$this->db->update('message', $data);
		return true;
	}
	
	function delete($id){
		$this->db->where('message_id',$id);
		$this->db->delete('message');
		return true;
	}
	
	function send_act($data){
		$this->db->insert('message',$data);
		return $this->db->insert_id();
	}
}
