<?php
class User extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('user');
	}
	
	function get_hash($business_id,$username, $password) {
		return md5($business_id.";".$username.':'.$password);
	}
	
	function auth($username, $password) {
		$data = $this->get(array('email' => $username))->row_array();
		$query = $this->db->get_where('user', array(
			'email' => $username,
			'password' => $this->get_hash($data['business_id'],$username, $password),
		));
		return $query->row_array();
	}
	
	function add($data){
		$data['password'] = $this->get_hash($data['business_id'],$data['email'],$data['password']);
		$this->db->insert('user',$data);
		return $this->db->insert_id();
	}
	
	function delete($business_id){
		$this->db->where('business_id',$business_id);
		$this->db->delete('user');
	}
	
	function add_admin($data){
		$data['password'] = $this->get_hash($data['business_id'],$data['email'],$data['password']);
		return $this->db->insert('user',$data);
	}
	
	function edit_admin($id,$data){
		$data['password'] = $this->get_hash($data['business_id'],$data['email'],$data['password']);
		$this->db->where('user_id',$id);
		return $this->db->update('user',$data);
	}
	
	function delete_admin($user_id){
		$this->db->where('user_id',$user_id);
		return $this->db->delete('user');
	}

	function change_password($old_password, $new_password, $session){
		$user = $this->session->userdata($session);
		if($this->get_hash($user['business_id'],$user['password'], $old_password)) {
			$new_password = $this->get_hash($user['business_id'],$user['email'],$new_password);
			$user_data = array("password" => $new_password);
			
			$this->db->where('user_id', $user['user_id']);
			
			return $this->db->update('user', $user_data);
		}
		return false;
	}
}
