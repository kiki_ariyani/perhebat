<?php
class Article extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where=null, $order_by=null, $limit=null, $offset=0){
		$this->db->select('*');
		$this->db->from('article');
		if($where != NULL){
			$this->db->where($where);
		}
		if ($order_by != NULL)
			$this->db->order_by($order_by);
		return $this->db->get();
	}
	
	function edit($id, $data){
		$this->db->where('article_id', $id);
		$this->db->update('article', $data);
		return true;
	}
	
	function add($data){
		$this->db->insert('article',$data);
		return $this->db->insert_id();
	}
	
	function delete($id){
		$this->db->where('article_id',$id);
		$this->db->delete('article');
		return true;
	}
}
