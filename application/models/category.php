<?php
class Category extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('category');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function edit($id, $data){
		$this->db->where('category_id', $id);
		$this->db->update('category', $data);
		return true;
	}
	
	function add($data){
		$this->db->insert('category',$data);
		return $this->db->insert_id();
	}
	
	function delete($id){
		$this->db->where('category_id',$id);
		$this->db->delete('category');
		return true;
	}
}
