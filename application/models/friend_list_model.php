<?php
class friend_list_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('friend_list');
	}
	
	function add($data){
		$add_data = $this->db->insert('friend_list',$data);
		return $this->db->insert_id();
	}
	
	function edit($id, $data){
		$this->db->where('friend_list_id', $id);
		$this->db->update('friend_list', $data);
		return true;
	}
	
	function delete($where = NULL){
		$this->db->where($where);
		$this->db->delete('friend_list');
	}
	
	function get_name($where = NULL){
		$this->db->select('business_person.name');
		$this->db->from('user');
		$this->db->join('business_person','user.business_id = business_person.business_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}
