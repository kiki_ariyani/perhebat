<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <title>PERHEBAT |</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		{{metas}}

		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.dataTables.js"></script>	
		<!--<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>-->
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/vendor/zepto.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/foundation.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/switch.js"></script>
		<script>var base_url = <?php echo json_encode(base_url());?>;</script>
		{{scripts}}

		<!--<link rel="icon" href="<?= base_url() ?>favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="<?= base_url() ?>favicon.ico" type="image/x-icon" />
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" media="screen,projection" href="<?= base_url() ?>/assets/css/demo_table.css" type="text/css" />
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/redmond/jquery-ui-1.8.17.custom.css" type="text/css" /> 
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<?= base_url() ?>/assets/css/style.css" /> -->		
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/normalize.css" />
	  	<link rel="stylesheet" href="<?= base_url() ?>/assets/css/demo_table.css" />
	  	<link rel="stylesheet" href="<?= base_url() ?>/assets/css/redmond/jquery-ui-1.8.17.custom.css" />
	  	<link rel="stylesheet" href="<?= base_url() ?>/assets/css/foundation-front.css" />
	  	<link rel="stylesheet" media="screen,projection" type="text/css" href="<?= base_url() ?>/assets/css/reset.css" /> <!-- RESET -->
	  	<link rel="stylesheet" media="screen,projection" type="text/css" href="<?= base_url() ?>/assets/css/front.css" /> <!-- MAIN STYLE SHEET -->
	 	<link rel="stylesheet" media="screen,projection" type="text/css" href="<?= base_url() ?>/assets/css/left.css" title="2col" />
		<style type="text/css">

		/*  THIS IS JUST TO GET THE GRID TO SHOW UP.  YOU DONT NEED THIS IN YOUR CODE */

		#maincontent .col {
			/*background: #ccc;
			background: rgba(204, 204, 204, 0.85);*/
		}

		</style>
  

  	<script src="<?= base_url(); ?>assets/js/vendor/custom.modernizr.js"></script>
  	{{styles}}
</head>
<body>
	<div id="head" class="header">
		{{head}}
	</div>
	<div id="content" class="row">
		<div class="large-3 columns">
		<!-- Aside (Left Column) -->	
		<div id="aside" class="asidered">
			{{left}}
		</div>
		<!-- /aside -->
		<div id="warper" class="large-9 columns">
		{{content}}
		</div>
	</div>
	<?php $sess = $this->session->userdata('perniagaan_user');
		if($sess){?>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/site/message.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/site/friend_list.js"></script>
	<?php } ?>
</body>  	
</html>
