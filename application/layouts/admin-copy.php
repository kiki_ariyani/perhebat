<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Perniagaan | {{title}}</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		{{metas}}
		
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-ui.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.dataTables.js"></script>	
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>	
		{{scripts}}
		
		<!--<link rel="icon" href="<?= base_url() ?>favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="<?= base_url() ?>favicon.ico" type="image/x-icon" />-->
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" media="screen,projection" href="<?= base_url() ?>/assets/css/demo_table.css" type="text/css" />
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/redmond/jquery-ui-1.8.17.custom.css" type="text/css" />
		<link rel="stylesheet" media="screen,projection" type="text/css" href="<?= base_url() ?>/assets/css/style-admin.css" />		
		
		{{styles}}
	</head>
	<body>
		<div id="container">
			<div id="head">
				{{head}}
			</div>
			<div class="clear"></div>
			<div id="content">
				{{content}}
			</div>
			<div id="footer">
				{{footer}}
			</div>
		</div>
	</body>
</html>