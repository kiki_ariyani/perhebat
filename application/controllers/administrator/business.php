<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Business extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "business /";
		$this->layout = "admin";
		$this->load->model(array('business_person','business_model','business_courses','activity','user','course','menu','role'));
	}

	public function index() {
		$this->parts['p_title'] = "";
		$menu = $this->menu->get(array('url' => "administrator/business"))->row_array();
		$this->scripts = array('jquery.dataTables');
		$data = array('business' 	=> $this->business_model->get_with_person()->result_array(),
					  'categories'	=> unserialize(BUSINESS_PERSON_CATEGORY),
					  'states' 		=> $this->business_person->get_state()->result_array(),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array());
		$this->load->view(ADMIN_DIR.'business/index',$data);
	}
	
	function add($form = "akaun",$business_id = NULL){
	//	parent::__construct("business_list");
		$this->parts['p_title'] = "add /";
		$this->scripts = array('site/general');
		if($business_id == NULL){
			$data = array('states' 			=> $this->business_person->get_state()->result_array(),
						  'categories'		=> unserialize(BUSINESS_PERSON_CATEGORY),
						  'activities'  	=> $this->activity->get()->result_array(),
						  'financing_bys'	=> unserialize(BUSINESS_FINANCING_BY),
						  'gender'			=> unserialize(GENDER),
						  'race'			=> unserialize(RACE),
						  'religion'		=> unserialize(RELIGION),
						  'work_status'		=> unserialize(WORK_STATUS),
						  'courses' 		=> $this->course->get()->result_array(),
						  'mode' 			=> 'ADD',
						  'detail'			=> "");
			$this->load->view(ADMIN_DIR.'business/form_akaun',$data);
		}else{
			$this->load_view($form,$business_id,'ADD');
		}
	}
	
	function edit($form,$business_id) {
	//	parent::__construct("business_list");
		$this->parts['p_title'] = "edit /";
		$this->scripts = array('site/general');
		$this->load_view($form,$business_id,'EDIT');
	}
	
	function view($form,$business_id) {
	//	parent::__construct("business_list");
		$this->parts['p_title'] = "view /";
		$this->scripts = array('site/general');
		$this->load_view($form,$business_id,'VIEW');
	}
	
	function load_view($form,$business_id,$mode){
		$detail 	= $this->business_person->get(array('business_id' => $business_id))->row_array();
		if($form == "akaun"){
			$data = array('detail' 			=> $detail,
						'user'			=> $this->user->get(array('business_id' => $business_id))->row_array(),
						 'mode'			=> $mode,
					  );
			$this->load->view(ADMIN_DIR.'business/form_akaun',$data);
		}else if($form == "maklumat_diri"){
			if($detail['gender'] == 0 || $detail['religion'] == 0){
				$result 				= $this->business_person->person_gender_religion($detail['name']);
				$detail['gender'] 		= $result['gender'];
				$detail['religion'] 	= $result['religion'];
			}
			$detail['check_name']	= $this->business_person->check_bin_al($detail['name']);
			$data = array('detail' 			=> $detail,
						  'states' 			=> $this->business_person->get_state()->result_array(),
						  'categories'		=> unserialize(BUSINESS_PERSON_CATEGORY),
						  'gender'			=> unserialize(GENDER),
						  'race'			=> unserialize(RACE),
						  'religion'		=> unserialize(RELIGION),
						  'mode'			=> $mode,
					  );
			$this->load->view(ADMIN_DIR.'business/form_maklumat_diri',$data);
		}else if($form == "maklumat_perniagaan"){
			$data = array('detail' 			=> $detail,
						'detail_business'	=> $this->business_model->get(array('business_id' => $business_id))->row_array(),
						'bus_activities' 	=> $this->business_model->get_business_activity(array('business_id' => $business_id))->result_array(),
						'activities'  		=> $this->activity->get()->result_array(),
						'financing_bys'		=> unserialize(BUSINESS_FINANCING_BY),
						'work_status'		=> unserialize(WORK_STATUS),
						'mode'				=> $mode,
					  );
			$this->load->view(ADMIN_DIR.'business/form_maklumat_perniagaan',$data);
		}else if($form == "maklumat_kursus"){
			$data = array('detail' 			=> $detail,
						'business_courses'	=> $this->business_courses->get($business_id)->result_array(),
						'courses' 			=> $this->course->get()->result_array(),
						'mode'				=> $mode,
					  );
			$this->load->view(ADMIN_DIR.'business/form_maklumat_kursus',$data);
		}else if($form == "comment"){
			$this->scripts = array('jquery.dataTables');
			$this->load->model('business_comment');
			$data = array(
					'detail'   => $detail,
					'comments' => $this->business_comment->get(array('business_id' => $business_id))->result_array(),
					'mode'	   => $mode
			);
			$this->load->view(ADMIN_DIR.'business/form_comment',$data);
		}
	}
	
	function save($form = "akaun"){
		$business_id 	= $this->input->post('business_id');
		if($business_id != NULL){
			$mode = 'EDIT';
		}else{
			$mode = 'ADD';
		}
		$direct_mode = $this->input->post('mode');
		
		if($form == "maklumat_diri"){
			$next_form = "maklumat_perniagaan/".$business_id;
			$business_info  = $this->business_person->get(array('business_id'=>$business_id))->row_array();
			$person_name		= $this->input->post('name');
			if($this->business_person->check_bin_al($person_name) == 'bin' || $this->business_person->check_bin_al($person_name) == 'al'){
				$result 			= $this->business_person->person_gender_religion($person_name);
				$person_gender		= $result['gender'];
				$person_religion	= ($this->business_person->check_bin_al($person_name) == 'al' ? $this->input->post('religion') : $result['religion']);
			}else{
				$person_gender 		= $this->input->post('gender');
				$person_religion	= $this->input->post('religion');
			}
			$updated_data	= array('name' 			=> $person_name,
									'category' 		=> $this->input->post('category'),
									'ic_no' 		=> $this->input->post('ic_no'),
									'age' 			=> $this->input->post('age'),
									'army_no' 		=> $this->input->post('army_no'),
									'address' 		=> $this->input->post('address'),
									'telp_no' 		=> $this->input->post('telp_no'),
									'state_id' 		=> $this->input->post('state_id'),
									'geo_location' 	=> $this->input->post('geo_location'),
									'gender' 		=> $person_gender,
									'race' 			=> $this->input->post('race'),
									'religion' 		=> $person_religion,
									'status_atm'	=> $this->input->post('status_atm'),
									'status_pencen'	=> $this->input->post('status_pencen'),
									'education_level'=> $this->input->post('education_level'),
									);
			if($mode == 'EDIT'){
				$this->business_person->edit($business_id,$updated_data);
			}else{
				$business_id = $this->business_person->add($updated_data);
			}
			//Uploading image
			if($_FILES['business_logo']['name'] != NULL){				
				if(isset($business_info['business_logo']) && file_exists(realpath(APPPATH . '../assets/img/business_logo') . DIRECTORY_SEPARATOR . $business_info['business_logo'])){
					$this->remove_business_logo($business_info['business_logo']);
				}
				$filename 		= $business_id."_".$_FILES['business_logo']['name'];
				$business_logo 	= $this->upload($filename,'business_logo',PATH_TO_BUSINESS_LOGO);
			}else{
				if($mode == 'EDIT'){
					$business_logo = $business_info['business_logo'];
				}else{
					$business_logo = "";
				}
			}
			$data_business_logo['business_logo'] = $business_logo;
			$this->business_person->edit($business_id,$data_business_logo);
		}
		//save perniagaan
		else if($form == "maklumat_perniagaan"){
			$next_form = "maklumat_kursus/".$business_id;
			$updated_data_niaga	= array(
									'work_status' 		=> $this->input->post('work_status'),
									'registration_no' 	=> $this->input->post('registration_no'),
									'registration_date' => $this->input->post('registration_date'),
									'business_name' 	=> $this->input->post('business_name'),
									'business_address' 	=> $this->input->post('business_address'),
									'email_web' 		=> $this->input->post('email_web'),
									'main_product' 		=> $this->input->post('main_product'),
									'account_type' 		=> $this->input->post('account_type'),
									'financing_by' 		=> $this->input->post('financing_by'));
			$activities		= $this->input->post('business_activity');
			if(count($activities) > 0){
				$this->business_model->delete_business_activity($business_id);
				foreach($activities as $activity){
					unset($activity_data_post);
					$activity_data_post = array('business_id' => $business_id,
												'activity_id' => $activity);
					$this->business_model->add_business_activity($activity_data_post);
				}				
			}
			if($mode == 'EDIT'){
				$this->business_model->edit($business_id,$updated_data_niaga);
			}else{
				$updated_data_niaga['business_id'] = $business_id;
				$this->business_model->add($updated_data_niaga);
			}
		}
		//save kursus
		else if($form == "maklumat_kursus"){
			$next_form = "comment/".$business_id;
			$courses		= $this->input->post('courses');
			$this->business_courses->delete($business_id);
			if($courses !=  NULL){
				for($i=0; $i<count($courses);$i++){
					unset($course_data_post);
					$course_data_post	= array('business_id' 	=> $business_id,
												'courses_id'	=> $courses[$i],
											);
					$this->business_courses->add($course_data_post);
				}
			}
		}
		//Save account
		else if($form == "akaun"){
			if($mode == 'ADD'){
				$business_id = $this->business_person->add(array('gender' => 0));
				$this->business_model->add(array('business_id' => $business_id));
				$data_account	= array(
					'role_id' 			=> 2,
					'business_id' 		=> $business_id,
					'email' 			=> $this->input->post('account_email'),
					'password' 			=> $this->input->post('account_password'),
					'activation_status'	=> 1,
				);
				$this->user->add($data_account);
				$next_form = "maklumat_diri/".$business_id;
			}		
		}
		//Save comment
		else if($form == "comment"){
			$this->load->model('business_comment');
			$comments = $this->input->post('comment');
			$this->business_comment->delete($business_id);
			for($i=0;$i<count($comments);$i++){
				$data_comment	= array(
					'business_id' 		=> $business_id,
					'comment' 			=> $comments[$i]
				);
				if($comments[$i] != ""){
					$this->business_comment->add($data_comment);
				}
			}
			if($direct_mode == 'EDIT'){
				$next_form = "comment";
			}else{
				$next_form = "";
			}
		}
		//define redirect link
		if(isset($_POST['save'])){
			if($direct_mode == 'EDIT'){
				$this->session->set_flashdata('form_msg', 'true');
				if($next_form == ""){
					redirect(base_url().ADMIN_DIR."business/");
				}else{
					redirect(base_url().ADMIN_DIR."business/edit/".$form."/".$business_id);	
				}
			}else{
				if($next_form == ""){
					$this->session->set_flashdata('form_msg', 'true');
					redirect(base_url().ADMIN_DIR."business/");
				}else{
					redirect(base_url().ADMIN_DIR."business/add/".$next_form);
				}
			}
		}
	}
	
	public function upload($name,$attachment,$upload_path) {
		$this->load->library('upload');
		$config['file_name'] 		= $name;
		$config['upload_path'] 		= $upload_path;
		$config['allowed_types'] 	= 'png|jpg|gif|bmp|jpeg';
		$config['remove_spaces']	= TRUE;
		$config['overwrite']		= TRUE;
		
		$this->upload->initialize($config);
		if(!$this->upload->do_upload($attachment,true)) {
			echo $this->upload->display_errors();
			return false;
		}else{
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
	}
	
	public function remove_business_logo($name){
		if($name != NULL){
			$url = "./assets/img/business_logo/".$name;
			if (file_exists(realpath(APPPATH . '../assets/img/business_logo') . DIRECTORY_SEPARATOR . $name)) {
				$remove = unlink($url);
			}else{
				return false;
			}
		}
		return true;
	}
	
	function delete($business_id){
	//	parent::__construct("business_list");
		$this->load->model('business_comment');
		$this->business_person->delete($business_id);
		$this->business_model->delete($business_id);
		$this->business_model->delete_business_activity($business_id);
		$this->business_comment->delete($business_id);
		$this->user->delete($business_id);
		redirect(base_url().ADMIN_DIR."business/");
	}
	
	function change_password($business_id) {
	//	parent::__construct("business_list");
			$this->load->model('user');
			$user = $this->user->get(array('business_id' => $business_id))->row_array();
            if ($new_password = $this->input->post('new_password')) {
                if ($this->user->edit_admin($user['user_id'],array('business_id'=>$business_id,'email'=>$user['email'],'password' => $new_password))) {
                    $this->session->set_flashdata('form', array('success' => 'true', 'message' => 'Your new password has been saved.'));
                } else {
                    $this->session->set_flashdata('form', array('success' => 'false', 'message' => 'Change password failed.'));
                }
				redirect(base_url().ADMIN_DIR.'business/change_password/'.$business_id);//should be redirect so that flashdata can be viewed.
            }
            $data = array('form_success' => $this->session->flashdata('form'),
            			  'business_id'	=> $business_id);
            $this->load->view(ADMIN_DIR.'business/form_change_password', $data);
    }
}

