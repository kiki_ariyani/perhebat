<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Highlights extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library(array('session','upload'));
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "highlights /";
		$this->layout = "admin";
		$this->load->model(array('highlight'));
	}

	public function index() {
		$this->scripts = array('jquery.dataTables');
		$menu = $this->menu->get(array('url' => "administrator/highlights"))->row_array();
		$data = array('highlights' 	=> $this->highlight->get()->result_array(),
					  'messages' 	=> $this->session->flashdata('form_msg'),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array()
		);
		$this->load->view(ADMIN_DIR.'highlight/index',$data);
	}
	
	function add(){
		$this->parts['p_title'] = "add /";
		$data = array('mode' => 'ADD',
					'messages' => "",
		);
		$this->load->view(ADMIN_DIR.'highlight/form',$data);
	}
	
	function edit($id){
		$this->parts['p_title'] = "edit /";
		$data = array('mode' 	=> 'EDIT',
					'highlight' 	=> $this->highlight->get(array('highlight_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
		);
		$this->load->view(ADMIN_DIR.'highlight/form',$data);
	}
	
	function view($id){
		$this->parts['p_title'] = "view /";
		$data = array('mode' 	=> 'VIEW',
					'highlight' => $this->highlight->get(array('highlight_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
		);
		$this->load->view(ADMIN_DIR.'highlight/form',$data);
	}
	
	function add_handler(){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
		);
		if($highlight_id = $this->highlight->add($data)){
			if($_FILES['photo']['name'] != NULL){
				$this->_upload($highlight_id.".jpg",'photo',PATH_TO_HIGHLIGHT_PROFILE);
			}
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/highlights'));
	}
	
	function edit_handler($id){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
		);
		if($_FILES['photo']['name'] != NULL){
			$this->_upload($id.".jpg",'photo',PATH_TO_HIGHLIGHT_PROFILE);
		}
		if($this->highlight->edit($id,$data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/highlights/edit/'.$id));
	}
	
	function delete($id){
		$thumb = file_exists(realpath(APPPATH . '../assets/attachment/content') . DIRECTORY_SEPARATOR . $id . ".jpg") ? BASE_URL() . "assets/attachment/content/" . $highlight['highlight_id'] . ".jpg?".rand() : "";
		if($thumb){
			$this->_remove_file($id.".jpg","content");
		}
		if($this->highlight->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/highlights/'));
	}
	
	public function _upload($name,$attachment,$upload_path) {
		$config['file_name'] 		= $name;
		$config['upload_path'] 		= $upload_path;
		$config['allowed_types'] 	= 'gif|jpg|png|bmp|jpeg|GIF|JPG|PNG|BMP|JPEG|pdf|doc|xls';
		//$config['max_size'] 		= 2000;
		//$config['max_width'] 		= 1024;
		//$config['max_height'] 	= 1024;
		$config['remove_spaces']	= TRUE;
		$config['overwrite']		= TRUE;
		
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($attachment,true)) {
			echo $this->upload->display_errors();
			return false;
		} else {
			//echo "success";
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
	}
	
	public function _remove_file($name,$type){
		if($name != NULL){
			$url = "./assets/attachment/".$type."/".$name;
			if (file_exists(realpath(APPPATH . '../assets/attachment/'.$type) . DIRECTORY_SEPARATOR . $name)) {
				$remove = unlink($url);
			}else{
				return false;
			}
		}
		return true;
	}
	
	function featured($id){
		$featured_data = $this->highlight->get(array('is_featured' => 1))->num_rows();
		if($featured_data < 2){
			if($this->highlight->edit($id,array('is_featured' => 1))){
				$this->session->set_flashdata('form_msg','Your data has been set data as featured.');
			}
		}else{
			$this->session->set_flashdata('form_msg_err',"You can't set more than 2 featured data.");
		}
		redirect(site_url('administrator/highlights'));
	}
	
	function unfeatured($id){
		if($this->highlight->edit($id,array('is_featured' => 0))){
			$this->session->set_flashdata('form_msg','Your data has been set data as unfeatured.');
		}
		redirect(site_url('administrator/highlights'));
	}
}

