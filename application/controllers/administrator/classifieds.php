<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Classifieds extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "classified /";
		$this->layout = "admin";
		$this->load->model(array('classified','business_person'));
	}

	public function index() {
		$data['classifieds'] = $this->classified->get_classified_with_person();
		$data['messages'] = $this->session->flashdata('form_msg');
		$this->load->view(ADMIN_DIR."classified/index", $data);
	}
	
	function edit($id){
		$sess 			 = $this->session->userdata('perniagaan_user');
		$company 		 = $this->business_person->get(array('business_id'=>$sess['business_id']))->row_array();
		$classified		= $this->classified->get(array('classified_id' => $id))->row_array();
		$data = array(
			'mode' 		 => 'EDIT',
			'classified' => $classified,
			'person'	 => $this->business_person->get(array('business_id'=>$classified['user_id']))->row_array(),
			'company'	 => $company,
			'type'		 => unserialize(CLASSIFIED_TYPE),
			'messages'	 => $this->session->flashdata('form_msg'),
 		);
		$this->load->view(ADMIN_DIR."classified/form", $data);
	}
	
	function save(){
		$this->layout 	= false;
		$start 			= $this->input->post('start_date');
		$end 			= date('Y-m-d', strtotime($start . " +30 days"));
		$sess 			= $this->session->userdata('perniagaan_user');
		$mode			= $this->input->post('mode');
		
		$data_post 		= array('user_id' 			=> $this->input->post('user_id'),
								'title' 			=> $this->input->post('title'),
								'description'		=> $this->input->post('description'),
								'start_date'		=> $start,			
								'end_date'			=> $end,			
								'contact_person'	=> $this->input->post('contact'),			
								'contact_no'		=> $this->input->post('contact_no'),			
								'type'				=> $this->input->post('type')
						  );
		
		if($mode == 'ADD'){
			$classified = $this->classified->get_notexpire(array('user_id' => $sess['user_id']))->num_rows();	
			if($classified < 2){
				$classified_id = $this->classified->add($data_post);
			}
		}else if($mode == 'EDIT'){
			$classified_id = $this->input->post('classified_id');
			$this->classified->edit($classified_id,$data_post);
		}else{
			$classified_id = 0;
		}
		if($classified_id){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		if($mode == 'EDIT'){
			redirect(site_url(ADMIN_DIR.'classifieds/edit/'.$classified_id));
		}else{
			redirect(site_url(ADMIN_DIR.'classifieds/'));
		}
	}
	
	function view($id){
		$classified		= $this->classified->get(array('classified_id' => $id))->row_array();
		$data = array(
			'mode' 		 => 'VIEW',
			'classified' => $classified,
			'type'		 => unserialize(CLASSIFIED_TYPE),
			'sess'		 => $this->session->userdata('perniagaan_user'),
			'messages'	 => '',
			'person'	 => $this->business_person->get(array('business_id'=>$classified['user_id']))->row_array(),
 		);
		$this->load->view(ADMIN_DIR."classified/form", $data);
	}
	
	function delete($id){
		if($this->classified->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
			redirect(site_url(ADMIN_DIR.'classifieds'));
		}
	}
}

