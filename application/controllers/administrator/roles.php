<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Roles extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$user = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array('administrator/role_setting');
		$this->styles 			= array();
		$this->title 			= "role /";
		$this->layout 			= "admin";
		$this->load->model(array('role','menu'));
	}

	public function index($type = 0) {
		$data = array('roles' 		=> $this->role->get(array('type' => $type))->result_array(),
					  'menus'		=> $this->menu->get_sorted_menu(array('parent_id'=>0,'type' => $type),array('type' => $type)),
					  'messages' 	=> $this->session->flashdata('form_msg'),
					  'type'		=> $type,
					  'page_type'	=> unserialize(PAGE_TYPE),
		);
		$this->load->view(ADMIN_DIR.'role/index',$data);
	}
	
	function add(){
		$this->parts['p_title']  = "add /";
		$data = array('mode' 	 	=> 'ADD',
					  'messages' 	=> "",
					  'admin_menus' => $this->menu->get_sorted_menu(array('parent_id' => 0,'type' => ADMIN_PAGE)),
					  'user_menus'	=> $this->menu->get_sorted_menu(array('parent_id' => 0,'type' => USER_PAGE)),
					  'page_type'	=> unserialize(PAGE_TYPE),
		);
		$this->load->view(ADMIN_DIR.'role/form',$data);
	}
	
	function edit($id){
		$this->parts['p_title']  = "edit /";
		$admin_menus = $this->menu->get_sorted_menu(array('parent_id'=>0,'type' => ADMIN_PAGE));
		$user_menus = $this->menu->get_sorted_menu(array('parent_id'=>0, 'type' => USER_PAGE));
		
		$data = array('mode' 	 => 'EDIT',
					  'role' 	 => $this->role->get(array('role_id' => $id))->row_array(),
					  'messages' => $this->session->flashdata('form_msg'),
					  'admin_menus' => $this->edit_load_data($id,$admin_menus),
					  'user_menus'  => $this->edit_load_data($id,$user_menus),
					  'page_type'	=> unserialize(PAGE_TYPE),
		);
		$this->load->view(ADMIN_DIR.'role/form',$data);
	}
	
	function edit_load_data($id,$menus){
		for($i=0;$i<count($menus);$i++){
			$menu_role_setting   = $this->role->get_role_setting(array('role_id'=>$id,'menu_id'=>$menus[$i]['menu_id']))->row_array();
			$menus[$i]['view']   = (isset($menu_role_setting['view']) ? $menu_role_setting['view'] : 0);
			$menus[$i]['create'] = (isset($menu_role_setting['create']) ? $menu_role_setting['create'] : 0);
			$menus[$i]['edit']   = (isset($menu_role_setting['edit']) ? $menu_role_setting['edit'] : 0);
			if(count($menus[$i]['childs']) > 0){
				for($j=0;$j<count($menus[$i]['childs']);$j++){
					$child_role_setting   = $this->role->get_role_setting(array('role_id'=>$id,'menu_id'=>$menus[$i]['childs'][$j]['menu_id']))->row_array();
					$menus[$i]['childs'][$j]['view']   = (isset($child_role_setting['view']) ? $child_role_setting['view'] : 0);
					$menus[$i]['childs'][$j]['create'] = (isset($child_role_setting['create']) ? $child_role_setting['create'] : 0);
					$menus[$i]['childs'][$j]['edit']   = (isset($child_role_setting['edit']) ? $child_role_setting['edit'] : 0);
				}
			}
		}
		return $menus;
	}
	
	function save(){
		$post_role_id   = $this->input->post('role_id');
		$role_data 		= array('name'=>$this->input->post('name'),'description'=>$this->input->post('description'),'type'=>$this->input->post('type'));
		if($post_role_id > 0){
			$role_id    = $post_role_id;
			$this->role->edit($role_id,$role_data);
		}else{
			$role_id 	= $this->role->add($role_data);
		}
		
		//initialize post data
		$view			= $this->input->post('view');
		$create			= $this->input->post('create');
		$edit			= $this->input->post('edit');
		$menu_id		= $this->input->post('menu_id');	
		$mode			= $this->input->post('mode');
		
		//delete all role setting first when save mode is 'ADD'
		if($mode == 'EDIT'){
			$this->role->delete_role_setting(array('role_id' =>$role_id));
		}
		
		//save role setting
		for($i=0;$i<count($menu_id);$i++){
			$role_setting_data = array('role_id' => $role_id,
									   'menu_id' => $menu_id[$i],
									   'view'	 => $view[$i],
									   'create'	 => $create[$i],
									   'edit'	 => $edit[$i]);
			$this->role->add_role_setting($role_setting_data);
		}
		redirect(site_url('administrator/roles/'));
	}
	
	function delete($id){
		if($this->role->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/roles/'));
	}

}

