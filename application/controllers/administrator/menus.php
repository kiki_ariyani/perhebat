<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Menus extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library(array('session','upload'));
//		$this->load->model('admin_session');
		$user = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "menus /";
		$this->layout = "admin";
		$this->load->model(array('menu'));
	}

	public function index() {
		$this->scripts = array('jquery.dataTables');
		$data = array('menu' 	=> $this->menu->get_menu_parent(),
					  'messages' 	=> $this->session->flashdata('form_msg'),
		);
		$this->load->view(ADMIN_DIR.'menu/index',$data);
	}
	
	function add(){
		$this->parts['p_title'] = "add /";
		$data = array('mode' => 'ADD',
					'messages' => "",
					'parent_item' => $this->menu->get(array('parent_id' => 0))->result_array(),
					'page_type'	=> unserialize(PAGE_TYPE),
		);
		$this->load->view(ADMIN_DIR.'menu/form',$data);
	}
	
	function edit($id){
		$this->parts['p_title'] = "edit /";
		$data = array('mode' 	=> 'EDIT',
					'menu' 	=> $this->menu->get(array('menu_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'parent_item' => $this->menu->get(array('parent_id' => 0))->result_array(),
					'page_type'	=> unserialize(PAGE_TYPE),
		);
		$this->load->view(ADMIN_DIR.'menu/form',$data);
	}
	
	function view($id){
		$this->parts['p_title'] = "view /";
		$data = array('mode' 	=> 'VIEW',
					'menu' 	=> $this->menu->get(array('menu_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'parent_item' => $this->menu->get(array('parent_id' => 0))->result_array(),
					'page_type'	=> unserialize(PAGE_TYPE),
		);
		$this->load->view(ADMIN_DIR.'menu/form',$data);
	}
	
	function add_handler(){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
			'url'			=> $this->input->post('url'),
			'parent_id'		=> $this->input->post('parent'),
			'type'			=> $this->input->post('type'),
		);
		if($menu_id = $this->menu->add($data)){
			if($_FILES['icon']['name'] != NULL){
				$this->_upload($menu_id.".jpg",'icon',PATH_TO_MENU_ICON);
			}
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/menus'));
	}
	
	function edit_handler($id){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
			'url'			=> $this->input->post('url'),
			'parent_id'		=> $this->input->post('parent'),
			'type'			=> $this->input->post('type'),
		);
		if($this->menu->edit($id,$data)){
			if($_FILES['icon']['name'] != NULL){
				$this->_upload($id.".jpg",'icon',PATH_TO_MENU_ICON);
			}
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/menus/edit/'.$id));
	}
	
	function delete($id){
		if($this->menu->delete(array('menu_id' => $id))){
			$thumb = file_exists(realpath(APPPATH . '../assets/img/menu_icon') . DIRECTORY_SEPARATOR . $id . ".jpg") ? BASE_URL() . "assets/img/menu_icon/" . $id . ".jpg?".rand() : "";
			if($thumb != ""){
				$this->_remove_file($id.".jpg");
			}
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/menus/'));
	}
	
	public function _upload($name,$attachment,$upload_path) {
		$config['file_name'] 		= $name;
		$config['upload_path'] 		= $upload_path;
		$config['allowed_types'] 	= 'gif|jpg|png|bmp|jpeg|GIF|JPG|PNG|BMP|JPEG';
		//$config['max_size'] 		= 2000;
		//$config['max_width'] 		= 1024;
		//$config['max_height'] 	= 1024;
		$config['remove_spaces']	= FALSE;
		$config['overwrite']		= TRUE;
		
		$this->upload->initialize($config);
		if (!$this->upload->do_upload($attachment,true)) {
			return false;
		}
		return true;
	}
	
	public function _remove_file($name){
		if($name != NULL){
			$url = "./assets/img/menu_icon/".$name;
			if (file_exists(realpath(APPPATH . '../assets/img/menu_icon') . DIRECTORY_SEPARATOR . $name)) {
				$remove = unlink($url);
			}else{
				return false;
			}
		}
		return true;
	}
}

