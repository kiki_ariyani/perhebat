<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('common.php');

class Locators extends Common {
	function __construct() {
		parent::__construct("locator");

		$this->load->library('session');
//		$this->load->model('admin_session');
		$user = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "search /";
		$this->layout 			= "admin";
	}
	
	function index(){
		$this->load->model('mapping');
		$this->load->library('googlemaps');
		
		$keyword 	= $this->input->post('key');
		$state 		= $this->input->post('state');
		
		$config['center'] = '4.210484,101.975766';
		$config['zoom'] = '7';
		$config['map_height'] = '590px';
		$config['disableMapTypeControl'] = true;
		$config['disableStreetViewControl'] = true;
		$this->googlemaps->initialize($config);
		
		if (isset($_POST['submit'])) {
			$pins = $this->mapping->locators_by_activity($keyword, $state);
		} else {
			$pins = $this->mapping->locators_by_activity("","");
		}
		
		foreach($pins as $pin){
				if (!empty($pin->geo_location)){
					
					if ($pin->category == 1){
						$category = "Army";
					} else if ($pin->category == 1){
						$category = "Air Force";
					} else {
						$category = "Navy";
					}
					
					if($pin->activity_id == 1){
						$icon = "red.png";
					}else if($pin->activity_id == 2){
						$icon = "green.png";
					}else if($pin->activity_id == 3){
						$icon = "blue.png";
					}else if($pin->activity_id == 4){
						$icon = "yellow.png";
					}else if($pin->activity_id == 5){
						$icon = "grey.png";
					}
					
					$marker = array();
					$marker['position'] = $pin->geo_location;
					$marker['infowindow_content'] = "<b>".$pin->business_name."</b><br>". 
													"<p style='font-size:11px'>".
													"<b>Name : </b>".$pin->name."<br>".
													"<b>No. IC : </b>".$pin->ic_no."<br>".
													"<b>Service Category : </b>".$category."<br>".
													"<b>Address : </b>".trim(preg_replace('/\s+/', ' ', $pin->address))."<br>".
													"<b>State : </b>".$pin->state_name."<br>".
													"<b>Telp : </b>".($pin->telp_no == "" ? "-" : $pin->telp_no)."<br>".
													"<b>Email/Web : </b>".($pin->email_web == "" ? "-" : $pin->email_web)."<br>".
													"<b>Business Activity : </b>".$pin->activity_name."<br>".
													"</p>";
					$marker['animation'] = 'DROP';
					$marker['scrollwheel'] = FALSE;
					$marker['icon'] = base_url()."assets/img/map_icon/".$icon;
					$this->googlemaps->add_marker($marker);
				} 
			}
			
			// additional address
			if (isset($_POST['submit'])) {
				if($state != 0){
					$additional = $this->mapping->locators_additional_address(array('state_id' => $state));
				}else{
					$additional = $this->mapping->locators_additional_address();
				}
			} else {
				$additional = $this->mapping->locators_additional_address();
			}
			
			foreach($additional as $row){
					if($row->type == 1){
						$icon = "red-square.png";
					}else if($row->type == 2){
						$icon = "green-tri.png";
					}else if($row->type == 3){
						$icon = "yellow-tri.png";
					}
					
					$marker = array();
					$marker['position'] = $row->geo_location;
					$marker['infowindow_content'] = "<b>".$row->name."</b><br>". 
													"<p style='font-size:11px'>".
													"<b>Address : </b>".$row->address."<br>".
													"<b>Telp : </b>".($row->telp == "" ? "-" : $row->telp)."<br>".
													"</p>";
				
					$marker['animation'] = 'DROP';
					$marker['scrollwheel'] = FALSE;
					$marker['icon'] = base_url()."assets/img/map_icon/".$icon;
					$this->googlemaps->add_marker($marker);
			}
		
		$data['datasearch'] = array('keyword' => $keyword,'state' => $state);
		$data['map'] = $this->googlemaps->create_map();
		$data['states'] = $this->mapping->get_state()->result();
		$this->parts['p_title'] = "location /";
		$this->load->view(ADMIN_DIR.'locators/index', $data);
	}
	
	
	
	//generate data for choropleth map
	function generate_data(){
		$this->load->model('mapping');
		$pins = $this->mapping->locators_by_activity(NULL, NULL);
		$data = array();
		foreach($pins as $row){
			if($row->activity_id == 1){
				$color = "red";
			}else if($row->activity_id == 2){
				$color = "green";
			}else if($row->activity_id == 3){
				$color = "blue";
			}else if($row->activity_id == 4){
				$color = "yellow";
			}else if($row->activity_id == 5){
				$color = "grey";
			}
					
			if($row->geo_location != NULL){
				$geo = explode(',', $row->geo_location);
			}else{
				$geo = array("","");
			}
			$data[] = array("id" => $row->business_id,"city" => $row->state_name,"country" => "Malaysia","lat" => $geo[0],"lon" => $geo[1],"activity" => $color);
		}
		
		echo json_encode($data);
	}
}

