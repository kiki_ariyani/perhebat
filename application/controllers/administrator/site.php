<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {
	function __construct() {
		parent::__construct();

		require_once('common.php');
		$this->load->library('session');
		$this->load->model('admin_session');
		$user = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "";
		$this->layout = "admin";
		$this->parts = array();
	}

	public function index() {
		if($user = $this->session->userdata(ADMIN_SESSION)){
			redirect(site_url(ADMIN_DIR.'business'));
		}else{
			$this->load->view(ADMIN_DIR."site/index");
		}
	}

	function login() {
		$data['message'] = $this->session->flashdata('login_msg');
		$this->load->view(ADMIN_DIR."site/login",$data);
	}
	
	function validate() {
		$this->layout = false;
		$login_success = false;
		//if ($_POST['login']) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if (isset($username) && isset($password)) {
			
				if ($this->admin_session->create($username, $password)) {
					$login_success = true;
				}
			}
		//}
		if(!$login_success){
			$this->session->set_flashdata('login_msg', 'false');
		}else{
			$this->session->set_flashdata('login_msg', 'true');
		}
		redirect(site_url(ADMIN_DIR));
	}

	function logout($param = NULL) {
		$this->admin_session->clear();
		if($param != NULL){
			$this->session->set_flashdata('login_msg', 'false');
		}
		redirect(site_url(ADMIN_DIR));
	}
	
	function no_access(){
		$this->load->view(ADMIN_DIR.'site/no_access');
	}
	
	function not_found(){
		$this->load->view(ADMIN_DIR.'site/page_not_found');
	}

}

