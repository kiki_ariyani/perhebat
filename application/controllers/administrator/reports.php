<?php

require_once("common.php");
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Reports extends Common {

	function __construct() {
		parent::__construct("articles");
		
		$this->load->helper('form');
		$this->load->model(array('business_person','business_model'));
		$this->scripts 	= array('jquery.form','highcharts','administrator/new_report');
		$user = $this->admin_session->get();
		$this->meta 			= array();
		$this->styles 			= array('redmond/jquery-ui-1.8.17-green.custom');
		$this->title 	= "reports /";
	}

	function index(){
		$this->state();
	}
	
	function state(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "state /";
		$this->load->view('administrator/new_report/state',$data);
	}
	
	function state_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('states');
		$result = array();
		$j = 0;
		for($i=0;$i<count($req_states);$i++){
			foreach($states as $state){
				if($req_states[$i] == 0){
					$value = $this->business_person->get(array('state_id'=>$state['state_id']))->num_rows();
					$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
					$result['title'][$j] = $title['name'];
					$result['value'][$j] = array("y" => $value,'id' => $state['state_id']);
					$j++;
				}else if($req_states[$i] == $state['state_id']){
					$value = $this->business_person->get(array('state_id'=> $req_states[$i]))->num_rows();
					$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
					$result['title'][$j] = $title['name'];
					$result['value'][$j] = array("y" => $value,'id' => $req_states[$i]);
					$j++;
				}
			}
		}
		echo json_encode($result);
	}
	
	function status(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "status /";
		$this->load->view('administrator/new_report/status',$data);
	}
	
	function status_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('status');
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_not_reg_arr = array();
		$value_reg_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
					$value_not_reg = $this->business_model->get_with_person(array('business.registration_no' => "",'state_id'=> $state['state_id']),'left')->num_rows();
					$value_reg = $this->business_model->get_with_person(array('business.registration_no !=' => "",'state_id'=> $state['state_id']),'left')->num_rows();
					$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
					$result['title'][$j] = $title['name'];
				//	$value_not_reg_arr[$j] = $value_not_reg;
				//	$value_reg_arr[$j] = $value_reg;
					$value_not_reg_arr[$j] = array("y" => $value_not_reg,'id' => $state['state_id'],'seri' => "tidak_berdaftar");
					$value_reg_arr[$j] = array("y" => $value_reg,'id' => $state['state_id'],'seri' => "berdaftar");
					$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						$value_not_reg = $this->business_model->get_with_person(array('business.registration_no' => "",'state_id'=> $req_states[$i]),'left')->num_rows();
						$value_reg = $this->business_model->get_with_person(array('business.registration_no !=' => "",'state_id'=> $req_states[$i]),'left')->num_rows();
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
					//	$value_not_reg_arr[$j] = $value_not_reg;
					//	$value_reg_arr[$j] = $value_reg;
						$value_not_reg_arr[$j] = array("y" => $value_not_reg,'id' => $req_states[$i],'seri' => "tidak_berdaftar");
						$value_reg_arr[$j] = array("y" => $value_reg,'id' => $req_states[$i],'seri' => "berdaftar");
						$j++;
					}
				}
			}
		}
		
		$result['category'][0] = array('name' => 'Berdaftar','data' => $value_reg_arr);
		$result['category'][1] = array('name' => 'Tidak Berdaftar','data' => $value_not_reg_arr);
		echo json_encode($result);
	}
	
	function category(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "category /";
		$this->load->view('administrator/new_report/category',$data);
	}
	
	function category_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$business_cat = unserialize(BUSINESS_PERSON_CATEGORY);
		$req_states = $this->input->post('category');
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($business_cat);$k++){
					$value[$k] = array('y'=> $this->business_model->get_with_person(array('person.category' => $business_cat[$k]['category_id'],'state_id'=> $state['state_id']),'left')->num_rows(),'id' => $state['state_id'],'seri' => $business_cat[$k]['category_id']);
				}
				$value_arr[$j] = $value;
				$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
				$result['title'][$j] = $title['name'];
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($business_cat);$k++){
							$value[$k] = array('y'=>$this->business_model->get_with_person(array('person.category' => $business_cat[$k]['category_id'],'state_id'=> $req_states[$i]),'left')->num_rows(),'id' => $req_states[$i],'seri' => $business_cat[$k]['category_id']);
						}
						$value_arr[$j] = $value;
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$j++;
					}
				}
			}
		}
		
		$new_array = array();
		for($l=0;$l<count($business_cat);$l++){
			for($m=0;$m<count($value_arr);$m++){
				$new_array[$m] = $value_arr[$m][$l];
			}
			$result['category'][$l] = array('name' => $business_cat[$l]['name'],'data' => $new_array);
		}
		echo json_encode($result);
	}
	
	function activity(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "activity /";
		$this->load->view('administrator/new_report/activity',$data);
	}
	
	function activity_handler(){
		$this->load->model('activity');
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$activities = $this->activity->get()->result_array();
		$req_states = $this->input->post('activity');
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($activities);$k++){
					$value[$k] = array('y'=>$this->activity->get_business_activity(array('activity.activity_id' => $activities[$k]['activity_id'],'state_id'=> $state['state_id']),'left')->num_rows(),'id'=>$state['state_id'],'seri' => $activities[$k]['activity_id']);
				}
				$value_arr[$j] = $value;
				$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
				$result['title'][$j] = $title['name'];
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($activities);$k++){
							$value[$k] = array('y'=>$this->activity->get_business_activity(array('activity.activity_id' => $activities[$k]['activity_id'],'state_id'=> $req_states[$i]),'left')->num_rows(),'id'=> $req_states[$i],'seri'=> $activities[$k]['activity_id']);
						}
						$value_arr[$j] = $value;
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$j++;
					}
				}
			}
		}
		
		$new_array = array();
		for($l=0;$l<count($activities);$l++){
			for($m=0;$m<count($value_arr);$m++){
				$new_array[$m] = $value_arr[$m][$l];
			}
			$result['category'][$l] = array('name' => $activities[$l]['name'],'data' => $new_array);
		}
		echo json_encode($result);
	}
	
	function finance(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "finance /";
		$this->load->view('administrator/new_report/finance',$data);
	}
	
	function finance_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$financing = unserialize(BUSINESS_FINANCING_BY);
		$req_states = $this->input->post('state');
		$value = array();
		$result = array();
		
		for($i=0;$i<count($req_states);$i++){
			for($j=0;$j<count($financing);$j++){
				if($req_states[$i] == 0){
					$value[$j] = array('name'=>$financing[$j]['name'],'y'=>$this->business_model->get_with_person(array('business.financing_by' => $financing[$j]['financing_by_id']),'left')->num_rows(),'id'=>$req_states[$i],'seri'=>$financing[$j]['financing_by_id']);
				}else{
					$value[$j] = array('name'=>$financing[$j]['name'],'y'=>$this->business_model->get_with_person(array('business.financing_by' => $financing[$j]['financing_by_id'],'person.state_id'=> $req_states[$i]),'left')->num_rows(),'id'=>$req_states[$i],'seri'=>$financing[$j]['financing_by_id']);
				}
			}
		}
		
		/*for($k=0;$k<count($financing);$k++){
			$result[$k] = array($financing[$k]['name'],$value[$k]);
		}*/
		echo json_encode($value);
	}
	
	function age(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "age /";
		$this->load->view('administrator/new_report/age',$data);
	}
	
	function age_handler(){
		$this->load->model('activity');
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$age_category = array(
			array('label' => 'below 31', 'from' => 0, 'to' => 31),
			array('label' => '31 - 40', 'from' => 30, 'to' => 41),
			array('label' => '41 - 50', 'from' => 40, 'to' => 51),
			array('label' => '51 - 60', 'from' => 50, 'to' => 61),
			array('label' => 'Above 60', 'from' => 60, 'to' => 200),
		);
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($age_category);$k++){
					$value[$k] = array('y'=>$this->business_person->get_current_age(array('current_age >' => $age_category[$k]['from'],'current_age <' => $age_category[$k]['to'],'state_id'=> $state['state_id']),'left')->num_rows(),'id'=> $state['state_id'],'seri'=> $k);
				}
				$value_arr[$j] = $value;
				$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
				$result['title'][$j] = $title['name'];
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($age_category);$k++){
							$value[$k] = array('y'=>$this->business_person->get_current_age(array('current_age >' => $age_category[$k]['from'],'current_age <' => $age_category[$k]['to'],'state_id'=> $req_states[$i]),'left')->num_rows(),'id'=> $state['state_id'],'seri'=> $k);
						}
						$value_arr[$j] = $value;
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$j++;
					}
				}
			}
		}
		
		$new_array = array();
		for($l=0;$l<count($age_category);$l++){
			for($m=0;$m<count($value_arr);$m++){
				$new_array[$m] = $value_arr[$m][$l];
			}
			$result['category'][$l] = array('name' => $age_category[$l]['label'],'data' => $new_array);
		}
		echo json_encode($result);
	}
	
	function work_status(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "work status /";
		$this->load->view('administrator/new_report/work_status',$data);
	}
	
	function work_status_handler(){
		$this->load->model('activity');
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$work_status =  unserialize(WORK_STATUS);
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($work_status);$k++){
					$value[$k] = array('y' => $this->business_model->get_with_person(array('business.work_status' => $work_status[$k]['status_id'],'state_id'=> $state['state_id']),'left')->num_rows(),'id'=>$state['state_id'],'seri'=>$work_status[$k]['status_id']);
				}
				$value_arr[$j] = $value;
				$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
				$result['title'][$j] = $title['name'];
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($work_status);$k++){
							$value[$k] = array('y' => $this->business_model->get_with_person(array('business.work_status' => $work_status[$k]['status_id'],'state_id'=> $req_states[$i]),'left')->num_rows(),'id'=>$state['state_id'],'seri'=>$work_status[$k]['status_id']);
						}
						$value_arr[$j] = $value;
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$j++;
					}
				}
			}
		}
		
		$new_array = array();
		for($l=0;$l<count($work_status);$l++){
			for($m=0;$m<count($value_arr);$m++){
				$new_array[$m] = $value_arr[$m][$l];
			}
			$result['category'][$l] = array('name' => $work_status[$l]['name'],'data' => $new_array);
		}
		echo json_encode($result);
	}
	
	function gender(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "gender /";
		$this->load->view('administrator/new_report/gender',$data);
	}
	
	function gender_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$gender =  unserialize(GENDER);
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($gender);$k++){
					$value[$k] = array('y'=>$this->business_person->get(array('gender' => $gender[$k]['gender_id'],'state_id'=> $state['state_id']),'left')->num_rows(),'id'=>$state['state_id'],'seri'=>$gender[$k]['gender_id']);
				}
				$value_arr[$j] = $value;
				$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
				$result['title'][$j] = $title['name'];
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($gender);$k++){
							$value[$k] = array('y' => $this->business_person->get(array('gender' => $gender[$k]['gender_id'],'state_id'=> $req_states[$i]),'left')->num_rows(),'id'=>$req_states[$i],'seri'=>$gender[$k]['gender_id']);
						}
						$value_arr[$j] = $value;
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$j++;
					}
				}
			}
		}
		
		$new_array = array();
		for($l=0;$l<count($gender);$l++){
			for($m=0;$m<count($value_arr);$m++){
				$new_array[$m] = $value_arr[$m][$l];
			}
			$result['category'][$l] = array('name' => $gender[$l]['name'],'data' => $new_array);
		}
		echo json_encode($result);
	}
	
	/*Provider aka Trainer*/
	function provider(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->parts['p_title'] = "provider /";
		$this->load->view('administrator/new_report/trainer',$data);
	}
	
	function trainer_handler(){
		$this->layout = FALSE;
		$this->load->model(array('trainer','business_courses'));
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$trainers = $this->trainer->get()->result_array();
		
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($trainers);$k++){
					$value[$k] = array('y'=>$this->business_courses->get_all(array('courses.trainer_id' => $trainers[$k]['trainer_id'],'business_person.state_id'=> $state['state_id']),'left')->num_rows(),'id'=>$state['state_id'],'seri'=>$trainers[$k]['trainer_id']);
				}
				$value_arr[$j] = $value;
				$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
				$result['title'][$j] = $title['name'];
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($trainers);$k++){
							$value[$k] = array('y'=>$this->business_courses->get_all(array('courses.trainer_id' => $trainers[$k]['trainer_id'],'business_person.state_id'=> $req_states[$i]),'left')->num_rows(),'id'=>$state['state_id'],'seri'=>$trainers[$k]['trainer_id']);
						}
						$value_arr[$j] = $value;
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$j++;
					}
				}
			}
		}
		
		$new_array = array();
		for($l=0;$l<count($trainers);$l++){
			for($m=0;$m<count($value_arr);$m++){
				$new_array[$m] = $value_arr[$m][$l];
			}
			$result['category'][$l] = array('name' => $trainers[$l]['code'],'data' => $new_array);
		}
		echo json_encode($result);
	}
	
	function status_export_handler(){
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('status');
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_not_reg_arr = array();
		$value_reg_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
					$value_not_reg = $this->business_model->get_with_person(array('business.registration_no' => "",'state_id'=> $state['state_id']),'left')->num_rows();
					$value_reg = $this->business_model->get_with_person(array('business.registration_no !=' => "",'state_id'=> $state['state_id']),'left')->num_rows();
					$title = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
					$result['title'][$j] = $title['name'];
					$value_not_reg_arr[$j] = $value_not_reg;
					$value_reg_arr[$j] = $value_reg;
					$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						$value_not_reg = $this->business_model->get_with_person(array('business.registration_no' => "",'state_id'=> $req_states[$i]),'left')->num_rows();
						$value_reg = $this->business_model->get_with_person(array('business.registration_no !=' => "",'state_id'=> $req_states[$i]),'left')->num_rows();
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$value_not_reg_arr[$j] = $value_not_reg;
						$value_reg_arr[$j] = $value_reg;
						$j++;
					}
				}
			}
		}
		
		$result['category'][0] = array('name' => 'Berdaftar','data' => $value_reg_arr);
		$result['category'][1] = array('name' => 'Tidak Berdaftar','data' => $value_not_reg_arr);
		echo json_encode($result);
		$columns = array("No","Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Status");
	}
	
	function export($file_name,$title,$subject,$columns,$fields,$contents) {
		if(!$columns  = @array_keys($contents[0])) {
			echo 'No transactions were made. <a href="'.base_url().'reports/view">Back</a>'; die();
		}
		
		$old_include_path = get_include_path();
		set_include_path(APPPATH . 'libraries/PHPExcel');

		require_once('PHPExcel.php');
		require_once('PHPExcel/Writer/Excel5.php');

		set_include_path($old_include_path);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
		header('Cache-Control: max-age=0');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("PERHEBAT");
		$objPHPExcel->getProperties()->setLastModifiedBy("PERHEBAT");
		$objPHPExcel->getProperties()->setTitle($title);
		$objPHPExcel->getProperties()->setSubject($subject);

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0);
		$col = 0;
		$row = 1;
		foreach($columns as $column) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
			$col++;
		}
		$col = 0;
		$row++;
		foreach($fields as $field) {
			foreach($columns as $column) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column[$field]);
				$col++;
			}
			$col = 0;
			$row++;
		}

		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

		// Save Excel 2007 file
		$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter->save('php://output');
		exit;
	}
	
	function provider_amount(){
		$this->load->model('trainer');
		$data = array(
			'providers' => $this->trainer->get()->result_array(),
			'states'	=> $this->business_person->get_state()->result_array()
		);
		$this->parts['p_title'] = "provider amount /";
		$this->load->view('administrator/new_report/provider_amount',$data);
	}
	
	function provider_amount_handler(){
		$this->load->model(array('business_courses','course'));
		$this->layout 	= FALSE;
		$states 		= $this->business_person->get_state()->result_array();
		$provider		= $this->input->post('provider');
		$courses	 	= $this->course->get(array('trainer.trainer_id'=>$provider))->result_array();
		$req_states 	= $this->input->post('state');
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result 	= array();
		$value_arr 	= array();
		$j 			= 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($courses);$k++){
					$nums 		= $this->business_courses->get_all(array('courses.courses_id' => $courses[$k]['courses_id'],'state_id'=> $state['state_id']),'left')->num_rows();
					$value[$k]	= array('y'=>$nums * $courses[$k]['cost'],'id'=>$state['state_id'],'seri'=> $courses[$k]['courses_id']);
				}
				$value_arr[$j] 			= $value;
				$title 					= $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
				$result['title'][$j] 	= $title['name'];
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($courses);$k++){
							$nums 	= $this->business_courses->get_all(array('courses.courses_id' => $courses[$k]['courses_id'],'state_id'=> $req_states[$i]),'left')->num_rows();
							$value[$k]	= array('y'=>$nums * $courses[$k]['cost'],'id'=>$req_states[$i],'seri'=>$courses[$k]['courses_id']);
						}
						$value_arr[$j] 			= $value;
						$title 					= $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] 	= $title['name'];
						$j++;
					}
				}
			}
		}
		
		$new_array = array();
		for($l=0;$l<count($courses);$l++){
			for($m=0;$m<count($value_arr);$m++){
				$new_array[$m] = $value_arr[$m][$l];
			}
			$result['category'][$l] = array('name' => $courses[$l]['name'],'data' => $new_array);
		}
		echo json_encode($result);
	}
	
	function course_all_amount(){
		$this->load->model('trainer');
		$data = array(
			'providers' => $this->trainer->get()->result_array()
		);
		$this->parts['p_title'] = "total amount /";
		$this->load->view('administrator/new_report/course_all_amount',$data);
	}
	
	function get_amount($courses){
		$total_amounts	= 0;
		foreach($courses as $course){
			$total_amounts += $course['cost'];
		}
		return $total_amounts;
	}
	
	function course_all_amount_handler(){
		$this->load->model(array('trainer','business_courses','course'));
		$this->layout 	= FALSE;
		$req_trainers	= $this->input->post('provider');
		if(in_array(0,$req_trainers)){
			$trainers	= $this->trainer->get()->result_array();
		}else{
			$trainers	= array();
			foreach($req_trainers as $req_trainer){
				$trainers[] = $this->trainer->get(array('trainer_id'=>$req_trainer))->row_array();
			}
		}
		$data			= array();
		$trains			= array();
		foreach($trainers as $trainer){	
			$trains[]	= $trainer['name'];
			$nums		= $this->business_courses->get_all(array('trainer.trainer_id'=>$trainer['trainer_id']))->result_array();
			$my_amount 	= $this->get_amount($nums);
			$my_courses	= $this->course->get(array('trainer.trainer_id'=>$trainer['trainer_id']))->result_array();
			$cours		= array();
			$c_num		= array();
			$cours_id	= array();
			foreach($my_courses as $my_course){
				$cours[] = $my_course['name'];
				$cours_id[] = $my_course['courses_id'];
				$nums2	 = $this->business_courses->get_all(array('courses.courses_id' => $my_course['courses_id']),'left')->num_rows();
				$c_num[] = $nums2 * $my_course['cost'];
			}
			$data[]		= array('y' 		=> $my_amount,
								'id'		=> $trainer['trainer_id'],
								'drilldown'	=> array('name' 	  => $trainer['name'],
													 'categories' => $cours,
													 'data'		  => $c_num,
													 'id'		=> $trainer['trainer_id'],
													 'seri'		=> $cours_id));
		}
		$result 		= array('categories' => $trains,
								'data'		 => $data);
		echo json_encode($result);
	}
	
	/*
	param: $state_id is $trainer_id on "count_amount" type
	*/
	function get_person($state_id,$type,$series = NULL){
		$this->layout = FALSE;
		$categories = unserialize(BUSINESS_PERSON_CATEGORY);
		if($type == "state"){
			$result = $this->business_person->get(array('state_id'=>$state_id))->result_array();
			for($i=0;$i<count($result);$i++){
				$business = $this->business_model->get(array('business_id' => $result[$i]['business_id']))->row_array();
				$result[$i]['business_name'] = $business['business_name'];
				$result[$i]['person_name'] = $result[$i]['name'];
			}
		}else if($type == "status"){
			if($series != NULL){
				if($series == "berdaftar"){
					$result = $this->business_model->get_with_person(array('state_id'=>$state_id,'business.registration_no !=' => ""))->result_array();
				}else if($series = "tidak_berdaftar"){
					$result = $this->business_model->get_with_person(array('state_id'=>$state_id,'business.registration_no' => ""))->result_array();
				}
				//$detail_title = ($series == "berdafter" ? "Berdafter" : "Tidak Berdafter");
			}
			/*else{
				$detail_title = "";
			}*/
		}else if($type == "category"){
			$result = $this->business_model->get_with_person(array('person.category' => $series,'state_id'=> $state_id),'left')->result_array();
		//	$detail_title = $categories[$series-1]['name'];
		}else if($type == "activity"){
			$this->load->model('activity');
			$result = $this->activity->get_business_activity(array('activity.activity_id' => $series,'state_id'=> $state_id),'left')->result_array();
			for($i=0;$i<count($result);$i++){
				$business = $this->business_model->get(array('business_id' => $result[$i]['business_id']))->row_array();
				$result[$i]['business_name'] = $business['business_name'];
				$result[$i]['person_name'] = $result[$i]['name'];
			}
		}else if($type == "work_status"){
			$result = $this->business_model->get_with_person(array('business.work_status' => $series,'state_id'=> $state_id),'left')->result_array();
		}else if($type == "gender"){
			$result = $this->business_person->get(array('gender' => $series,'state_id'=> $state_id),'left')->result_array();
			for($i=0;$i<count($result);$i++){
				$business = $this->business_model->get(array('business_id' => $result[$i]['business_id']))->row_array();
				$result[$i]['business_name'] = $business['business_name'];
				$result[$i]['person_name'] = $result[$i]['name'];
			}
		}else if($type == "provider"){
			$this->load->model('business_courses');
			$result = $this->business_courses->get_all(array('courses.trainer_id' => $series,'business_person.state_id'=> $state_id))->result_array();
			for($i=0;$i<count($result);$i++){
				$business = $this->business_model->get(array('business_id' => $result[$i]['business_id']))->row_array();
				$result[$i]['business_name'] = $business['business_name'];
			}
		}else if($type == "age"){
			$age_category = array(
				array('label' => 'below 31', 'from' => 0, 'to' => 31),
				array('label' => '31 - 40', 'from' => 30, 'to' => 41),
				array('label' => '41 - 50', 'from' => 40, 'to' => 51),
				array('label' => '51 - 60', 'from' => 50, 'to' => 61),
				array('label' => 'Above 60', 'from' => 60, 'to' => 200),
			);
			$result = array();
			$data = $this->business_person->get_current_age(array('current_age >' => $age_category[$series]['from'],'current_age <' => $age_category[$series]['to'],'state_id'=> $state_id),'left')->result_array();
			for($i=0;$i<count($data);$i++){
				$result[$i] = $this->business_model->get_with_person(array('person.business_id'=>$data[$i]['business_id']))->row_array();
			}
		}else if($type == "finance"){
			if($state_id == 0){
				$result = $this->business_model->get_with_person(array('business.financing_by' => $series),'left')->result_array();
			}else{
				$result = $this->business_model->get_with_person(array('business.financing_by' => $series,'person.state_id'=> $state_id),'left')->result_array();
			}	
		}else if($type == "provider_amount"){
			$this->load->model('business_courses');
			$result = $this->business_courses->get_all(array('courses.courses_id' => $series,'state_id'=> $state_id),'left')->result_array();
			for($i=0;$i<count($result);$i++){
				$business = $this->business_model->get(array('business_id' => $result[$i]['business_id']))->row_array();
				$result[$i]['business_name'] = $business['business_name'];
			}
		}else if($type == "count_amount"){
			$this->load->model('business_courses');
			$trainer_id = $state_id;
			if($series != NULL){
				$result = $this->business_courses->get_all(array('courses.trainer_id' => $trainer_id,'courses.courses_id' => $series),1)->result_array();
			}else{
				$result = $this->business_courses->get_all(array('courses.trainer_id' => $trainer_id),1)->result_array();
			}
			for($i=0;$i<count($result);$i++){
				$business = $this->business_model->get(array('business_id' => $result[$i]['business_id']))->row_array();
				$result[$i]['business_name'] = $business['business_name'];
			}
		}
		
		for($i=0;$i<count($result);$i++){
			for($j=0;$j<count($categories);$j++){
				if($result[$i]['category'] == $categories[$j]['category_id']){
					$result[$i]['category_name'] = $categories[$j]['name'];
				}else if($result[$i]['category'] == 0){
					$result[$i]['category_name'] = "";
				}
					
				$state = $this->business_person->get_state(array('state_id' => $result[$i]['state_id']))->row_array();
				$result[$i]['state_name'] = $state['name'];
			}
		}
		//$result['detail_title'] = $detail_title;
		echo json_encode($result);
	}
}
