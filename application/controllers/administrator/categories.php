<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Categories extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "categories /";
		$this->layout = "admin";
		$this->load->model(array('category','role'));
	}

	public function index() {
		$this->scripts = array('jquery.dataTables');
		$menu = $this->menu->get(array('url' => "administrator/categories"))->row_array();
		$data = array('categories' 	=> $this->category->get()->result_array(),
					  'messages' 	=> $this->session->flashdata('form_msg'),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array()
		);
		$this->load->view(ADMIN_DIR.'category/index',$data);
	}
	
	function add(){
		$this->parts['p_title'] = "add /";
		$data = array('mode' => 'ADD',
					'messages' => "",
		);
		$this->load->view(ADMIN_DIR.'category/form',$data);
	}
	
	function edit($id){
		$this->parts['p_title'] = "edit /";
		$data = array('mode' 	=> 'EDIT',
					'category' 	=> $this->category->get(array('category_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
		);
		$this->load->view(ADMIN_DIR.'category/form',$data);
	}
	
	function view($id){
		$this->parts['p_title'] = "view /";
		$data = array('mode' 	=> 'VIEW',
					'category' 	=> $this->category->get(array('category_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
		);
		$this->load->view(ADMIN_DIR.'category/form',$data);
	}
	
	function add_handler(){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
		);
		if($this->category->add($data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/categories'));
	}
	
	function edit_handler($id){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
		);
		if($this->category->edit($id,$data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/categories/edit/'.$id));
	}
	
	function delete($id){
		if($this->category->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/categories/'));
	}
}

