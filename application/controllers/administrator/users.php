<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Users extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array('jquery.validate','administrator/form_validation');
		$this->styles 			= array();
		$this->title 			= "users /";
		$this->layout = "admin";
		$this->load->model(array('course','trainer','user'));
	}

	public function index() {
		//parent::__construct("users");
		$this->scripts = array('jquery.dataTables');
		$menu = $this->menu->get(array('url' => "administrator/users"))->row_array();
		$data = array('users' 	=> $this->user->get(array('role_id !=' => COMPANY_ADMIN))->result_array(),
					  'messages'=> $this->session->flashdata('form_msg'),
					  'role'	=> $this->role->get(array('type' => ADMIN_PAGE))->result_array(),
					  'user_status'=> unserialize(USER_STATUS),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array()
		);
		$this->load->view(ADMIN_DIR.'user/index',$data);
	}
	
	function add(){
		//parent::__construct("users");
		$this->parts['p_title'] = "add /";
		$data = array('mode' => 'ADD',
					'messages' => "",
					'role'	=> unserialize(USER_ROLE),
		);
		$this->load->view(ADMIN_DIR.'user/form',$data);
	}
	
	function edit($id){
		//parent::__construct("users");
		$this->parts['p_title'] = "edit /";
		$data = array('mode' 	=> 'EDIT',
					'user' 	=> $this->user->get(array('user_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'role'	=> $this->role->get(array('type' => ADMIN_PAGE))->result_array(),
		);
		$this->load->view(ADMIN_DIR.'user/form',$data);
	}
	
	function view($id){
		//parent::__construct("users");
		$this->parts['p_title'] = "view /";
		$data = array('mode' 	=> 'VIEW',
					'user' 	=> $this->user->get(array('user_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'role'	=> $this->role->get(array('type' => ADMIN_PAGE))->result_array(),
		);
		$this->load->view(ADMIN_DIR.'user/form',$data);
	}
	
	function add_handler(){
		//parent::__construct("users");
		$data = array(
			'email' 			=> $this->input->post('email'),
			'password'			=> $this->input->post('password'),
			'role_id'			=> $this->input->post('role'),
			'activation_status' => 1,
			'business_id'		=> 0,
		);
		if($this->user->add_admin($data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/users'));
	}
	
	function edit_handler($id){
		//parent::__construct("users");
		$data = array(
			'email' 			=> $this->input->post('email'),
			'password'			=> $this->input->post('password'),
			'role_id'			=> $this->input->post('role'),
			'activation_status' => 1,
			'business_id'		=> 0,
		);
		if($this->user->edit_admin($id,$data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/users/edit/'.$id));
	}
	
	function delete($id){
		//parent::__construct("users");
		if($this->user->delete_admin($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/users/'));
	}
	
	function change_password($business_id) {
        $this->load->model('user');
		$this->parts['p_title'] = "change password /";
        $user = $this->session->userdata(ADMIN_SESSION);
        if ($user != NULL) {
            $data = NULL;
            if ($new_password = $this->input->post('new_password')) {
                $old_password = $this->input->post('old_password');
                if ($this->user->change_password($old_password, $new_password,ADMIN_SESSION)) {
                    $this->session->set_flashdata('form', array('success' => 'true', 'message' => 'Your new password has been saved.'));
                } else {
                    $this->session->set_flashdata('form', array('success' => 'false', 'message' => 'Change password failed. Old password does not match.'));
                }
				redirect(base_url().ADMIN_DIR.'users/change_password/'.$business_id);//should be redirect so that flashdata can be viewed.
            }
            $data = array('form_success' => $this->session->flashdata('form'),
            			  'business_id'	=> $business_id);
            $this->load->view(ADMIN_DIR.'user/form_change_password', $data);
        }
    }
}

