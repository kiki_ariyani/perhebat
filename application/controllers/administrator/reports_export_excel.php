<?php

require_once("common.php");
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Reports_export_excel extends Common {
	function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->model(array('business_person','business_model'));
		$this->title 	= "Administrator";
		$this->scripts 	= array();
		$user = $this->admin_session->get();
		$this->meta 			= array();
		$this->styles 			= array();
	}
	
	function state_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('states');
		$result = array();
		$j = 0;
		for($i=0;$i<count($req_states);$i++){
			foreach($states as $state){
				if($req_states[$i] == 0){
					$value = $this->business_model->get_for_excel(array('person.state_id'=>$state['state_id']));
					$result[$j] = $value;
					$j++;
				}else if($req_states[$i] == $state['state_id']){
					$value = $this->business_model->get_for_excel(array('person.state_id'=> $req_states[$i]));
					$result[$j] = $value;
					$j++;
				}
			}
		}
		
		$l = 0;
		$content_value = array();
		foreach($result as $row_state){
			foreach($row_state as $row){
				$content_value[$l] = $row;
			$l++;}
		}
		
		$contents = $content_value;
		//$contents = array(array('business_id' =>1));
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State");
		$this->export("Company by State","Company by State",$columns,$fields,$contents,"state");
	}
	
	function status_handler(){
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('status');
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$value_not_reg_arr = array();
		$value_reg_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value_not_reg = $this->business_model->get_for_excel(array('business.registration_no' => "",'person.state_id'=> $state['state_id']),'left');
				$value_reg = $this->business_model->get_for_excel(array('business.registration_no !=' => "",'person.state_id'=> $state['state_id']),'left');
				$value_not_reg_arr[$j] = $value_not_reg;
				$value_reg_arr[$j] = $value_reg;
				$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						$value_not_reg = $this->business_model->get_for_excel(array('business.registration_no' => "",'person.state_id'=> $req_states[$i]),'left');
						$value_reg = $this->business_model->get_for_excel(array('business.registration_no !=' => "",'person.state_id'=> $req_states[$i]),'left');
						$value_not_reg_arr[$j] = $value_not_reg;
						$value_reg_arr[$j] = $value_reg;
						$j++;
					}
				}
			}
		}
		
		$l = 0;
		$content_value = array();
		foreach($value_reg_arr as $row_state){
			foreach($row_state as $row){
				$content_value[$l] = $row;
				$content_value[$l]['status'] = "Berdaftar";
			$l++;}
		}
		$m = count($content_value);
		foreach($value_not_reg_arr as $row_state){
			foreach($row_state as $row){
				$content_value[$m] = $row;
				$content_value[$m]['status'] = "Tidak Berdaftar";
			$m++;}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Status");
		$this->export("Company by Registration Status","Company by Registration Status",$columns,$fields,$contents,"status");
	}
	
	function category_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$business_cat = unserialize(BUSINESS_PERSON_CATEGORY);
		$req_states = $this->input->post('category');
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($business_cat);$k++){
					$value[$k] = $this->business_model->get_for_excel(array('person.category' => $business_cat[$k]['category_id'],'person.state_id'=> $state['state_id']),'left');
				}
				$value_arr[$j] = $value;
				$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($business_cat);$k++){
							$value[$k] = $this->business_model->get_for_excel(array('person.category' => $business_cat[$k]['category_id'],'person.state_id'=> $req_states[$i]),'left');
						}
						$value_arr[$j] = $value;
						$j++;
					}
				}
			}
		}
		
		$m = 0;
		$content_value = array();
		foreach($value_arr as $row_category){
			foreach($row_category as $row_state){
				foreach($row_state as $row){
					$content_value[$m] = $row;
				$m++;} 
			}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State");
		$this->export("Company by Kategori Perkhidmatan","Company by Kategori Perkhidmatan",$columns,$fields,$contents,"category");
	}
	
	function work_status_handler(){
		$this->load->model('activity');
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$work_status =  unserialize(WORK_STATUS);
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($work_status);$k++){
					$value[$k] = $this->business_model->get_for_excel(array('business.work_status' => $work_status[$k]['status_id'],'person.state_id'=> $state['state_id']),'left',"work_status");
				}
				$value_arr[$j] = $value;
				$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($work_status);$k++){
							$value[$k] = $this->business_model->get_for_excel(array('business.work_status' => $work_status[$k]['status_id'],'person.state_id'=> $req_states[$i]),'left',"work_status");
						}
						$value_arr[$j] = $value;
						$j++;
					}
				}
			}
		}
		
		$m = 0;
		$content_value = array();
		foreach($value_arr as $row_category){
			foreach($row_category as $row_state){
				foreach($row_state as $row){
					$content_value[$m] = $row;
					$content_value[$m]['work_status']=($row['work_status'] == 1 ? "Bermajikan" : "Berniaga");
				$m++;} 
			}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Work Status");
		$this->export("Company by Work Status","Company by Work Status",$columns,$fields,$contents,"work_status");
	}
	
	function gender_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$gender =  unserialize(GENDER);
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($gender);$k++){
					$value[$k] = $this->business_model->get_for_excel(array('gender' => $gender[$k]['gender_id'],'person.state_id'=> $state['state_id']),'left',"gender");
				}
				$value_arr[$j] = $value;
				$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($gender);$k++){
							$value[$k] = $this->business_model->get_for_excel(array('gender' => $gender[$k]['gender_id'],'person.state_id'=> $req_states[$i]),'left',"gender");
						}
						$value_arr[$j] = $value;
						$j++;
					}
				}
			}
		}
		
		$m = 0;
		$content_value = array();
		foreach($value_arr as $row_category){
			foreach($row_category as $row_state){
				foreach($row_state as $row){
					$content_value[$m] = $row;
					$content_value[$m]['gender'] = ($row['gender'] == 1 ? "Male" : "Female");
				$m++;} 
			}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Gender");
		$this->export("Company by Gender","Company by Gender",$columns,$fields,$contents,"gender");
	}
	
	function age_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$age_category = array(
			array('label' => 'below 31', 'from' => 0, 'to' => 31),
			array('label' => '31 - 40', 'from' => 30, 'to' => 41),
			array('label' => '41 - 50', 'from' => 40, 'to' => 51),
			array('label' => '51 - 60', 'from' => 50, 'to' => 61),
			array('label' => 'Above 60', 'from' => 60, 'to' => 200),
		);
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value = array();
				for($k=0;$k<count($age_category);$k++){
					$value[$k] = $this->business_person->get_current_age_for_excel(array('current_age >' => $age_category[$k]['from'],'current_age <' => $age_category[$k]['to'],'state_id'=> $state['state_id']),'left');
				}
				$value_arr[$j] = $value;
				$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						for($k=0;$k<count($age_category);$k++){
							$value[$k] = $this->business_person->get_current_age_for_excel(array('current_age >' => $age_category[$k]['from'],'current_age <' => $age_category[$k]['to'],'state_id'=> $req_states[$i]),'left');
						}
						$value_arr[$j] = $value;
						$title = $this->business_person->get_state(array('state_id' => $req_states[$i]))->row_array();
						$result['title'][$j] = $title['name'];
						$j++;
					}
				}
			}
		}
		
		$m = 0;
		$content_value = array();
		foreach($value_arr as $row_category){
			foreach($row_category as $row_state){
				foreach($row_state as $row){
					$content_value[$m] = $row;
				$m++;} 
			}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Age");
		$this->export("Company by Age","Company by Age",$columns,$fields,$contents,"age");
	}
	
	function activity_handler(){
		$this->load->model('activity');
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$activities = $this->activity->get()->result_array();
		$req_states = $this->input->post('activity');
	
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$activity_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value_arr[$j] = $this->activity->get_business_activity_for_excel(array('person.state_id'=> $state['state_id']),'left');
				$j++;
			}
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						$value_arr[$j] = $this->activity->get_business_activity_for_excel(array('person.state_id'=> $req_states[$i]),'left');
						$j++;
					}
				}
			}
		}
		$m = 0;
		$content_value = array();
		foreach($value_arr as $row_category){
			foreach($row_category as $row_state){
					$content_value[$m] = $row_state;
				$m++;
			}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Activity");
		$this->export("Company Activity","Company Activity",$columns,$fields,$contents,"activity");
	}
	
	function trainer_handler(){
		$this->layout = FALSE;
		$this->load->model(array('trainer','business_courses'));
		$states = $this->business_person->get_state()->result_array();
		$req_states = $this->input->post('state');
		$trainers = $this->trainer->get()->result_array();
		
		if (in_array(0, $req_states)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		
		$result = array();
		$value_arr = array();
		$j = 0;
		
		if($is_all == true){
			foreach($states as $state){
				$value_arr[$j] = $this->business_courses->get_business_course_for_excel(array('person.state_id'=> $state['state_id']),'left');
				$j++;
			}
			
		}else if($is_all == false){
			for($i=0;$i<count($req_states);$i++){
				foreach($states as $state){
					if($req_states[$i] == $state['state_id']){
						$value_arr[$j] = $this->business_courses->get_business_course_for_excel(array('person.state_id'=> $req_states[$i]),'left');
						$j++;
					}
				}
			}
		}
		
		$m = 0;
		$content_value = array();
		foreach($value_arr as $row_category){
			foreach($row_category as $row_state){
					$content_value[$m] = $row_state;
				$m++;
			}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Provider");
		$this->export("Company by Provider","Company by Provider",$columns,$fields,$contents,"provider");
	}
	
	function finance_handler(){
		$this->layout = FALSE;
		$states = $this->business_person->get_state()->result_array();
		$financing = unserialize(BUSINESS_FINANCING_BY);
		$req_states = $this->input->post('state');
		$value = array();
		$result = array();
		
		for($i=0;$i<count($req_states);$i++){
			for($j=0;$j<count($financing);$j++){
				if($req_states[$i] == 0){
					$value[$j] = $this->business_model->get_for_excel(array('business.financing_by' => $financing[$j]['financing_by_id']),'left',"finance");
				}else{
					$value[$j] = $this->business_model->get_for_excel(array('business.financing_by' => $financing[$j]['financing_by_id'],'person.state_id'=> $req_states[$i]),'left',"finance");
				}
			}
		}
	
		$m = 0;
		$content_value = array();
		foreach($value as $row_category){
			foreach($row_category as $row){
				$content_value[$m] = $row;
				$content_value[$m]['financing_by'] = ($row['financing_by'] == 1 ? "SENDIRI" : ($row['financing_by'] == 2 ? "TEKUN" : ""));
				$m++;
			}
		}
		$contents = $content_value;
		if($content_value){
			$fields = array_keys($content_value[0]);
		}else{
			$fields = "";
		}
		$columns = array("Business Name","Person Name","Kategori Perkhidmatan","Registration No","Email","State","Finance By");
		$this->export("Company by Pembiayaan","Company by Pembiayaan",$columns,$fields,$contents,"finance");
	}
	
	function export($file_name,$title,$columns,$fields,$contents,$url = NULL) {
		if(!$fields) {
			echo 'No data available. <a href="'.base_url().ADMIN_DIR.'reports/'.$url.'">Back</a>'; die();
		}
		
		$this->load->library('PHPExcel');
		//$old_include_path = get_include_path();
		//set_include_path(APPPATH . 'libraries/PHPExcel');

		//require_once('PHPExcel.php');
		//require_once('PHPExcel/Writer/Excel5.php');

		//set_include_path($old_include_path);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
		header('Cache-Control: max-age=0');

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("PERHEBAT");
		$objPHPExcel->getProperties()->setLastModifiedBy("PERHEBAT");
		$objPHPExcel->getProperties()->setTitle($title);
		$objPHPExcel->getProperties()->setSubject("PERHEBAT");
		
		#Column Width
		$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
		
		#Column Title Style
		$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A1:I1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		#Wrap Text
		$objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true); 
		
		// Add some data
		$objPHPExcel->setActiveSheetIndex(0);
		$col = 0;
		$row = 1;

		foreach($columns as $column) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
			$col++;
		}
		$col = 0;
		$row++;
		foreach($contents as $content) {
			foreach($fields as $field) {
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
				$col++;
			}
			$col = 0;
			$row++;
		}
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

		// Save Excel 2007 file
		//$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}

	function provider_amount_handler(){
		$this->layout = FALSE;
		$this->load->model(array('trainer','business_courses'));
		$req_states = $this->input->post('state');
		$req_provider = $this->input->post('provider');

		$data = $this->business_courses->get_provider_amount_for_excel($req_states,$req_provider);

		$file_name = "Company by Provider Amount - ".date('d-m-Y');
		$title = "Company by Provider Amount";
		$no=1;
		foreach($data as $i=>$row){
			$content[$i]['no'] = $no;
			$content[$i]['business_name'] = $row['business_name'];
			$content[$i]['name'] = $row['person_name'];
			$content[$i]['registration_no'] = $row['registration_no'];
			$content[$i]['email_web'] = $row['email_web'];
			$content[$i]['courses_name'] = $row['courses_name'];
			$content[$i]['provider'] = $row['trainer_name'];
			$content[$i]['cost'] = $row['cost'];
			$content[$i]['state'] = $row['state_name'];
			$no++;
		}
		$columns = array("No","Business Name","Name","Registration No","Email","Courses Name","Provider","Cost","State");
		$fields = array_keys($content[0]);
		$this->export($file_name,$title,$columns,$fields,$content);
	}

	function course_all_amount_handler(){
		$this->layout = FALSE;
		$this->load->model(array('trainer','business_courses'));
		$req_states = array(0);
		$req_provider = $this->input->post('provider');
		$content = array();
		$fields = array();

		$data = $this->business_courses->get_provider_amount_for_excel($req_states,$req_provider,1);
	
		$file_name = "Company by Courses Amount - ".date('d-m-Y');
		$title = "Company by Courses Amount";
		$no=1;
		foreach($data as $i=>$row){
			$content[$i]['no'] = $no;
			$content[$i]['business_name'] = $row['business_name'];
			$content[$i]['name'] = $row['person_name'];
			$content[$i]['registration_no'] = $row['registration_no'];
			$content[$i]['email_web'] = $row['email_web'];
			$content[$i]['courses_name'] = $row['courses'];
			$content[$i]['provider'] = $row['trainer_name'];
			$content[$i]['cost'] = $row['cost'];
			$no++;
		}
		$columns = array("No","Business Name","Name","Registration No","Email","Courses Name","Provider","Cost");
		if($content){
			$fields = array_keys($content[0]);
		}
		$this->export($file_name,$title,$columns,$fields,$content);
	}

	function export_manual(){
		$data = $this->business_model->get_for_excel_manual();
		$gender = unserialize(GENDER);
		$religion = unserialize(RELIGION);
		$race = unserialize(RACE);
		$work_status = unserialize(WORK_STATUS);
		$financing = unserialize(BUSINESS_FINANCING_BY);

		$file_name = "PERHEBAT_USER_".date('d-m-Y');
		$title = "PERHEBAT USER LIST";

		$no=1;
		foreach($data as $i=>$row){
			$content[$i]['no'] = $no;
			$content[$i]['name'] = $row['name'];
			$content[$i]['category'] = $row['category_name'];
			$content[$i]['ic_no'] = $row['ic_no'];
			$content[$i]['age'] = $row['age'];
			$content[$i]['gender'] = ($row['gender'] != 0 ? $gender[$row['gender']-1]['name'] : '');
			$content[$i]['religion'] = ($row['religion'] != 0 ? $religion[$row['religion']-1]['name'] : '');
			$content[$i]['education_level'] = $row['education_level'];
			$content[$i]['service_no'] = $row['army_no'];
			$content[$i]['address'] = $row['address'];
			$content[$i]['telp_no'] = $row['telp_no'];
			$content[$i]['state'] = $row['state_name'];
			$content[$i]['race'] = ($row['race'] != 0 ? $race[$row['race']-1]['name'] : '');
			$content[$i]['status_atm'] = ($row['status_atm'] == 1 ? "Bakal Pesara ATM" : ($row['status_atm'] == 2 ? 'Pesara ATM' : ''));
			$content[$i]['geo_location'] = $row['geo_location'];
			$content[$i]['business_name'] = $row['business_name'];
			$content[$i]['work_status'] = ($row['work_status'] != 0 ? $work_status[$row['work_status']-1]['name'] : '');
			$content[$i]['reg_no'] = $row['registration_no'];
			$content[$i]['reg_date'] = $row['registration_date'];
			$content[$i]['business_address'] = $row['business_address'];
			$content[$i]['email'] = $row['email_web'];
			$content[$i]['business_activity'] = $row['activity'];
			$content[$i]['main_product'] = $row['main_product'];
			$content[$i]['account_type'] = $row['account_type'];
			$content[$i]['financing_by'] = ($row['financing_by'] != 0 ? $financing[$row['financing_by']-1]['name'] : '');
			$content[$i]['courses'] = $row['courses'];
			$no++;
		}
			$columns = array("No","Name","Category","IC No","Age","Gender","Religion","Education level","Service No","Address","Telp No","State","Race","Status ATM","Geo Location","Business name","Work Status","Registration No","Registration Date","Business Address","Email/Web","Business Activity","Main Product","Account Type","Financing By","Courses");
			$fields = array_keys($content[0]);
			$this->export($file_name,$title,$columns,$fields,$content);
	}
}
