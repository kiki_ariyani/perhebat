<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Articles extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array('tiny_mce/tiny_mce','mce_loader');
		$this->styles 			= array();
		$this->title 			= "articles /";
		$this->layout = "admin";
		$this->load->model(array('article','category','role'));
	}

	public function index() {
		$this->scripts = array('jquery.dataTables');
		$articles = $this->article->get(NULL,'priority ASC')->result_array();
		$menu = $this->menu->get(array('url' => "administrator/articles"))->row_array();
		for($i=0;$i<count($articles);$i++){
			$category = $this->category->get(array('category_id' => $articles[$i]['category_id']))->row_array();
			$articles[$i]['category_name'] = $category['name'];
		}
		$data = array('articles' 	=> $articles,
					  'messages' 	=> $this->session->flashdata('form_msg'),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array()
		);
		$this->load->view(ADMIN_DIR.'article/index',$data);
	}
	
	function add(){
		$this->parts['p_title'] = "add /";
		$data = array('mode' => 'ADD',
					'messages' => "",
					'categories' => $this->category->get()->result_array(),
		);
		$this->load->view(ADMIN_DIR.'article/form',$data);
	}
	
	function edit($id){
		$this->parts['p_title'] = "edit /";
		$data = array('mode' 	=> 'EDIT',
					'article' 	=> $this->article->get(array('article_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'categories' => $this->category->get()->result_array(),
		);
		$this->load->view(ADMIN_DIR.'article/form',$data);
	}
	
	function view($id){
		$this->parts['p_title'] = "view /";
		$data = array('mode' 	=> 'VIEW',
					'article' 	=> $this->article->get(array('article_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'categories' => $this->category->get()->result_array(),
		);
		$this->load->view(ADMIN_DIR.'article/form',$data);
	}
	
	function add_handler(){
		$data = array(
			'title' 	=> $this->input->post('title'),
			'headline'	=> $this->input->post('headline'),
			'content'	=> $this->input->post('content'),
			'publish'	=> $this->input->post('publish'),
			'priority'	=> $this->input->post('priority'),
			'category_id'=> $this->input->post('category'),
		);
		if($this->article->add($data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/articles'));
	}
	
	function edit_handler($id){
		$data = array(
			'title' 	=> $this->input->post('title'),
			'headline'	=> $this->input->post('headline'),
			'content'	=> $this->input->post('content'),
			'publish'	=> $this->input->post('publish'),
			'priority'	=> $this->input->post('priority'),
			'category_id'=> $this->input->post('category'),
		);
		if($this->article->edit($id,$data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/articles/edit/'.$id));
	}
	
	function delete($id){
		if($this->article->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/articles/'));
	}
	
	function set_featured($id){
		if($this->article->edit($id,array('featured' => 1))){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/articles/'));
	}
	
	function set_unfeatured($id){
		if($this->article->edit($id,array('featured' => 0))){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/articles/'));
	}
	
	function set_publish($id){
		if($this->article->edit($id,array('publish' => 1))){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/articles/'));
	}
	
	function set_unpublish($id){
		if($this->article->edit($id,array('publish' => 0))){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/articles/'));
	}
}

