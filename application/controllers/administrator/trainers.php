<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Trainers extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "trainers /";
		$this->layout = "admin";
		$this->load->model(array('trainer','role'));
	}

	public function index() {
		$this->scripts = array('jquery.dataTables');
		$menu = $this->menu->get(array('url' => "administrator/trainers"))->row_array();
		$data = array('trainers' 	=> $this->trainer->get()->result_array(),
					  'messages' 	=> $this->session->flashdata('form_msg'),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array()
		);
		$this->load->view(ADMIN_DIR.'trainer/index',$data);
	}
	
	function add(){
		$this->parts['p_title'] = "add /";
		$data = array('mode' => 'ADD',
					'messages' => "",
		);
		$this->load->view(ADMIN_DIR.'trainer/form',$data);
	}
	
	function edit($id){
		$this->parts['p_title'] = "edit /";
		$data = array('mode' 	=> 'EDIT',
					'trainer' 	=> $this->trainer->get(array('trainer_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
		);
		$this->load->view(ADMIN_DIR.'trainer/form',$data);
	}
	
	function view($id){
		$this->parts['p_title'] = "view /";
		$data = array('mode' 	=> 'VIEW',
					'trainer' 	=> $this->trainer->get(array('trainer_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
		);
		$this->load->view(ADMIN_DIR.'trainer/form',$data);
	}
	
	function add_handler(){
		$data = array(
			'name' 	=> $this->input->post('name'),
			'code'	=> $this->input->post('code'),
		);
		if($this->trainer->add($data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/trainers'));
	}
	
	function edit_handler($id){
		$data = array(
			'name' 	=> $this->input->post('name'),
			'code'	=> $this->input->post('code'),
		);
		if($this->trainer->edit($id,$data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/trainers/edit/'.$id));
	}
	
	function delete($id){
		if($this->trainer->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/trainers/'));
	}
}

