<?php 

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Common extends CI_Controller {
	function __construct($module=null) {
		parent::__construct();
		
		$this->load->model(array('admin_session','menu','role'));
		$this->load->helper(array('language','url'));
		$user = $this->admin_session->get(ADMIN_SESSION); 
		if (!$user) {
			if ($module != "site"){
				redirect(site_url("administrator/site/no_access/" ));
			}
		}
		
		$url = $this->uri->uri_string();
		$module = $this->uri->segment(2);
		$method  = $this->uri->segment(3);
		$my_menu_url 	= $this->menu->get(array('url' => $url))->row_array();
		$my_menu_module	= $this->menu->get(array('url' => "administrator/".$module))->row_array();
		
		if($my_menu_url){
			$my_menu = $my_menu_url;
			$my_menu_access = $this->role->get_role_setting(array('role_id' => $user['role_id'],'menu_id' => $my_menu['menu_id']))->row_array();
		}else if($my_menu_module){
			$my_menu = $my_menu_module;
			$my_menu_access = $this->role->get_role_setting(array('role_id' => $user['role_id'],'menu_id' => $my_menu['menu_id']))->row_array();
		}else{
			$my_menu_access = array();
		}
		
		$methods = array('view', 'edit', 'create');
		if($method == NULL || $method == 'index' || $method == 'view'){
			$my_con_mthd = 'view';
		}elseif($method == 'add'){
			$my_con_mthd = 'create';
		}elseif($method == 'delete'){
			$my_con_mthd = 'create';
		}elseif($method == 'edit'){
			$my_con_mthd = 'edit';
		}else{
			if($module == 'reports' || $module == 'search'){
				$my_con_mthd = 'view';
			}else{
				$my_con_mthd = $method;
			}
		}
	
		if($my_menu_access){
			if($my_con_mthd != NULL && in_array($my_con_mthd, $methods)){
				if($my_menu_access[$my_con_mthd] == 0){
					redirect(site_url('administrator/site/no_access')); Die;
				}
			}
		}

		$this->layout 			= "admin";
		$menu = $this->menu->get_menu_by_role($user['role_id']);
		$this->parts['head'] 	= $this->load->view('administrator/partial/head', array('username' => $user['email'],'role' => $user['role_id'], 'menu' => $menu), true);
		$this->parts['footer'] 	= $this->load->view('administrator/partial/footer', null, true);
	}
	 
	function set_lang(){
		$lang 		= $this->input->post('language');
		$controller	= $this->input->post('cntrl');
		$this->session->set_userdata('language', $lang);
		redirect(site_url($controller));
	}
}