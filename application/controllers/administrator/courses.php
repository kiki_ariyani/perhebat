<?php
require_once("common.php");
if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Courses extends Common {
	function __construct() {
		parent::__construct();

		$this->load->library('session');
//		$this->load->model('admin_session');
		$this->user_sess = $this->admin_session->get();
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "courses /";
		$this->layout = "admin";
		$this->load->model(array('course','trainer','role'));
	}

	public function index() {
		$this->scripts = array('jquery.dataTables');
		$menu = $this->menu->get(array('url' => "administrator/courses"))->row_array();
		$data = array('courses' 	=> $this->course->get()->result_array(),
					  'messages' 	=> $this->session->flashdata('form_msg'),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array()
		);
		$this->load->view(ADMIN_DIR.'courses/index',$data);
	}
	
	function add(){
		$this->parts['p_title'] = "add /";
		$data = array('mode' => 'ADD',
					'messages' => "",
					'trainers' => $this->trainer->get()->result_array(),
		);
		$this->load->view(ADMIN_DIR.'courses/form',$data);
	}
	
	function edit($id){
		$this->parts['p_title'] = "edit /";
		$data = array('mode' 	=> 'EDIT',
					'courses' 	=> $this->course->get(array('courses_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'trainers' => $this->trainer->get()->result_array(),
		);
		$this->load->view(ADMIN_DIR.'courses/form',$data);
	}
	
	function view($id){
		$this->parts['p_title'] = "view /";
		$data = array('mode' 	=> 'VIEW',
					'courses' 	=> $this->course->get(array('courses_id' => $id))->row_array(),
					'id'		=> $id,
					'messages'	=> $this->session->flashdata('form_msg'),
					'trainers' => $this->trainer->get()->result_array(),
		);
		$this->load->view(ADMIN_DIR.'courses/form',$data);
	}
	
	function add_handler(){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
			'objective'		=> $this->input->post('objective'),
			'trainer_id'	=> $this->input->post('trainer'),
			'cost'			=> $this->input->post('cost')
		);
		if($this->course->add($data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/courses'));
	}
	
	function edit_handler($id){
		$data = array(
			'name' 			=> $this->input->post('name'),
			'description'	=> $this->input->post('description'),
			'objective'		=> $this->input->post('objective'),
			'trainer_id'	=> $this->input->post('trainer'),
			'cost'			=> $this->input->post('cost')
		);
		if($this->course->edit($id,$data)){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		redirect(site_url('administrator/courses/edit/'.$id));
	}
	
	function delete($id){
		if($this->course->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url('administrator/courses/'));
	}
	
	function get_detail($courses_id){
		$this->layout = FALSE;
		$result = $this->course->get(array('courses_id' => $courses_id))->row_array();
		echo json_encode($result);
	}
}

