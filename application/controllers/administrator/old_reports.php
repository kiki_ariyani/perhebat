<?php

require_once("common.php");
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Old_reports extends Common {

	function __construct() {
		parent::__construct("articles");
		require_once('common.php');
		$this->load->helper('form');
		$this->load->model(array('business_person','business_model'));
		$this->title 	= "Administrator";
		$this->scripts 	= array('fusionchart/FusionCharts','jquery.form','administrator/report');
		$this->meta 			= array();
		$this->styles 			= array();
	}

	public function index(){
		
	}
	
	public function state(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->load->view('administrator/report/by_state',$data);
	}
	
	public function get_state_report() {
		$this->load->library('FusionCharts');
		$this->layout = false;
		//get data category
		$states = $this->business_person->get_state()->result_array();
		// Instantiate the FusionCharts object 
		$FC = new FusionCharts("column3D","600","300");
		$FC->setSwfPath(base_url()."assets/swf/");
		// specify the graph parameters
		$strParam="caption=State Report ;xAxisName=States;yAxisName=Number of Company;numberPrefix=;decimalPrecision=0;formatNumberScale=1";
		$FC->setChartParams($strParam);
		// specify the data bsaed on the $year parameter
		$state_id = $this->input->post('states');
		
		if($state_id != NULL){
			for($i=0;$i<count($state_id);$i++){
				foreach($states as $state){
					if($state['state_id'] == $state_id[$i]){
						$count_state = $this->business_person->get(array('state_id'=>$state_id[$i]))->num_rows();
						$state_name = $this->business_person->get_state(array('state_id' => $state_id[$i]))->row_array();
						$FC->addChartData($count_state,"name=".$state_name['name']); 
					}else if($state_id[$i] == 0){
						$count_state = $this->business_person->get(array('state_id'=>$state['state_id']))->num_rows();
						$state_name = $this->business_person->get_state(array('state_id' => $state['state_id']))->row_array();
						$FC->addChartData($count_state,"name=".$state_name['name']); 
					}
				}
			}
		}
			
		$strXML=$FC->getXML();
		echo $strXML;
	}
	
	public function status(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->load->view('administrator/report/status',$data);
	}
	
	public function get_status_report() {
		$this->load->library('FusionCharts');
		$this->layout = false;
		// Instantiate the FusionCharts object 
		$FC = new FusionCharts("MSColumn3D","600","300");
		$FC->setSwfPath(base_url()."assets/swf/");
		// specify the graph parameters
		$strParam="caption=Status Report;xAxisName=Status;yAxisName=Number of Company;numberPrefix=;decimalPrecision=0;formatNumberScale=1";
		$FC->setChartParams($strParam);
		// specify the data bsaed on the $year parameter
		$ids = $this->input->post('status');
		$states = $this->business_person->get_state()->result_array();
		if (in_array(0, $ids)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		if($is_all == true){
			$j = 0;
			foreach($states as $state){
				$FC->addCategory($state['name']);
			}
			$FC->addDataset("Tidak berdaftar");
			foreach($states as $state){
				$count_status_no = $this->business_model->get_with_person(array('business.registration_no' => "",'person.state_id' =>$state['state_id']),'left')->num_rows();
				$FC->addChartData($count_status_no,"name= Syarikat tidak berdaftar");
			}
			
			$FC->addDataset("Berdaftar");
			foreach($states as $state){
				$count_status = $this->business_model->get_with_person(array('business.registration_no !=' => "",'person.state_id' =>$state['state_id']),'left')->num_rows();
				$FC->addChartData($count_status,"name= Syarikat berdaftar");
			}
		}else if($is_all == false){
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$FC->addCategory($state['name']);
					}
				}
			}
					
			$FC->addDataset("Tidak berdaftar");
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$count_status_no = $this->business_model->get_with_person(array('business.registration_no' => "",'person.state_id' =>$ids[$i]),'left')->num_rows();
						$FC->addChartData($count_status_no,"name= Syarikat tidak berdaftar");
					}
				}
			}
			
			$FC->addDataset("Berdaftar");
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$count_status = $this->business_model->get_with_person(array('business.registration_no !=' => "",'person.state_id' =>$ids[$i]),'left')->num_rows();
						$FC->addChartData($count_status,"name= Syarikat berdaftar");
					}
				}
			}
		}
		$strXML=$FC->getXML(); 
		echo $strXML;
	}
	
	public function finance(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->load->view('administrator/report/finance',$data);
	}
	
	public function get_finance_report() {
		$this->load->library('FusionCharts');
		$this->layout = false;
		// Instantiate the FusionCharts object 
		$FC = new FusionCharts("Pie3D","600","300");
		$FC->setSwfPath(base_url()."assets/swf/");
		// specify the graph parameters
		$strParam="caption=Pembiayaan;bgColor=F1f1f1;decimalPrecision=0;showPercentageValues=1;showNames=1;numberPrefix=;showValues=1;showPercentageInLabel=1;pieYScale=45;pieBorderAlpha=40;pieFillAlpha=70;pieSliceDepth=15;pieRadius=100;showLegend=1";
		$FC->setChartParams($strParam);
		// specify the data bsaed on the $year parameter
		$ids = $this->input->post('status');
		if (in_array(1, $ids) && in_array(2, $ids)) {
			$ids = array(0 => 0);
		}

		for($i=0;$i<count($ids);$i++){
			if($ids[$i] == 0){
				$count_status_no = $this->business_model->get_with_person(array('business.financing_by' => 1),'left')->num_rows();
				$count_status = $this->business_model->get_with_person(array('business.financing_by' => 2),'left')->num_rows();
				
				if($count_status_no != 0 || $count_status != 0){
					$FC->addChartData($count_status_no,"color=0099FF;name= Pembiayaan Sendiri, ".$count_status_no." Usaha");
					$FC->addChartData($count_status,"color=FCEC0D;name= Pembiayaan Tekun, ".$count_status." Usaha");
				}
			}else{
				$count_status_no = $this->business_model->get_with_person(array('business.financing_by' => 1,'person.state_id'=> $ids[$i]),'left')->num_rows();
				$count_status = $this->business_model->get_with_person(array('business.financing_by' => 2,'person.state_id'=> $ids[$i]),'left')->num_rows();
				
				if($count_status_no != 0 || $count_status != 0){
					$FC->addChartData($count_status_no,"color=0099FF;name= Pembiayaan Sendiri, ".$count_status_no." Usaha");
					$FC->addChartData($count_status,"color=FCEC0D;name= Pembiayaan Tekun, ".$count_status." Usaha");
				}
			}
		}
		$strXML=$FC->getXML(); 
		echo $strXML;
	}
	
	public function category(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->load->view('administrator/report/category',$data);
	}
	
	public function get_category_report() {
		$this->load->library('FusionCharts');
		$this->layout = false;
		// Instantiate the FusionCharts object 
		$FC = new FusionCharts("MSColumn3D","800","300");
		$FC->setSwfPath(base_url()."assets/swf/");
		// specify the graph parameters
		$strParam="caption=Status Report;xAxisName=Status;yAxisName=Number of Company;numberPrefix=;decimalPrecision=0;formatNumberScale=1";
		$FC->setChartParams($strParam);
		// specify the data bsaed on the $year parameter
		$ids = $this->input->post('category');
		$states = $this->business_person->get_state()->result_array();
		if (in_array(0, $ids)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		if($is_all == true){
			$j = 0;
			foreach($states as $state){
				$FC->addCategory($state['name']);
			}
			$FC->addDataset("ARMY");
			foreach($states as $state){
				$count_status_no = $this->business_model->get_with_person(array('person.category' => 1,'person.state_id' =>$state['state_id']),'left')->num_rows();
				$FC->addChartData($count_status_no,"name= ARMY");
			}
			
			$FC->addDataset("AIRFORCE");
			foreach($states as $state){
				$count_status = $this->business_model->get_with_person(array('person.category' => 2,'person.state_id' =>$state['state_id']),'left')->num_rows();
				$FC->addChartData($count_status,"name= AIRFORCE");
			}
			
			$FC->addDataset("NAVY");
			foreach($states as $state){
				$count_status = $this->business_model->get_with_person(array('person.category' => 3,'person.state_id' =>$state['state_id']),'left')->num_rows();
				$FC->addChartData($count_status,"name= NAVY");
			}
		}else if($is_all == false){
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$FC->addCategory($state['name']);
					}
				}
			}
					
			$FC->addDataset("ARMY");
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$count_status_no = $this->business_model->get_with_person(array('person.category' => 1,'person.state_id' =>$ids[$i]),'left')->num_rows();
						$FC->addChartData($count_status_no,"name= ARMY");
					}
				}
			}
			
			$FC->addDataset("AIRFORCE");
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$count_status = $this->business_model->get_with_person(array('person.category' => 2,'person.state_id' =>$ids[$i]),'left')->num_rows();
						$FC->addChartData($count_status,"name= Syarikat berdaftar");
					}
				}
			}
			
			$FC->addDataset("NAVY");
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$count_status = $this->business_model->get_with_person(array('person.category' => 3,'person.state_id' =>$ids[$i]),'left')->num_rows();
						$FC->addChartData($count_status,"name= NAVY");
					}
				}
			}
		}
		$strXML=$FC->getXML(); 
		echo $strXML;
	}
	
	public function activity(){
		$data = array(
			'states' => $this->business_person->get_state()->result_array(),
		);
		$this->load->view('administrator/report/activity',$data);
	}
	
	public function get_activity_report() {
		$this->load->library('FusionCharts');
		$this->layout = false;
		$this->load->model('activity');
		// Instantiate the FusionCharts object 
		$FC = new FusionCharts("MSColumn3D","800","300");
		$FC->setSwfPath(base_url()."assets/swf/");
		// specify the graph parameters
		$strParam="caption=Status Report;xAxisName=Status;yAxisName=Number of Company;numberPrefix=;decimalPrecision=0;formatNumberScale=1";
		$FC->setChartParams($strParam);
		// specify the data bsaed on the $year parameter
		$ids = $this->input->post('activity');
		$states = $this->business_person->get_state()->result_array();
		$activities = $this->activity->get()->result_array();
		if (in_array(0, $ids)) {
			$is_all = true;
		}else{
			$is_all = false;
		}
		if($is_all == true){
			$j = 0;
			foreach($states as $state){
				$FC->addCategory($state['name']);
			}
			foreach($activities as $activity){
				$FC->addDataset($activity['name']);
				foreach($states as $state){
					$count_status_no = $this->activity->get_business_activity(array('activity.activity_id' => $activity['activity_id'],'person.state_id' =>$state['state_id']),'left')->num_rows();
					$FC->addChartData($count_status_no,"name=".$activity['name']);
				}
			}
		}else if($is_all == false){
			for($i=0;$i<count($ids);$i++){
				foreach($states as $state){
					if($state['state_id'] == $ids[$i]){
						$FC->addCategory($state['name']);
					}
				}
			}
			
			foreach($activities as $activity){
				$FC->addDataset($activity['name']);
				for($i=0;$i<count($ids);$i++){
					foreach($states as $state){
						if($state['state_id'] == $ids[$i]){
							$count_status_no = $this->activity->get_business_activity(array('activity.activity_id' => $activity['activity_id'],'person.state_id' =>$ids[$i]),'left')->num_rows();
							$FC->addChartData($count_status_no,"name=".$activity['name']);
						}
					}
				}
			}
		}
		$strXML=$FC->getXML(); 
		echo $strXML;
	}

	function example(){
		$this->load->view('administrator/report/example');
	}
}
