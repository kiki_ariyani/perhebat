<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("site_common.php");

class Message extends Site_common {
	function __construct() {
		parent::__construct("message");
		$this->load->model('message_model');
		$this->load->helper('date');
		$this->load->library('session');		
		$this->meta 			= array();
		$this->scripts 			= array('tiny_mce/tiny_mce','mce_loader','site/general');
		$this->styles 			= array();
		$this->title 			= "";
	}

	function send($user_id){
		$data['person'] = $this->message_model->get_name($user_id)->row_array();
		$this->load->view('message/form_send_message',$data);
	}
	
	function send_message(){
		$session = $this->session->all_userdata();
		$message_data = array(
			'from_user_id' => $session['perniagaan_user']['user_id'],
			'to_user_id' => $this->input->post('user_id'),
			'message_subject' => $this->input->post('message_subject'),
			'message_content' => $this->input->post('message_content'));		
		if($this->message_model->send_act($message_data)){
			$this->session->set_flashdata('message_alert','Your message has been sent.');
		}
		redirect(site_url('message/outbox'));
	}
	
	function inbox(){
		$session = $this->session->all_userdata();
		$data['messages'] = $this->message_model->get(array('to_user_id' => $session['perniagaan_user']['user_id'],'to_delete'=>0),'date DESC','inbox')->result_array();
		$data['list'] = 'inbox';
		$this->load->view('message/list_message',$data);
	}
	
	function outbox(){
		$session = $this->session->all_userdata();
		$data['messages'] = $this->message_model->get(array('from_user_id' => $session['perniagaan_user']['user_id'],'from_delete'=>0),'date DESC','outbox')->result_array();
		$data['list'] = 'outbox';
		$this->load->view('message/list_message',$data);
	}
	
	function delete($id,$list){
		$delete_status = $this->message_model->get(array('message_id'=>$id))->row_array();
		if($list=='inbox'){
			$data = array('to_delete' => 1);
			if($this->message_model->edit($id,$data)){
				$this->delete_all($id);
				$this->session->set_flashdata('message_alert','Your message has been deleted.');
				redirect(site_url('message/inbox'));
			}
		}else{
			$data = array('from_delete'=>1);
			if($this->message_model->edit($id,$data)){
				$this->delete_all($id);
				$this->session->set_flashdata('message_alert','Your message has been deleted.');
				redirect(site_url('message/outbox'));
			}
		}
	}
	
	function delete_all($id){
		$delete_status = $this->message_model->get(array('message_id'=>$id))->row_array();
		if($delete_status['to_delete'] == 1 && $delete_status['from_delete'] == 1){
			$this->message_model->delete($id);
		}else{return false;}
	}
	
	function view($id,$list){
		$data['message'] = $this->message_model->get(array('message_id'=>$id))->row_array();
		if($list == 'inbox' && $data['message']['message_status'] == 0 ){
			$data_edit = array('message_status'=>1);
			$this->message_model->edit($id,$data_edit);
		}
		$this->load->view('message/message_detail',$data);
	}
	
	function new_inbox(){
		$session = $this->session->all_userdata();
		$data = $this->message_model->get(array('to_user_id' => $session['perniagaan_user']['user_id'],'to_delete'=>0,'message_status'=>0),'','inbox')->num_rows();
		echo json_encode($data);
		die;
	}
}

