<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {
	function __construct() {
		parent::__construct("search");

		$this->load->library('session');
		$this->user_sess = $this->session->userdata('perniagaan_user');
		if(!$this->user_sess){
			echo CANT_ACCESS_MSG;
			die;
		}
		
		$this->parts['head'] 	= $this->load->view('partial/head', null, true);
		$this->parts['footer'] 	= $this->load->view('partial/footer', null, true);
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "Search";
	}
	
	function location(){
		$this->load->model('mapping');
		$this->load->library('googlemaps');
		
		$keyword 	= $this->input->post('key');
		$state 		= $this->input->post('state');
		
		$config['center'] = '4.133784,109.116879';
		$config['zoom'] = '6';
		$config['map_height'] = '590px';
		$config['disableMapTypeControl'] = true;
		$config['disableStreetViewControl'] = true;
		$this->googlemaps->initialize($config);
		
		if (isset($_POST['submit'])) {
			$pins = $this->mapping->find_search($keyword, $state);
					
			foreach($pins as $pin){
				if (!empty($pin->geo_location)){
					
					if ($pin->category == 1){
						$category = "Army";
					} else if ($pin->category == 1){
						$category = "Air Force";
					} else {
						$category = "Navy";
					}
					
					$marker = array();
					$marker['position'] = $pin->geo_location;
					$marker['infowindow_content'] = "<b>".$pin->business_name."</b><br>". 
													"<p style='font-size:11px'>".
													"<b>Person : </b>".$pin->name."<br>".
													//"<b>Product : </b>".$pin->main_product."<br>".
													"<b>No. IC : </b>".$pin->ic_no."<br>".
													"<b>Category : </b>".$category."<br>".
													"<b>State : </b>".$pin->state_name."<br>".
													"<b>Telpon : </b>".$pin->telp_no."<br>".
													"<b>Email/Web : </b>".$pin->email_web."<br>".
													"</p>";
													//"<b>Address : </b>".json_encode($pin->address)."<br>";
					//$marker['infowindow_content'] = '<img src='.base_url().'assets/attachment/project/'.$project->attachment_id['attachment_id'].'.jpg width=110px height=90 style=margin:0px;float:left;><div style=font-weight:900;margin-left:10px;float:left>'.$project->name.'<lable style=display:block;font-weight:100>'.$project->location.'</lable><lable style=display:block;font-weight:100>Type: '.$project->type_name.'</lable></div>';
					$marker['animation'] = 'DROP';
					//$marker['icon'] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=A|9999FF|000000';
					$marker['scrollwheel'] = FALSE;
					$this->googlemaps->add_marker($marker);
				} 
			}
		} else {
			$pins = $this->mapping->find_search(NULL,NULL);
		}
		
		$data['datasearch'] = array('keyword' => $keyword,'state' => $state);
		$data['map'] = $this->googlemaps->create_map();
		$data['states'] = $this->mapping->get_state()->result();
		$this->load->view('search/location', $data);
	}

	function advanced() {
		$this->scripts 		= array('jquery.dataTables','administrator/administrator');
		$this->load->model(array('business_model','business_person','activity','trainer'));
		if(isset($_POST['search'])){
			$cat 		= $this->input->post('s_category');
			$st 		= $this->input->post('s_state');
			$act 		= $this->input->post('s_activity');
			$finc 		= $this->input->post('s_financing');
			$key 		= $this->input->post('s_keyword');
			$stat		= $this->input->post('s_work_status');
			$train		= $this->input->post('s_trainer');
			$business 	= $this->business_model->search($cat,$st,$act,$finc,$key,$stat,$train);
			$form		= array('s_cat'=>$cat,'s_st'=>$st,'s_act'=>$act,'s_finc'=>$finc,'s_key'=>$key,'s_stat'=>$stat,'s_train'=>$train);
		}else{
			$business 	= $this->business_model->search();
			$form		= array('s_cat'=>'','s_st'=>'','s_act'=>array(),'s_finc'=>'','s_key'=>'','s_stat'=>'','s_train'=>'');
		}
		$data = array('business' 		=> $business,
					  'categories'		=> unserialize(BUSINESS_PERSON_CATEGORY),
					  'states' 			=> $this->business_person->get_state()->result_array(),
					  'financing_bys' 	=> unserialize(BUSINESS_FINANCING_BY),
					  'work_status'		=> unserialize(WORK_STATUS),
					  'activities'		=> $this->activity->get()->result_array(),
					  'form'			=> $form,
					  'type'			=> 'user',
					  'trainers'		=> $this->trainer->get()->result_array());
		$this->load->view('search/advanced',$data);
	}

}

