<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("site_common.php");

class Site extends Site_common {
	function __construct() {
		parent::__construct("site");

		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "";
	}

	function index() {
	/*	$this->scripts = array('jquery.dataTables');
		$this->load->model(array('business_model','business_person','article','highlight','friend_list_model'));
		$session = $this->session->userdata('perniagaan_user');
		$friend_list = array();
		$business = $this->business_model->get_with_person()->result_array();
		if($session){
			$inviter = $session['user_id'];			
			for($i=0;$i<count($business);$i++){
				$status_friend = $this->friend_list_model->get(array('inviter'=>$inviter,'invitee'=>$business[$i]['user_id']))->row_array();
				if(count($status_friend) > 0){
					$business[$i]['friend_status'] = $status_friend['status'];
				}else{
					$business[$i]['friend_status'] = -1;
				}
			}
		}
		$data = array('business' 		=> $business,
					  'announcements' 	=> $this->article->get(array('publish' => 1, 'category_id' => 2), 'priority ASC')->result_array(),
					  'program_highs' 	=> $this->article->get(array('publish' => 1, 'category_id' => 1), 'priority ASC')->result_array(),
					  'categories'		=> unserialize(BUSINESS_PERSON_CATEGORY),
					  'states'			=> $this->business_person->get_state()->result_array(),
					  'program_highlight'=> $this->highlight->get(array('is_featured' => 1))->result_array());
		$this->load->view('site/index',$data);*/
		$this->layout = FALSE;
		$this->load->view('site/under_maintenance');
	}
	
	function login(){
		$this->load->model('site_model');
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user 	  = $this->site_model->login($username, $password);
		if($user){
			$this->session->set_userdata(array('perniagaan_user' => $user));
			redirect(base_url('site'));
		}else{
			$this->session->set_flashdata('login_msg', 'false');
			redirect(base_url('site'));
		}
	}
	
	function logout(){
		if($this->session->userdata('perniagaan_user')) {
			$this->session->unset_userdata('perniagaan_user');
		}
		redirect(base_url()."site");
	}
	
	function detail($id){
		$this->load->model(array('business_person','business_model','activity'));
		$person = $this->business_person->get(array('business_id' => $id))->row_array();
		$activities = $this->activity->get_business_activity(array('person.business_id' => $id))->result_array();
		
		$activity_name = array();
		foreach($activities as $row){
			$result = $this->activity->get(array('activity_id' => $row['activity_id']))->row_array();
			$activity_name[] = $result['name'];
		}
		$data = array(
				'person' => $person,
				'business' => $this->business_model->get(array('business_id' => $id))->row_array(),
				'state'	=> $this->business_person->get_state(array('state_id' => $person['state_id']))->row_array(),
				'activities' => $activity_name,
				);
		$this->parts['p_title'] = "detail /";
		$this->load->view('site/detail',$data);
	}

	function article($id){
		$this->load->model(array('article','category'));
		$data = array('articles' => $this->article->get(array('article_id' => $id))->row_array());
		$category = $this->category->get(array('category_id' => $data['articles']['category_id']))->row_array();
		$this->parts['p_title'] = $category['name']." /";
		
		$this->load->view('site/article_detail',$data);
	}
	
	function profile($id){
		$this->load->model('highlight');
		$data['highlight'] = $this->highlight->get(array('highlight_id' => $id))->row_array();
		$this->parts['p_title'] = "profile /";
		$this->load->view('site/profile_detail',$data);
	}
}

