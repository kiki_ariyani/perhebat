<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("site_common.php");

class friend_list extends Site_common {
	function __construct() {
		parent::__construct("friend_list");
		$this->load->model('friend_list_model');
		$this->load->helper('date');
		$this->load->library('session');		
		$this->meta 			= array();
		$this->scripts 			= array('tiny_mce/tiny_mce','mce_loader','site/general');
		$this->styles 			= array();
		$this->title 			= "";
	}

	function invite($invitee){ 
		$session = $this->session->all_userdata();
		$inviter = $session['perniagaan_user']['user_id'];
		$data = array('inviter'=>$inviter,'invitee'=>$invitee);
		$result = $this->friend_list_model->add($data);
		$this->session->set_flashdata('message_alert','Your friend request has been sent');
		redirect(site_url('site'));
	}
	
	function list_friend(){
		$session = $this->session->all_userdata();
		$user_id = $session['perniagaan_user']['user_id'];
		$lists = $this->friend_list_model->get(array('inviter'=>$user_id,'status'=>1))->result_array();
		if($lists){
			for($i=0;$i<count($lists);$i++){
				$friend_name = $this->friend_list_model->get_name(array('user.business_id'=>$lists[$i]['invitee']))->row_array();
				$lists[$i]['friend_name'] = $friend_name['name'];
			}
		}
		$data['lists_friend'] = $lists;
		$this->load->view('friend_list/friends',$data);
	}
	
	function friend_request(){
		$session = $this->session->all_userdata();
		$user_id = $session['perniagaan_user']['user_id'];
		$data['user_id'] = $user_id;
		$data['type'] = 'friend_request';
		$lists = $this->friend_list_model->get(array('invitee'=>$user_id,'status'=>0))->result_array();
		if($lists){
			for($i=0;$i<count($lists);$i++){
				$name_inviter = $this->friend_list_model->get_name(array('user.business_id'=>$lists[$i]['inviter']))->row_array();
				$lists[$i]['inviter_name'] = $name_inviter['name'];
				$name_invitee = $this->friend_list_model->get_name(array('user.business_id'=>$lists[$i]['invitee']))->row_array();
				$lists[$i]['invitee_name'] = $name_invitee['name'];
			}
		}
		$data['list_requests'] = $lists;
		$this->load->view('friend_list/list_request',$data);
	}
	
	function pending_request(){
		$session = $this->session->all_userdata();
		$user_id = $session['perniagaan_user']['user_id'];
		$data['user_id'] = $user_id;
		$data['type'] = 'pending_request';
		$lists = $this->friend_list_model->get(array('inviter'=>$user_id,'status'=>0))->result_array();
		if($lists){
			for($i=0;$i<count($lists);$i++){
				$name_inviter = $this->friend_list_model->get_name(array('user.business_id'=>$lists[$i]['inviter']))->row_array();
				$lists[$i]['inviter_name'] = $name_inviter['name'];
				$name_invitee = $this->friend_list_model->get_name(array('user.business_id'=>$lists[$i]['invitee']))->row_array();
				$lists[$i]['invitee_name'] = $name_invitee['name'];
			}
		}
		$data['list_requests'] = $lists;
		$this->load->view('friend_list/list_request',$data);
	}
	
	function confirm($friend_list_id){
		$request = $this->friend_list_model->get(array('friend_list_id'=>$friend_list_id))->result_array();
		$data = array('inviter'=>$request[0]['invitee'],'invitee'=>$request[0]['inviter'],'status'=>1);
		$result = $this->friend_list_model->add($data);
		if($result){
			$this->session->set_flashdata('message_alert','Friend request has been confirmed');
		}
		$this->friend_list_model->edit($friend_list_id,array('status'=>1));
		redirect(site_url('friend_list/friend_request'));
	}
	
	function ignore($friend_list_id){
		$this->friend_list_model->delete(array('friend_list_id'=>$friend_list_id));
		$this->session->set_flashdata('message_alert','Friend request has been deleted');
		redirect(site_url('friend_list/friend_request'));
	}
	
	function new_request(){
		$session = $this->session->all_userdata();
		$user_id = $session['perniagaan_user']['user_id'];
		$data = $this->friend_list_model->get(array('invitee'=>$user_id,'status'=>0))->num_rows();
		echo json_encode($data);
		die;
	}
}

