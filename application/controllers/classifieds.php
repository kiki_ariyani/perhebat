<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('site_common.php');

class Classifieds extends Site_common {
	function __construct() {
		parent::__construct();

		$this->meta 			= array();
		$this->scripts 			= array('site/general','jquery.validate','site/form_validation');
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('classified','business_person'));
	}
	
	function index() {
		$sess = $this->session->userdata('perniagaan_user');
		$data = array(
			'classifieds' 		=> $this->classified->get(array('user_id' => $sess['business_id']))->result_array(),
			'classified_count' 	=> $this->classified->get_notexpire(array('user_id' => $sess['business_id']))->num_rows(),
			'type'				=> unserialize(CLASSIFIED_TYPE),
			'messages'			=> $this->session->flashdata('form_msg'),
		);
		$this->load->view('classified/index', $data);
	}
	
	function add(){
		$sess = $this->session->userdata('perniagaan_user');
		$class_count 	 = $this->classified->get_notexpire(array('user_id' => $sess['business_id']))->num_rows();
		if($class_count < 2){
			$business 	= $this->business_person->get(array('business_id'=>$sess['business_id']))->row_array();
			if($class_count > 0){
				$exist_class = $this->classified->get_notexpire(array('user_id' => $sess['business_id']))->row_array();
			}else{
				$exist_class = NULL;
			}
			$data = array(
				'mode' 				=> 'ADD',
				'company'			=> $business,
				'type'				=> unserialize(CLASSIFIED_TYPE),
				'exist_class_type'  => $exist_class['type'],
				'messages'			=> "",
			);
			$this->load->view("classified/form", $data);
		}else{
			echo 'You have created 2 classifieds.';
		}
	}
	
	function edit($id){
		$sess 			 = $this->session->userdata('perniagaan_user');
		$company 		 = $this->business_person->get(array('business_id'=>$sess['business_id']))->row_array();
		$data = array(
			'mode' 		 => 'EDIT',
			'classified' => $this->classified->get(array('classified_id' => $id))->row_array(),
			'company'	 => $company,
			'type'		 => unserialize(CLASSIFIED_TYPE),
			'messages'	 => $this->session->flashdata('form_msg'),
 		);
		$this->load->view("classified/form", $data);
	}
	
	function save(){
		$this->layout 	= false;
		$start 			= $this->input->post('start_date');
		$end 			= date('Y-m-d', strtotime($start . " +30 days"));
		$sess 			= $this->session->userdata('perniagaan_user');
		$mode			= $this->input->post('mode');
		
		$data_post 		= array('user_id' 			=> $sess['business_id'],
								'title' 			=> $this->input->post('title'),
								'description'		=> $this->input->post('description'),
								'start_date'		=> $start,			
								'end_date'			=> $end,			
								'contact_person'	=> $this->input->post('contact'),			
								'contact_no'		=> $this->input->post('contact_no'),			
								'type'				=> $this->input->post('type')
						  );
		
		if($mode == 'ADD'){
			$classified = $this->classified->get_notexpire(array('user_id' => $sess['user_id']))->num_rows();	
			if($classified < 2){
				$classified_id = $this->classified->add($data_post);
			}
		}else if($mode == 'EDIT'){
			$classified_id = $this->input->post('classified_id');
			$this->classified->edit($classified_id,$data_post);
		}else{
			$classified_id = 0;
		}
		if($classified_id){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}
		if($mode == 'EDIT'){
			redirect(site_url('classifieds/edit/'.$classified_id));
		}else{
			redirect(site_url('classifieds/'));
		}
	}
	
	function view($id){
		$data = array(
			'mode' 		 => 'VIEW',
			'classified' => $this->classified->get(array('classified_id' => $id))->row_array(),
			'type'		 => unserialize(CLASSIFIED_TYPE),
			'sess'		 => $this->session->userdata('perniagaan_user'),
			'messages'	 => '',
 		);
		$this->load->view("classified/form", $data);
	}
	
	function view_all(){
		$data['classifieds'] = $this->classified->get_classified_with_person();
		$this->load->view("classified/all_classified", $data);
	}
	
	function get_classified_detail($id){
		$this->layout = FALSE;
		$data = array('detail' 		 => $this->classified->get(array('classified_id' => $id),'detail')->row_array(),
					  'class_type'   => unserialize(CLASSIFIEDS_TYPE));
		echo json_encode($data);
	}
	
	function delete($id){
		if($this->classified->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
			redirect(site_url('classifieds'));
		}
	}
}

