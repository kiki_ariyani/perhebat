<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once("site_common.php");

class Business extends Site_common {
	function __construct() {
		parent::__construct();
		
		$this->meta 			= array();
		$this->scripts 			= array('jquery.validate','site/form_validation');
		$this->styles 			= array();
		$this->title 			= "";
	}

	function maklumat_diri($business_id) {
		$this->scripts = array('site/general');
		$this->load->model('business_person');
		$detail 					= $this->business_person->get(array('business_id' => $business_id))->row_array();
		if($detail['gender'] == 0 || $detail['religion'] == 0){
			$result 				= $this->business_person->person_gender_religion($detail['name']);
			$detail['gender'] 		= $result['gender'];
			$detail['religion'] 	= $result['religion'];			
		}
		$detail['check_name']	= $this->business_person->check_bin_al($detail['name']);
		$menu = $this->menu->get(array('menu_id' => 30))->row_array();
		$data = array('detail' 		=> $detail,
					  'states' 		=> $this->business_person->get_state()->result_array(),
					  'categories'	=> unserialize(BUSINESS_PERSON_CATEGORY),
					  'sess'   		=> $this->user_sess, 
					  'gender'		=> unserialize(GENDER),
					  'race'		=> unserialize(RACE),
					  'religion'	=> unserialize(RELIGION),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array()
					  );
					  
		$this->parts['p_title'] = ($detail['business_id'] == $data['sess']['business_id'] ? "edit /" : "view /")." maklumat diri /";
		
		$this->load->view('business/form_maklumat_diri',$data);
	}
	
	function maklumat_perniagaan($business_id){
		$this->scripts = array('site/general');
		$this->load->model(array('business_model','activity'));
		$menu = $this->menu->get(array('menu_id' => 30))->row_array();
		$data = array('detail' 			=> $this->business_model->get(array('business_id' => $business_id))->row_array(),
					  'bus_activities' 	=> $this->business_model->get_business_activity(array('business_id' => $business_id))->result_array(),
					  'activities'  	=> $this->activity->get()->result_array(),
					  'financing_bys'	=> unserialize(BUSINESS_FINANCING_BY),
					  'sess'   			=> $this->user_sess,
					  'work_status'		=> unserialize(WORK_STATUS),
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array());
		
		$this->parts['p_title'] = ($data['detail']['business_id'] == $data['sess']['business_id'] ? "edit /" : "view /")." maklumat perniagaan /";
		
		$this->load->view('business/form_maklumat_perniagaan',$data);
	}
	
	function maklumat_kursus($business_id){
		$this->scripts = array('site/general');
		$this->load->model('business_courses');
		$menu = $this->menu->get(array('menu_id' => 30))->row_array();
		$data = array('courses' 	=> $this->business_courses->get($business_id)->result_array(),
					  'business_id'	=> $business_id,
					  'sess'   		=> $this->user_sess,
					  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array());
		$this->parts['p_title'] = ($business_id == $data['sess']['business_id'] ? "edit /" : "view /")." maklumat kursus /";
		$this->load->view('business/form_maklumat_kursus',$data);
	}
	
	function change_password($business_id) {
		$this->scripts = array('site/general');
        $this->load->model(array('business_person','user'));
        $user = $this->session->userdata('perniagaan_user');
		$menu = $this->menu->get(array('menu_id' => 30))->row_array();
        if ($user != NULL) {
            $data = NULL;
            if ($new_password = $this->input->post('new_password')) {
                $old_password = $this->input->post('old_password');
                // $test = $this->user->change_password($old_password, $new_password,'perniagaan_user');
                // print_r($test);
                // die;
                if ($this->user->change_password($old_password, $new_password,'perniagaan_user')) {
                    $this->session->set_flashdata('form', array('success' => 'true', 'message' => 'Your password has been changed to <u>'.$new_password.'</u>'));
                } else {
                    $this->session->set_flashdata('form', array('success' => 'false', 'message' => 'Change password failed. Old password does not match.'));
                }
				redirect(base_url().'business/change_password/'.$business_id);//should be redirect so that flashdata can be viewed.
            }
            $data = array('detail' => $this->business_person->get(array('business_id' => $business_id))->row_array(),
            			  'form_success' => $this->session->flashdata('form'),
            			  'business_id'	=> $business_id,
						  'role_setting'=> $this->role->get_role_setting(array('role_id' => $this->user_sess['role_id'],'menu_id' => $menu['menu_id']))->row_array());
			$this->parts['p_title'] = ($business_id == $user['business_id'] ? "edit /" : "view /")." change password /";
            $this->load->view('business/form_change_password', $data);
        }
    }

	function save_maklumat_diri(){
		$this->load->model('business_person');
		$business_id 		= $this->input->post('business_id');
		$business_info  	= $this->business_person->get(array('business_id'=>$business_id))->row_array();
		if($_FILES['business_logo']['name'] != NULL){				
			if(isset($business_info['business_logo']) && file_exists(realpath(APPPATH . '../assets/img/business_logo') . DIRECTORY_SEPARATOR . $business_info['business_logo'])){
				$this->remove_business_logo($business_info['business_logo']);
			}
			$filename 		= $business_id."_".$_FILES['business_logo']['name'];
			$business_logo 	= $this->upload($filename,'business_logo',PATH_TO_BUSINESS_LOGO);
		}else{
			$business_logo 	= $business_info['business_logo'];
		}
		$person_name		= $this->input->post('name');
		if($this->business_person->check_bin_al($person_name) == 'bin' || $this->business_person->check_bin_al($person_name) == 'al'){
			$result 			= $this->business_person->person_gender_religion($person_name);
			$person_gender		= $result['gender'];
			$person_religion	= ($this->business_person->check_bin_al($person_name) == 'al' ? $this->input->post('religion') : $result['religion']);
		}else{
			$person_gender 		= $this->input->post('gender');
			$person_religion	= $this->input->post('religion');
		}
		$updated_data	= array('name' 			=> $person_name,
								'category' 		=> $this->input->post('category'),
								'ic_no' 		=> $this->input->post('ic_no'),
								'age' 			=> $this->input->post('age'),
								'army_no' 		=> $this->input->post('army_no'),
								'address' 		=> $this->input->post('address'),
								'telp_no' 		=> $this->input->post('telp_no'),
								'state_id' 		=> $this->input->post('state_id'),
								'geo_location' 	=> $this->input->post('geo_location'),
								'business_logo'	=> $business_logo,
								'gender' 		=> $person_gender,
								'religion' 		=> $person_religion,
								'race' 			=> $this->input->post('race'),
								'status_atm'	=> $this->input->post('status_atm'),
								'status_pencen'	=> $this->input->post('status_pencen'),
								'education_level'=>$this->input->post('education_level'));
		$this->business_person->edit($business_id,$updated_data);
		if(isset($_POST['save'])){
			$this->session->set_flashdata('form_msg', 'true');
			redirect(base_url()."business/maklumat_diri/".$business_id);
		}elseif(isset($_POST['next'])){
			redirect(base_url()."business/maklumat_perniagaan/".$business_id);
		}
	}
	
	function save_maklumat_perniagaan(){
		$this->load->model('business_model');
		$business_id 	= $this->input->post('business_id');
		$updated_data	= array('registration_no' 	=> $this->input->post('registration_no'),
								'registration_date' => $this->input->post('registration_date'),
								'business_name' 	=> $this->input->post('business_name'),
								'business_address' 	=> $this->input->post('business_address'),
								'email_web' 		=> $this->input->post('email_web'),
								'main_product' 		=> $this->input->post('main_product'),
								'account_type' 		=> $this->input->post('account_type'),
								'financing_by' 		=> $this->input->post('financing_by'),
								'work_status' 		=> $this->input->post('work_status'));
		$activities		= $this->input->post('business_activity');
		if(count($activities) > 0){
			$this->business_model->delete_business_activity($business_id);
			foreach($activities as $activity){
				unset($activity_data_post);
				$activity_data_post = array('business_id' => $business_id,
									    	'activity_id' => $activity);
				$this->business_model->add_business_activity($activity_data_post);
			}				
		}
		$this->business_model->edit($business_id,$updated_data);
		if(isset($_POST['save'])){
			$this->session->set_flashdata('form_msg', 'true');
			redirect(base_url()."business/maklumat_perniagaan/".$business_id);
		}elseif(isset($_POST['next'])){
			redirect(base_url()."business/maklumat_kursus/".$business_id);
		}
	}
	
	function save_maklumat_kursus(){
		$this->load->model('business_courses');
		$business_id 		= $this->input->post('business_id');
		$course_id			= $this->input->post('course_id');
		$course_name		= $this->input->post('course_name');
		$course_description	= $this->input->post('course_description');

		for($i=0; $i<count($course_name);$i++){
			unset($course_data_post);
			$course_data_post	= array('business_id' 	=> $business_id,
										'name'			=> $course_name[$i],
										'description'	=> $course_description[$i]);
			if(isset($course_id[$i])){
				$this->business_courses->edit($course_id[$i], $course_data_post);
			}else{
				$this->business_courses->add($course_data_post);
			}
		}
		$this->session->set_flashdata('form_msg', 'true');
		redirect(base_url()."business/maklumat_kursus/".$business_id);
	}
	
	public function upload($name,$attachment,$upload_path) {
		$this->load->library('upload');
		$config['file_name'] 		= $name;
		$config['upload_path'] 		= $upload_path;
		$config['allowed_types'] 	= 'png|jpg|gif|bmp|jpeg';
		$config['remove_spaces']	= TRUE;
		$config['overwrite']		= TRUE;
		
		$this->upload->initialize($config);
		if(!$this->upload->do_upload($attachment,true)) {
			echo $this->upload->display_errors();
			return false;
		}else{
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
	}
	
	public function remove_business_logo($name){
		if($name != NULL){
			$url = "./assets/img/business_logo/".$name;
			if (file_exists(realpath(APPPATH . '../assets/img/business_logo') . DIRECTORY_SEPARATOR . $name)) {
				$remove = unlink($url);
			}else{
				return false;
			}
		}
		return true;
	}

}

