<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once('site_common.php');

class Locators extends Site_common {
	function __construct() {
		parent::__construct();

		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "";
	}
	
	function index(){
		$this->load->model('mapping');
		$this->load->library('googlemaps');
		
		$keyword 	= $this->input->post('key');
		$state 		= $this->input->post('state');
		
		$config['center'] = '4.210484,101.975766';
		$config['zoom'] = '7';
		$config['map_height'] = '590px';
		$config['disableMapTypeControl'] = true;
		$config['disableStreetViewControl'] = true;
		$this->googlemaps->initialize($config);
		
		if (isset($_POST['submit'])) {
			$pins = $this->mapping->locators_by_activity($keyword, $state);
		} else {
			$pins = $this->mapping->locators_by_activity("","");
		}
		
		foreach($pins as $pin){
				if (!empty($pin->geo_location)){
					
					if ($pin->category == 1){
						$category = "Army";
					} else if ($pin->category == 1){
						$category = "Air Force";
					} else {
						$category = "Navy";
					}
					
					if($pin->activity_id == 1){
						$icon = "red.png";
					}else if($pin->activity_id == 2){
						$icon = "green.png";
					}else if($pin->activity_id == 3){
						$icon = "blue.png";
					}else if($pin->activity_id == 4){
						$icon = "yellow.png";
					}else if($pin->activity_id == 5){
						$icon = "grey.png";
					}
					
					$marker = array();
					$marker['position'] = $pin->geo_location;
					$marker['infowindow_content'] = "<b>".$pin->business_name."</b><br>". 
													"<p style='font-size:11px'>".
													"<b>Person : </b>".$pin->name."<br>".
													"<b>No. IC : </b>".$pin->ic_no."<br>".
													"<b>Category : </b>".$category."<br>".
													"<b>State : </b>".$pin->state_name."<br>".
													"<b>Telpon : </b>".$pin->telp_no."<br>".
													"<b>Email/Web : </b>".$pin->email_web."<br>".
													"</p>";
					$marker['animation'] = 'DROP';
					$marker['scrollwheel'] = FALSE;
					$marker['icon'] = base_url()."assets/img/map_icon/".$icon;
					$this->googlemaps->add_marker($marker);
				} 
			}
			
			// additional address
			if (isset($_POST['submit'])) {
				$additional = $this->mapping->locators_additional_address(array('state_id' => $state));
			} else {
				$additional = $this->mapping->locators_additional_address();
			}
			
			foreach($additional as $row){
					if($row->type == 1){
						$icon = "red-square.png";
					}else if($row->type == 2){
						$icon = "green-tri.png";
					}else if($row->type == 3){
						$icon = "yellow-tri.png";
					}
					
					$marker = array();
					$marker['position'] = $row->geo_location;
					$marker['infowindow_content'] = "<b>".$row->name."</b><br>". 
													"<p style='font-size:11px'>".
													"<b>Address : </b>".$row->address."<br>".
													"<b>Telp : </b>".$row->telp."<br>".
													"</p>";
				
					$marker['animation'] = 'DROP';
					$marker['scrollwheel'] = FALSE;
					$marker['icon'] = base_url()."assets/img/map_icon/".$icon;
					$this->googlemaps->add_marker($marker);
			}
		
		$data['datasearch'] = array('keyword' => $keyword,'state' => $state);
		$data['map'] = $this->googlemaps->create_map();
		$data['states'] = $this->mapping->get_state()->result();
		$this->parts['p_title'] = "location /";
		$this->load->view('locators/index', $data);
	}
}

