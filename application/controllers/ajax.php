<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	function __construct() {
		parent::__construct();

		$this->layout = false;
	}

	function get_person_gender_and_religion(){
		$this->load->model('business_person');
		$name	= $this->input->post('person_name');
		$person	= $this->business_person->person_gender_religion($name);
		$result	= array('gender' 	=> $person['gender'],
						'religion'	=> $person['religion']);
		echo json_encode($result);
	}
	
	function delete_business_course($course_id){
		$this->load->model('business_courses');
		if($this->business_courses->delete($course_id)){
			$success = true;
		}else{
			$success = false;
		}
		echo json_encode($success);
	}
	
	function get_geo_location(){
		$address 		= $this->input->post('address');
		$address 		= str_replace(" ", "+", $address);
		$json 			= file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
		$json 			= json_decode($json);
		if($json->status != 'ZERO_RESULTS'){			
			$result		= array('status'	=> true,
								'latitude'  => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}, 
								'longitude' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'});
		}else{
			$result		= array('status'	=> false,
								'message'	=> "Geographic Location not found!");
		}
		echo json_encode($result);
	}
	
	function write_geo_location(){
		$this->load->model('business_person');
		$businesses		= $this->business_person->get(array("geo_location" => "", "business_id >"=>1300))->result_array();
		echo "Geo location not found count: <b>".count($businesses)."</b><br/><hr/>";		
		foreach($businesses as $business){
			$address 		= str_replace(" ", "+", $business['address']);
			$json 			= file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
			$json 			= json_decode($json);
			echo "<b>business_id: ".$business['business_id']."</b><br/>";
			if($json->status != 'ZERO_RESULTS'){
				$latitude   = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$longitude  = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				$data_upd	= array('geo_location' => $latitude.",".$longitude);
				if($this->business_person->edit($business['business_id'],$data_upd) == TRUE){
					echo "Updated. ";
				}
				echo "Geo: ".$latitude.",".$longitude;				
			}else{
				echo "Location not found";
			}
			echo "<hr/>";
		}
	}
	
	function search_data_ic(){
		$this->load->model("business_person");
		$ics = array("620316-13-5743","771110-13-5295","771021-13-5015","600607-13-5219","760419-13-5863","611222-13-5529","540409-13-5451","590914-13-5415",
		"611026-13-5337","581202-13-5543","750806-13-6015","550413-13-5111","830402-13-5681","560520-13-5247","530530-13-5067","600211-13-5351","650901-13-6445",
		"830901-13-5387","610322-01-5327","600324-09-5069","580908-13-5065","620504-13-6073","650910-13-6295","590202-13-5679","601110-13-5197","590326-13-5005",
		"770207-03-5915","600306-13-5477","640718-13-5699","620217-13-5617","721123-13-5651","601230-15-5351","7302226-13-5229","510921-13-5269","550802-13-5043",
		"590408-13-5957","600512-13-5435","580725-13-5177","63042-13-5611","491210-13-5093","600304-13-6409","770327-13-5945","600329-13-5701","700308-13-6009",
		"590703-13-5087","610613-08-6563","610220-13-5619","630525-13-5925","750202-13-5505","720103-13-6353","820927-02-5689","480303-13-5275","750518-13-5229",
		"040369-13-6347","770808-13-5815","600103-13-6111","660616-13-6357","610212-08-5009","690316-13-3931","620905-13-5637","620202-13-5809","750716-13-6337",
		"610616-13-5447","520701-13-5063","610820-13-6279","640807-13-6407","590207-13-5571","661105-13-6157","620504-01-5263","621129-02-5295","720715-13-6311",
		"610410-13-6161","571117-13-5355","640608-13-5807");
		$j = 0;
		for($i=0;$i<count($ics);$i++){
			echo $i+1 ." | ";
			$data = $this->business_person->get(array('ic_no' => $ics[$i]))->row_array();
			if($data){
				$j++;
				echo $data['business_id']." - ".$data['name']." - ".$data['ic_no']."<br/>";
				//echo $data['business_id']."<br/>";
			}else{
				echo "data tidak ada<br>";
			}
		}
		echo $j;
	}
	
	function insert_user(){
		$this->load->model(array('business_person','site_model'));
		$this->layout = FALSE;
		$filter_array = array();
		$persons = $this->business_person->get(array('business_id >='=>2160,'business_id <='=>2227))->result_array();
		foreach($persons as $person){
			$filter_name = strtolower(str_replace(' ','.', $person['name']));
			if(in_array($filter_name,$filter_array)){
				$count_name = array_count_values($filter_array);
				$username = $filter_name.".".$count_name[$filter_name];
			}else{
				$username = $filter_name;
			}
			
			$filter_array[] = $filter_name;
			$set_data = array(
				'role_id' => 2,
				'business_id' => $person['business_id'],
				'email' => $username,
				'password' => $this->site_model->get_hash($person['business_id'],$username,"testing"),
				'activation_status' => 1
			);
			$this->site_model->insert_user_upt($set_data);
		}
	}
}

