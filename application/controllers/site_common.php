<?php if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Site_common extends CI_Controller {
	function __construct($module=null) {
		parent::__construct();
		
		$this->load->helper(array('url'));
		$this->load->model(array('article','menu','role'));

		$this->load->library('session');
		$this->user_sess = $this->session->userdata('perniagaan_user');
		$user = $this->user_sess;
		$url = $this->uri->uri_string();
		
		if($url != ""){
			if($module != "site"){
				if(!$this->user_sess){
					echo CANT_ACCESS_MSG;
					die;
				}
			}else{
				$module = $this->uri->segment(1);
				$method  = $this->uri->segment(2);
				$my_menu_url 	= $this->menu->get(array('url' => $url))->row_array();
				$my_menu_module	= $this->menu->get(array('url' => "administrator/".$module))->row_array();
			
				if($my_menu_url){
					$my_menu = $my_menu_url;
					$my_menu_access = $this->role->get_role_setting(array('role_id' => $user['role_id'],'menu_id' => $my_menu['menu_id']))->row_array();
				}else if($my_menu_module){
					$my_menu = $my_menu_module;
					if($module == "business"){
						$my_menu['menu_id'] = 30;
					}
					$my_menu_access = $this->role->get_role_setting(array('role_id' => $user['role_id'],'menu_id' => $my_menu['menu_id']))->row_array();
				}else{
					$my_menu_access = array();
				}
				
				$methods = array('view', 'edit', 'create');
				if($method == NULL || $method == 'index' || $method == 'view'){
					$my_con_mthd = 'view';
				}elseif($method == 'add'){
					$my_con_mthd = 'create';
				}elseif($method == 'delete'){
					$my_con_mthd = 'create';
				}elseif($method == 'edit'){
					$my_con_mthd = 'edit';
				}else{
					if($module == 'business'){
						$my_con_mthd = 'view';
					}
				}
			
				if($my_menu_access){
					if($my_con_mthd != NULL && in_array($my_con_mthd, $methods)){
						if($my_menu_access[$my_con_mthd] == 0){
							echo CANT_ACCESS_MSG;
							die;
						}
					}
				}
			}
		}
		
		if($module == "search"){
			$data = NULL;
		}else{
			if(!$this->user_sess){
				$this->user_sess['role_id'] = 5;
			}
			$menu = $this->menu->get_menu_by_role($this->user_sess['role_id']);
			$data = array('announcements' 	=> $this->article->get(array('publish' => 1, 'category_id' => 2), 'priority ASC')->result_array(),
						  'program_highs' 	=> $this->article->get(array('publish' => 1, 'category_id' => 1), 'priority ASC')->result_array(),
						  'menu'			=> $menu,
						   'messages'		=> $this->session->flashdata('login_msg')
						  );
		}
		$this->parts['head'] 	= $this->load->view('partial/head', $data, true);
		$this->parts['right'] 	= $this->load->view('partial/rightside', $data, true);
		$this->parts['left'] 	= $this->load->view('partial/leftside', $data, true);
		$this->parts['footer'] 	= $this->load->view('partial/footer', null, true);
	}
}