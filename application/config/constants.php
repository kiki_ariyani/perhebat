<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*USER ROLE*/
define('SUPER_ADMIN',1);
define('COMPANY_ADMIN',2);
define('MANAGER',3);
define('GUEST',4);

define('USER_ROLE', serialize(array(
	0 => array('role_id' => 1, 'name' => 'SUPER ADMIN'),
	1 => array('role_id' => 2, 'name' => 'COMPANY ADMIN'),
	2 => array('role_id' => 3, 'name' => 'MANAGER'),
	3 => array('role_id' => 4, 'name' => 'GUEST'),
)));

/*USER ACTIVE*/
define('ACTIVE_ACCOUNT',1);
define('SUSPEND_ACCOUNT',0);
define('USER_STATUS', serialize(array(
	0 => array('id' => 1, 'name' => 'ACTIVE'),
	1 => array('id' => 2, 'name' => 'SUSPENDED'),
)));

/*CAN'T ACCESS MESSAGE*/
define('CANT_ACCESS_MSG', "Sorry, you can't access this content.");
/*ADMIN*/
define('ADMIN_DIR', "administrator/");
define('ADMIN_SESSION', "adm_niaga");

/*BUSINESS PERSON CATEGORY*/
define('BUSINESS_PERSON_CATEGORY', serialize(array(
	0 => array('category_id' => 1, 'name' => 'ARMY'),
	1 => array('category_id' => 2, 'name' => 'AIRFORCE'),
	2 => array('category_id' => 3, 'name' => 'NAVY')
)));

/*BUSINESS FINANCING BY*/
define('BUSINESS_FINANCING_BY', serialize(array(
	0 => array('financing_by_id' => 1, 'name' => 'SENDIRI'),
	1 => array('financing_by_id' => 2, 'name' => 'TEKUN')
)));

/*GENDER*/
define('GENDER', serialize(array(
	0 => array('gender_id' => 1, 'name' => 'Male', 'name_malay' => 'Lelaki'),
	1 => array('gender_id' => 2, 'name' => 'Female', 'name_malay' => 'Perempuan')
)));

/*RACE*/
define('RACE', serialize(array(
	0 => array('race_id' => 1, 'name' => 'Melayu'),
	1 => array('race_id' => 2, 'name' => 'Cina'),
	2 => array('race_id' => 3, 'name' => 'India'),
	3 => array('race_id' => 4, 'name' => 'Pribumi'),
	4 => array('race_id' => 5, 'name' => 'Lain-lain')
)));

/*RELIGION*/
define('RELIGION', serialize(array(
	0 => array('religion_id' => 1, 'name' => 'Islam'),
	1 => array('religion_id' => 2, 'name' => 'Kristian'),
	2 => array('religion_id' => 3, 'name' => 'Hindu'),
	3 => array('religion_id' => 4, 'name' => 'Buddist'),
	4 => array('religion_id' => 5, 'name' => 'Lain-lain')
)));

/*WORK_STATUS*/
define('WORK_STATUS', serialize(array(
	0 => array('status_id' => 1, 'name' => 'Bekerja/Bermajikan'),
	1 => array('status_id' => 2, 'name' => 'Berniaga'),
)));

/*STATUS ATM*/
define('BAKAL_PESARA_ATM',1);
define('PESARA_ATM',2);

/*STATUS PENCEN*/
define('TIDAK_BERPENCEN',1);
define('BERPENCEN',2);

/*PATH*/
define('PATH_TO_BUSINESS_LOGO', 'assets/img/business_logo/');
define('PATH_TO_MENU_ICON', 'assets/img/menu_icon/');
define('PATH_TO_HIGHLIGHT_PROFILE', 'assets/attachment/content/');

/*PAGE TYPE*/
define('ADMIN_PAGE', 0);
define('USER_PAGE', 1);
define('PAGE_TYPE', serialize(array(
	0 => array('type_id' => 0, 'name' => 'Administrator Page'),
	1 => array('type_id' => 1, 'name' => 'User Page'),
)));

/*CLASSIFIEDS TYPE*/
define('CLASSIFIED_TYPE', serialize(array(
	0=>array("value" => 1,"label" => "Looking for"),
	1=>array("value" => 2,"label" => "Offering"),
)));
define('LOOKING_FOR',1);
define('OFFERING',2);

/* End of file constants.php */
/* Location: ./application/config/constants.php */