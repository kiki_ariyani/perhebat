<h1>Messages List</h1>
<?php if($this->session->flashdata('message_alert')){ ?>
<div id="alert-message" class="row">
	<b><?=$this->session->flashdata('message_alert')?></b>
</div>
<?php } ?>
	
	<div id="tab" class="row">
		<ul class="reg-step">
			<li class="title"><a>MESSAGES LIST</a></li>
			<li class="<?= $list == 'inbox' ? 'selected':''?>"><a href="<?=base_url()."message/inbox"?>">INBOX</a></li>
			<li class="<?= $list == 'outbox' ? 'selected':''?>"><a href="<?=base_url()."message/outbox"?>">OUTBOX</a></li>
		</ul>
	</div>
	<div id="page" class="row no_table_label">
		<table class="dataTables biz-table message_list" cellpadding="5" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th><?= $list == 'inbox' ? 'From' : 'To' ?></th>
					<th width="15%">Subject</th>
					<th width="30%">Content</th>
					<th>Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php $no=1; foreach($messages as $message){ ?>
				<tr style="<?= $message['message_status'] == 0 && $list == 'inbox' ? 'background:#F5F5F5;':'background:#FFFFFF;'?>">
					<td style="<?= $message['message_status'] == 0 && $list == 'inbox' ? 'background:#F5F5F5;':'background:#FFFFFF;'?>"><?=$no?></td>
					<td><a href="<?= base_url()."message/view/".$message['message_id']."/".$list?>"><?php if($list == 'inbox' && $message['message_status'] == 0){echo "<b>".$message['person_name']."</b>";}else{echo $message['person_name'];}?></a></td>
					<td><a href="<?= base_url()."message/view/".$message['message_id']."/".$list?>"><?=$message['message_subject']?></a></td>
					<td><?= substr($message['message_content'], 0, 40)?>...</td>
					<td><?=$message['date']?></td>
					<td>
						<a href="<?= base_url()."message/view/".$message['message_id']."/".$list?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
						<a href="<?=base_url()."message/delete/".$message['message_id']."/".$list?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
					</td>
				</tr>
			<?php $no++; } ?>
			</tbody>
		</table>
	</div>