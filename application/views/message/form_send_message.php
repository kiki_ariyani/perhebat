<h1>MESSAGES</h1>

<div id="page" class="row">
<form method="post" action="<?=base_url()."message/send_message"?>" class="biz-form" enctype="multipart/form-data" id="form-send-message">
	<input type="hidden" name="user_id" value="<?=$person['user_id']?>" />	
	<div class="large-4 columns">
		<label>To <span class="red">*</span><span class="block-ita">Kepada</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" disabled="disabled" type="text" name="message_person" value="<?=$person['person_name']?>" id="message_person" />
		</div>
	</div>	
	<div class="large-4 columns">
		<label>Subject<span class="red">*</span><span class="block-ita">Subjek</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" id="message_subject" name="message_subject" value="" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Message <span class="red">*</span><span class="block-ita">Pesan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="6" class="valuebox mce_editor_small" name="message_content"></textarea>
		</div>
	</div>
	<div class="large-4 columns"><br></div>
	<div class="large-8 columns"><br></div>
	
	<div class="large-4 columns">
		<input type="submit" name="send" class="button" value="Send" />		
	</div>
	<div class="large-8 columns"></div>
</form>
</div>