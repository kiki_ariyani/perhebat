<h1><?php echo $message['message_subject'];?></h1>
<div id="page" class="row">
	<div class="message_detail">
		<div style="float:left">From : <b><?= $message['person_name'] ?></b></div>
		<div style="float:right;"><i><?php echo $message['date'];?></i></div>
		<div class="clear"></div>
		<br>
		<div class="inline_box">
			<?php echo $message['message_content'];?>
		</div>
		<div style="float:left; margin-top:20px;"><input class="button" value="Back" type="button" onclick="history.go(-1);" /></div>
		<div class="clear"></div>
	</div>
</div>