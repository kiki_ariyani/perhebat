<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-classified'));
	});
</script>
<?php } ?>

<h1><?=($mode == 'ADD' ? 'Add' : ($mode == 'EDIT' ? 'Edit' : 'View'))?> Classified</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
	<form action="<?=base_url().ADMIN_DIR."classifieds/save"?>" method="post" enctype="multipart/form-data" class="biz-form" id="form-classified">
		<?php if($mode == 'EDIT'){ ?>
			<input type="hidden" value="<?=$classified['classified_id']?>" name="classified_id">
			<input type="hidden" value="<?=$classified['user_id']?>" name="user_id">
		<?php } ?>
		<?php if($mode != 'VIEW'){ ?>
			<input type="hidden" value="<?=$company['name']?>" id="company-name">
			<input type="hidden" value="<?=$mode?>" id="form-mode" name="mode">
		<?php } ?>
		
		<div class="large-4 columns">
			<label>Profile Name <span class="red">*</span><span class="block-ita">Nama Profil</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<?= $person['name']; ?>
			</div>
		</div>
		
		<div class="large-4 columns">
			<label>Type <span class="red">*</span><span class="block-ita">Jenis</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<?php foreach($type as $row){ ?>
						<input <?=($mode == 'EDIT' ? "disabled" : "")?> style="<?=($mode == 'ADD' ? ($exist_class_type == $row['value'] ? "display:none" : "") : "")?>" class="inputbox required" type="radio" name="type" value="<?= $row['value']; ?>" <?= ($mode != 'ADD' && $classified['type'] == $row['value'] ? 'checked' : ''); ?> <?= ($mode == 'ADD' && $row['value'] == 1 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= ($mode == 'ADD' ? ($exist_class_type == $row['value'] ? "" : $row['label']) : $row['label']); ?></input>
				<?php } ?>
				<?=($mode == 'EDIT' ? "<input type='hidden' name='type' value='$classified[type]' />" : "")?>
			</div>
		</div>
			
		<div class="large-4 columns">
			<label>Title <span class="red">*</span><span class="block-ita">Judul</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<input class="inputbox required" type="text" name="title" value="<?= ($mode != 'ADD' ? $classified['title'] : ""); ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>/>
			</div>
		</div>
		
		<div class="large-4 columns">
			<label>Description <span class="red">*</span><span class="block-ita">Penerangan</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<textarea id="desc" class="inputbox required <?/* = ($mode == 'VIEW' ? 'mce_editor_small_readonly' : 'mce_editor_small');  */?>"  <?= ($mode == 'VIEW' ? 'disabled' : ''); ?> cols="50" rows="10" name="description" ><?= ($mode != 'ADD' ? $classified['description'] : ""); ?></textarea>
			</div>
		</div>
		
		<div class="large-4 columns">
			<label>Start Date<span class="red">*</span><span class="block-ita">Tarikh Mula</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<input class="inputbox required <?= ($mode == 'ADD' ? 'datepicker' : ""); ?>" type="text" name="start_date" value="<?= ($mode != 'ADD' ? $classified['start_date'] : ""); ?>" readonly />
			</div>
		</div>
		
		<?php if ($mode != 'ADD') { ?>
			<div class="large-4 columns">
				<label>Expired Date<span class="red">*</span><span class="block-ita">Tarikh Akhir</span></label>
			</div>
			<div class="large-8 columns">
				<div class="panel">
					<input class="inputbox <?= ($mode == 'ADD' ? 'datepicker' : ""); ?>" type="text" name="end_date" <?= ($mode != 'ADD' ? 'readonly' : ''); ?> value="<?= ($mode != 'ADD' ? $classified['end_date'] : ""); ?>"/>
				</div>
			</div>
		<?php } ?>
		<div class="large-4 columns">
			<label>Contact Person<span class="red">*</span><span class="block-ita">Orang Untuk Dihubungi</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<input class="inputbox required" type="text" name="contact" value="<?= ($mode != 'ADD' ? $classified['contact_person'] : ""); ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>/>
			</div>
		</div>
		
		<div class="large-4 columns">
			<label>Contact No<span class="red">*</span><span class="block-ita">Nombor Untuk Dihubungi</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<input class="inputbox required" number="true" maxlength="12" type="text" name="contact_no" value="<?= ($mode != 'ADD' ? $classified['contact_no'] : ""); ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>/>
			</div>
		</div>

		<div class="large-4 columns">
			&nbsp;
		</div>
		<div class="large-8 columns">
		<?php if($mode != 'VIEW'){ ?>
			<div class="field_wrap">
			<input type="submit" value="Save" class="button" name="save" class="submit"/>
			<!--<input type="button" value="Save" class="button" name="save" class="submit" id="submit-classified"/>-->
			<a href="<?= site_url().ADMIN_DIR.'classifieds'; ?>" onclick="return confirm('Are you sure want to cancel input data')" class="button">Cancel</a>
			</div>	
		<?php }else{ ?>
			<div class="field_wrap">
				<?php if($classified['user_id'] == $sess['business_id']){ ?>
					<?php if (date('Y-m-d') < $classified['end_date']) {?>
						<a href="<?= site_url().ADMIN_DIR.'classifieds/edit/'.$classified['classified_id']; ?>" class="button">Edit</a>
					<?php } else {?>
						<a href="" onclick="alert('You cannot edit classified, classified has expired')" class="button" >Edit</a>
					<?php } ?>
				<?php } ?>			
				<a class="button" href="<?= site_url(ADMIN_DIR.'classifieds'); ?>" />Back</a>
			</div>
		<?php } ?>
		</div>
	</form>
</div>

<div id="progress-wrapper"></div>