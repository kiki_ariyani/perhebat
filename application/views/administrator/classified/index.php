<h1>Classifieds</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>
<br/>
<div id="page" class="row no_table_label">
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="3%">No. </th>
			<th>Company Name</th>
			<th>Type</th>
			<th>Title</th>
			<th>Desciption</th>
			<th width="150px">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$no = 1;
		foreach($classifieds as $classified){ ?>
			<tr>
				<td><?=$no?></td>
				<td><?=$classified['name']?></td>
				<td><?=($classified['type'] == LOOKING_FOR ? 'Looking For' : 'Offering')?></td>
				<td><?=$classified['title']?></td>
				<td><?=$classified['description']?></td>
				<td>
					<a href="<?= site_url(ADMIN_DIR.'classifieds/edit/'.$classified['classified_id']); ?>" class="edit" title="Edit"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a>
					<a href="<?= site_url(ADMIN_DIR.'classifieds/view/'.$classified['classified_id']); ?>" class="view" title="View Detail"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
					<a href="<?= site_url(ADMIN_DIR.'classifieds/delete/'.$classified['classified_id']); ?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
				</td>
			</tr>
		<?php $no++;} ?>
	</tbody>
</table>
</div>
