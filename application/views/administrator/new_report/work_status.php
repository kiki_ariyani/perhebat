<h1>Entrepreneur Distribution by Work Status</h1>
<div class="box" id="content-chart" content_type="work_status">
<form class="webform" action="<?= base_url()."administrator/reports/work_status_handler" ?>" id="report-work-status" method="post">
	
	<div class="category_wrap">
	<input type="checkbox" class="state-value" name="state[]" value="0">ALL<br/>
	<?php foreach($states as $state){ ?>
		<input type="checkbox" name="state[]" class="state-value" value="<?= $state['state_id']; ?>"><?= $state['name']; ?><br/>
	<?php } ?>
	</div>
	<div class="category_wrap">
		<a id="work-status" class="button">Generate Report</a>
		<a id="export-work-status" class="button">Export to Excel</a>
	</div>
	<!--<input type="checkbox" class="status-value" name="status[]" value="0">ALL<br/>
	<input type="checkbox" class="status-value" name="status[]" value="1">Registered<br/>
	<input type="checkbox" class="status-value" name="status[]" value="2">Not Registered<br/>-->
</form>
<br>
<br>
<div id="container-work-status" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No.</th>
				<th>Business Name</th>
				<th>Person Name</th>
				<th>Category</th>
				<th>State</th>
			</tr>
		</thead>
		<tbody id="people-dialog">
		</tbody>
	</table>
</div>