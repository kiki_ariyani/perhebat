<h1>Entrepreneur Distribution by Pembiayaan Sendiri vs Tekun</h1>
<div class="box" id="content-chart" content_type="finance">
<form class="webform" action="<?= base_url()."administrator/reports/finance_handler" ?>" id="report-finance" method="post">
	<div class="category_wrap">
		<select name="state[]" style="width:250px;">
			<option value="0">ALL</option>
			<?php foreach($states as $state){ ?>
			<option value="<?= $state['state_id'] ?>"><?= $state['name']; ?></option>
			<?php } ?>
		</select>	
	</div>
	<div class="category_wrap">
		<a id="finance" class="button">Generate Report</a>
		<a id="export-finance" class="button">Export to Excel</a>
	</div>
</form>
<br>
<br>
<div id="container-finance" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No.</th>
				<th>Business Name</th>
				<th>Person Name</th>
				<th>Category</th>
				<th>State</th>
			</tr>
		</thead>
		<tbody id="people-dialog">
		</tbody>
	</table>
</div>