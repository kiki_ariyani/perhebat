<h1>Entrepreneur Distribution by Provider's Amount Spend</h1>
<div class="box" id="content-chart" content_type="provider_amount">
<form class="webform" action="<?= base_url()."administrator/reports/provider_amount_handler" ?>" id="report-provider-amount" method="post">
	<div class="category_wrap">
		<p><b>Select Provider  : </b>
			<select name="provider" id="provider-combobox" style="width:200px;">
				<option value="0">- Select Provider -</option>
				<?php foreach($providers as $provider){ ?>
					<option value="<?=$provider['trainer_id']?>"><?=$provider['name']?></option>
				<?php } ?>
			</select>
		</p>
	</div>	
	<div id="state-wrapper" style="display:none">
		<div class="category_wrap">
		<b>Select State : </b><br/>
		<input type="checkbox" class="state-value" name="state[]" value="0">ALL<br/>
		<?php foreach($states as $state){ ?>
			<input type="checkbox" name="state[]" class="state-value" value="<?= $state['state_id']; ?>"><?= $state['name']; ?><br/>
		<?php } ?>
		</div>
		<div class="category_wrap">
			<a id="provider-amount" class="button">Generate Report</a>
			<a id="export-provider-amount" class="button">Export to Excel</a>
		</div>
	</div>
</form>
<br>
<br>
<div id="container-provider-amount" align="left"></div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No.</th>
				<th>Business Name</th>
				<th>Person Name</th>
				<th>Category</th>
				<th>State</th>
			</tr>
		</thead>
		<tbody id="people-dialog">
		</tbody>
	</table>
</div>