<h1>Entrepreneur Distribution by Negeri Report</h1>
<div class="box" id="content-chart" content_type="state">
<form class="webform" action="<?= base_url()."administrator/reports/state_handler" ?>" id="report-state" method="post">

	<div class="category_wrap">
	<input type="checkbox" class="state-value" name="states[]" checked="checked" value="0">ALL<br/>
	<?php foreach($states as $state){ ?>
		<input type="checkbox" name="states[]" class="state-value" value="<?= $state['state_id']; ?>"><?= $state['name']; ?><br/>
	<?php } ?>
	</div>
	<div class="category_wrap">
		<a id="state" class="button">Generate Report</a>
		<a id="export-state" class="button">Export to Excel</a>
	</div>
</form>
<br>
<br>
<div id="container_state" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No.</th>
				<th>Business Name</th>
				<th>Person Name</th>
				<th>Category</th>
				<th>State</th>
			</tr>
		</thead>
		<tbody id="people-dialog">
		</tbody>
	</table>
</div>