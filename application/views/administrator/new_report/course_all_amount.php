<h1>Entrepreneur Distribution by Courses Amount</h1>
<div class="box" id="content-chart" content_type="course_all_amount">
<form class="webform" action="<?= base_url()."administrator/reports/course_all_amount_handler" ?>" id="report-course-all-amount" method="post">
	
	<div class="category_wrap">
	<input type="checkbox" class="state-value" name="provider[]" value="0">ALL<br/>
	<?php foreach($providers as $provider){ ?>
		<input type="checkbox" name="provider[]" class="state-value" value="<?= $provider['trainer_id']; ?>"><?= $provider['name']; ?><br/>
	<?php } ?>
	</div>
	<div class="category_wrap">
		<a id="course-all-amount" class="button">Generate Report</a>
		<a id="export-course-all-amount" class="button">Export to Excel</a>
	</div>
</form>
<br>
<br>
<div id="container-course-all-amount" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No.</th>
				<th>Business Name</th>
				<th>Person Name</th>
				<th>Category</th>
				<th>State</th>
			</tr>
		</thead>
		<tbody id="people-dialog">
		</tbody>
	</table>
</div>