<h1>Training Providers</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<!--<a href="<?= site_url(ADMIN_DIR.'trainers/add'); ?>" id="add" class="button">Add New Provider</a>-->
<?php if($role_setting['create'] == 1){ ?>
		<a href="<?= site_url(ADMIN_DIR.'trainers/add'); ?>" id="add" class="button">Add New Provider</a>
	<?php } ?>
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="">No.</th>
			<th width="">Name</th>
			<th width="">Code</th>
			<th width="">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($trainers as $trainer){ ?>
		<tr>
			<td><?=$no?></td>
			<td><?=$trainer['name']?></a></td>
			<td><?=$trainer['code']?></a></td>
			<td>
			<?php if($role_setting['edit'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'trainers/edit/'.$trainer['trainer_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a>
			<?php } ?>
			<?php if($role_setting['view'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'trainers/view/'.$trainer['trainer_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
			<?php } ?>
			<?php if($role_setting['create'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'trainers/delete/'.$trainer['trainer_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
			<?php } ?>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
