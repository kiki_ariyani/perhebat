<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-trainer'));
	});
</script>
<?php } ?>
<h1>Training Provider Form</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."trainers/".($mode == 'ADD' ? "add_handler" : "edit_handler/".$id.""); ?>" class="biz-form" enctype="multipart/form-data" id="form-trainer">
	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $trainer['name'] : ""); ?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Code <span class="red">*</span><span class="block-ita">Kode</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="code" value="<?= ($mode != 'ADD' ? $trainer['code'] : ""); ?>"  id="person-name" />
		</div>
	</div>
	
	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'trainers/edit/'.$trainer['trainer_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'trainers'); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>