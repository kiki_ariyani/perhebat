<h1>CHANGE PASSWORD</h1>
<div id="page" class="row">
<?=$form_success['message'];?>
<form method="post" action="<?=base_url()."administrator/users/change_password/".$business_id?>" class="biz-form" enctype="multipart/form-data" id="form-change-password">
	<!-- <input type="hidden" name="business_id" value="<?=$detail['business_id']?>" /> -->
	<div class="large-4 columns">
		<label>Old Password <span class="red">*</span><span class="block-ita">Old Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="old_password" id="old_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>New Password <span class="red">*</span><span class="block-ita">New Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="new_password" id="new_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Confirm New Password <span class="red">*</span><span class="block-ita">Confirm New Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="confirm_new_password" id="confirm_new_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<input type="submit" name="save" class="button" value="Save" />	
	</div>
	<div class="large-8 columns"></div>
</form>
</div>