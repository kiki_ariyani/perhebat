<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-user'));
	});
</script>
<?php } ?>
<h1>User Form</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."users/".($mode == 'ADD' ? "add_handler" : "edit_handler/".$id.""); ?>" class="biz-form" enctype="multipart/form-data" id="form-user">
	<div class="large-4 columns">
		<label>Email <span class="red">*</span><span class="block-ita">Emel</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="email" value="<?= ($mode != 'ADD' ? $user['email'] : ""); ?>" />
		</div>
	</div>
	<?php if($mode != 'VIEW'){ ?>
	<div class="large-4 columns">
		<label>Password <span class="red">*</span><span class="block-ita">Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="password" id="password" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Retype Password <span class="red">*</span><span class="block-ita">Retype Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="retype_password"/>
		</div>
	</div>
	<?php } ?>
	<div class="large-4 columns">
		<label>Role <span class="red">*</span><span class="block-ita">Role</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<select name="role" class="valuebox" style="width:150px;">
				<?php 
				foreach($role as $row){ ?>
					<option value="<?= $row['role_id']; ?>" <?= $mode != 'ADD' && $row['role_id'] == $user['role_id'] ? "selected" : ""; ?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'users/edit/'.$user['user_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'users'); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>