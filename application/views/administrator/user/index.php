<h1>Users</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<!--<a href="<?= site_url(ADMIN_DIR.'users/add'); ?>" id="add" class="button">Add New Users</a>-->
<?php if($role_setting['create'] == 1){ ?>
		<a href="<?= site_url(ADMIN_DIR.'users/add'); ?>" id="add" class="button">Add New User</a>
	<?php } ?>
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="">No.</th>
			<th width="300px">Email</th>
			<th width="300px">Role</th>
			<th width="">Status</th>
			<th width="">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($users as $user){ ?>
		<tr>
			<td><?=$no?></td>
			<td><?=$user['email']?></a></td>
			<td>
				<?php foreach($role as $row){ 
						if($row['role_id'] == $user['role_id']){
							echo $row['name'];
						}
				 } ?>
			</td>
			<td><?=$user_status[$user['activation_status']-1]['name'];?></a></td>
			<td>
			<?php if($role_setting['edit'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'users/edit/'.$user['user_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a> 
			<?php } ?>
			<?php if($role_setting['view'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'users/view/'.$user['user_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
			<?php } ?>
			<?php if($role_setting['create'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'users/delete/'.$user['user_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
			<?php } ?>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
