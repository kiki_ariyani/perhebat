<?php echo $map['js'];?>
<h1>Business Locator</h1>
<div id="page" class="row">
	<div class="box">
		<form method="post" class="form-locator">
			<div class="category_wrap">
				<div class="inp">
					<input type="text" name="key" class="span6 typeahead"></br>
					<select name="state" style="width:250px;height:25px">
						<option value="0">All</option>
						<?php foreach ($states as $state){ ?>
							<option <?php echo ($datasearch['state']==$state->state_id)?"SELECTED":"";?> value="<?php echo $state->state_id; ?>"><?php echo $state->name; ?></option>
						<?php } ?>
					</select>
					<input value="Search" name="submit" type="submit" class="button" style="height: 25px;padding-top: 6px;">
				</div>
			</div>
		</form>
	</div>
	<div class="legend_wrap">
		<img src="<?= base_url()."assets/img/map_icon/red.png"; ?>"><span>Manufacturing / Pembuatan</span>
		<img src="<?= base_url()."assets/img/map_icon/green.png"; ?>"><span>Services / Perkhidmatan</span>
		<img src="<?= base_url()."assets/img/map_icon/blue.png"; ?>"><span>Agriculture / Pertanian</span>
		<img src="<?= base_url()."assets/img/map_icon/yellow.png"; ?>"><span>Trade / Perdagangan</span>
		<img src="<?= base_url()."assets/img/map_icon/grey.png"; ?>"><span>Livestock / Penternakan</span>
		<img src="<?= base_url()."assets/img/map_icon/red-square.png"; ?>"><span>UTC</span>
		<img src="<?= base_url()."assets/img/map_icon/green-tri.png"; ?>"><span>RTC</span>
		<img src="<?= base_url()."assets/img/map_icon/yellow-tri.png"; ?>"><span>Kedai 1 Malaysia</span>
		<div style="clear:both;"></div>
	</div>
	<br/>
	<div class="warper">
		<?php echo $map['html']; ?>
		<div class="clear"></div>
		<?php ?>
	</div>
</div>