<div class="box" id="content-chart" content_type="finance">
<form class="webform" action="<?= base_url()."administrator/old_reports/get_finance_report" ?>" id="report-finance" method="post">
	<h2>Entrepreneur Distribution by Pembiayaan Sendiri vs Tekun</h2>
	
	<select name="status[]">
		<option value="0">ALL</option>
		<?php foreach($states as $state){ ?>
		<option value="<?= $state['state_id'] ?>"><?= $state['name']; ?></option>
		<?php } ?>
	</select>
	<br/>
	<a id="finance" class="btn btn-primary">Generate Report</a>
</form>
<br>
<br>
<div id="chart1div" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
<div class="chart_legend">
	<div class="legend_box" style="background-color:#0099FF;"></div> <span class="legend_text">Pembiayaan Sendiri</span>
	<div class="legend_box" style="background-color:#FCEC0D;"></div> <span class="legend_text">Pembiayaan Tekun</span>
</div>
</div>