<?php if($user = $this->session->userdata(ADMIN_SESSION)){ ?>
<!-- Menu -->
	<div id="menu">
		<div class="nav">
			<img class="header-logo" src="<?=base_url()."assets/img/perhebatcopy.png"?>">
			<!--<h2 style="margin:50px auto 0 100px;text-align:center;float:left">PERHEBAT Entrepreneurs Network (PEN)</h2> -->
			<div class="log-navi">
				<img src="<?=base_url()."assets/img/images.jpg"?>" class="va-top">
				<ul>
					<li><?=$username;?></li>
					<li><a href="<?=base_url()."administrator/users/change_password/".$user['user_id']; ?>">Edit Profile</a></li>
					<li><a href="#">Role : <?= ($role == 1 ? "Super Admin" : ($role == 3 ? "Manager" : "")); ?></a></li>
					<li><a href="<?=base_url().ADMIN_DIR."site/logout";?>">Logout</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- /menu -->
	<!-- Switcher -->
	<div class="button-switch">
		<span class="f-left" id="switcher">
			<a href="#" rel="1col" class="styleswitch ico-col1" title="Hidden Menu Navigation"><img class="button-switch-button" src="<?=base_url()."assets/img/back.png"?>" alt="1 Column"></a>
			<a href="#" rel="2col" class="styleswitch ico-col2" title="Display Menu Navigation"><img class="button-switch-button" src="<?=base_url()."assets/img/arrow.png"?>" alt="2 Columns"></a>
		</span>
	</div>
	<!-- Aside (Left Column) -->
	<div id="aside">
		<div class="large-12 columns boxmg">
			<img src="<?=base_url()."assets/img/logo(1).png"?>">
		</div>
		<span class="desk">NAVIGATION</span>
		
		<div class="test">
		<div id="menu" class="ddsmoothmenu-v">
		<!--<ul class="box">
			<li><a href="<?=base_url().ADMIN_DIR."business";?>"><img src="<?=base_url()."assets/img/company.png"?>" class="va-top">Business List</a></li>
			<?php if($user['role_id'] == SUPER_ADMIN){ ?>
			<li><a href="<?=base_url().ADMIN_DIR."users";?>"><img src="<?=base_url()."assets/img/user.png"?>" class="va-top">Users</a></li>
			<?php } ?>
			<li><a href="<?=base_url().ADMIN_DIR."trainers";?>"><img src="<?=base_url()."assets/img/product.png"?>" class="va-top">Training Provider</a></li>
			<li><a href="<?=base_url().ADMIN_DIR."courses";?>"><img src="<?=base_url()."assets/img/classified.png"?>" class="va-top">Courses</a></li>
			<li><a href=""><img src="<?=base_url()."assets/img/subs.png"?>" class="va-top">Reports</a>
				<ul>
					<li><a href="<?=base_url().ADMIN_DIR."reports/state" ?>">By State</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/status" ?>">By Syarikat berdaftar vs Syarikat tidak berdaftar</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/finance" ?>">By Pembiayaan Sendiri vs Tekun</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/category" ?>">By Kategori</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/activity" ?>">By Aktiviti Perniagaan</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/age" ?>">By Age</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/work_status" ?>">By Work Status</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/gender" ?>">By Gender</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/provider" ?>">By Provider</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/provider_amount" ?>">By Provider's Amount Spend</a></li>
					<li><a href="<?=base_url().ADMIN_DIR."reports/course_all_amount" ?>">By Total Amount Spend</a></li>
				</ul>
			</li>
			<?php if($user['role_id'] == SUPER_ADMIN){ ?> 
			<li><a href="#"><img src="<?=base_url()."assets/img/lamp.png"?>" class="va-top">Search</a>
				<ul>
					<li><a href="<?=base_url()."administrator" ?>">By Data</a></li>
					<li><a href="<?=base_url()."administrator/search/location"?>">By Location</a></li>
					<li><a href="<?=base_url()."administrator/search/advanced"?>">Advanced Search</a></li>
				</ul>
			</li>
			<?php } ?>
			<?php if($user['role_id'] == MANAGER){ ?>
				<li><a href="<?=base_url()."administrator/users/change_password/".$user['user_id']; ?>"><img src="<?=base_url()."assets/img/user.png"?>" class="va-top">Change Password</a></li>
			<?php } ?>
			<?php if($user['role_id'] == SUPER_ADMIN){ ?>
				<li><a href="<?=base_url()."administrator/categories/"; ?>"><img src="<?=base_url()."assets/img/classified.png"?>" class="va-top">Categories</a></li>
				<li><a href="<?=base_url()."administrator/articles/"; ?>"><img src="<?=base_url()."assets/img/classified.png"?>" class="va-top">Articles</a></li>
			<?php } ?>
		</ul>-->
		<ul class="box">
			<?php foreach($menu as $row){ ?>
				<?php $thumb = file_exists(realpath(APPPATH . '../assets/img/menu_icon') . DIRECTORY_SEPARATOR . $row['menu_id'] . ".jpg") ? BASE_URL() . "assets/img/menu_icon/" . $row['menu_id'] . ".jpg?".rand() : ""; ?>
				<li><a href="<?= ($row['child'] ? "#" : base_url().$row['url']); ?>" ><img src="<?= $thumb; ?>" class="va-top"><?= $row['name']; ?></a>
					<?php if($row['child']){ ?>
						<ul>
						<?php foreach($row['child'] as $child){ ?>
								<li><a href="<?= base_url().($child['url'] != "" ? $child['url'] : "#"); ?>"><?= $child['name']; ?></a></li>
						<?php } ?> 
						</ul>
					<?php } ?>
				</li>
			<?php } ?>
		</ul>
		</div>
		</div>
	</div> 
	<!-- /aside -->
<?php } ?>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "menu", //Menu DIV id
	orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
	//customtheme: ["#804000", "#482400"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>