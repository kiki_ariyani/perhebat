<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-menu'));
	});
</script>
<?php } ?>
<h1>Menu Form</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."menus/".($mode == 'ADD' ? "add_handler" : "edit_handler/".$id.""); ?>" class="biz-form" enctype="multipart/form-data" id="form-menu" enctype="multipart/form-data">
	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $menu['name'] : ""); ?>" />
		</div>
	</div>
	
	<div class="large-4 columns">
		<label>Description <span class="block-ita">Deskripsi</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<textarea class="valuebox" name="description"><?= ($mode != 'ADD' ? $menu['description'] : ""); ?></textarea>
		</div>
	</div>
	
	<div class="large-4 columns">
		<label>URL <span class="red">*</span><span class="block-ita">URL</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<?= base_url(); ?><input class="valuebox" type="text" name="url" value="<?= ($mode != 'ADD' ? $menu['url'] : ""); ?>" />
		</div>
	</div>
	
	<div class="large-4 columns">
		<label>Page Type <span class="red">*</span><span class="block-ita">Tipe Halaman</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<select name="type">
				<?php foreach($page_type as $row){ ?>
					<option value="<?= $row['type_id']; ?>" <?= ($mode != 'ADD' ? ($menu['type'] == $row['type_id'] ? "selected" : "") : ""); ?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="large-4 columns">
		<label>Parent <span class="red">*</span><span class="block-ita">Parent</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<select name="parent">
				<option value="0">-No parent-</option>
				<?php foreach($parent_item as $row){ ?>
					<option value="<?= $row['menu_id']; ?>" <?= ($mode != "ADD" ? ($row['menu_id'] == $menu['parent_id'] ? "selected" : "") : ""); ?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<?php if($mode == 'ADD' || $menu['parent_id'] == 0){ ?>
		<div class="large-4 columns">
			<label>Icon <span class="red">*</span><span class="block-ita">Ikon</span></label>
		</div>
		<div class="large-8 columns">	
			<div class="panel">
				<?php if($mode != 'ADD'){ ?>
					<?php $thumb = file_exists(realpath(APPPATH . '../assets/img/menu_icon') . DIRECTORY_SEPARATOR . $menu['menu_id'] . ".jpg") ? BASE_URL() . "assets/img/menu_icon/" . $menu['menu_id'] . ".jpg?".rand() : ""; ?>
					<img src="<?php echo $thumb; ?>" class="menu_icon">
				<?php } ?>
				
				<?php if($mode != 'VIEW'){ ?>
					<input type="file" name="icon" class="valuebox"><br/><br/>
					<span>Recommended size: 16px x 16px<span>
				<?php } ?>
			</div>
		</div>
	<?php } ?>

	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'menus/edit/'.$menu['menu_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'menus'); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>