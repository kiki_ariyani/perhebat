<script type="text/javascript">
$(document).ready(function()
{
	$("tr").click(function(){
		var parent_id = $(this).children().eq(1).attr("menu_id");
		$(this).siblings("tr[parent_id="+parent_id+"]").slideToggle();
	});
});
</script>
<h1>Menu</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<a href="<?= site_url(ADMIN_DIR.'menus/add'); ?>" id="add" class="button">Add New Menu</a>
<table class="dataTables_custom biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="">No.</th>
			<th width="">Name</th>
			<th width="">URL</th>
			<th width="">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($menu as $row){ ?>
		<tr menu_id="<?= $row['menu_id']; ?>">
			<td menu_id="<?= $row['menu_id']; ?>"><?=$no?></td>
			<td menu_id="<?= $row['menu_id']; ?>"><?php if($row['child']){ echo "[+] "; } echo $row['name']; ?></a></td>
			<td menu_id="<?= $row['menu_id']; ?>"><?=$row['url']?></a></td>
			<td menu_id="<?= $row['menu_id']; ?>">
				<a href="<?=site_url(ADMIN_DIR.'menus/edit/'.$row['menu_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a>
				<a href="<?=site_url(ADMIN_DIR.'menus/view/'.$row['menu_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
				<a href="<?=site_url(ADMIN_DIR.'menus/delete/'.$row['menu_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
			</td>
		</tr>
		<?php if($row['child']){ 
				foreach($row['child'] as $child){ ?>
					<tr parent_id="<?= $child['parent_id']; ?>" style="display:none">
						<td></td>
						<td><?="- ".$child['name']?></a></td>
						<td><?=$child['url']?></a></td>
						<td>
							<a href="<?=site_url(ADMIN_DIR.'menus/edit/'.$child['menu_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a>
							<a href="<?=site_url(ADMIN_DIR.'menus/view/'.$child['menu_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
							<a href="<?=site_url(ADMIN_DIR.'menus/delete/'.$child['menu_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
						</td>
					</tr>
		<?php 	}
			  } ?>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
