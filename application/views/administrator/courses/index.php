<h1>Courses</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<!--<a href="<?= site_url(ADMIN_DIR.'courses/add'); ?>" id="add" class="button">Add New Courses</a>-->
<?php if($role_setting['create'] == 1){ ?>
		<a href="<?= site_url(ADMIN_DIR.'courses/add'); ?>" id="add" class="button">Add New Courses</a>
	<?php } ?>
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="">No.</th>
			<th width="300px">Name</th>
			<th width="300px">Description</th>
			<th width="">Trainer Code</th>
			<th width="">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($courses as $course){ ?>
		<tr>
			<td><?=$no?></td>
			<td><?=$course['name']?></a></td>
			<td><?= substr($course['description'],0,100)."..."; ?></a></td>
			<td><?=$course['code']?></a></td>
			<td>
			<?php if($role_setting['edit'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'courses/edit/'.$course['courses_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a> 
			<?php } ?>
			<?php if($role_setting['view'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'courses/view/'.$course['courses_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
			<?php } ?>
			<?php if($role_setting['create'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'courses/delete/'.$course['courses_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
			<?php } ?>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
