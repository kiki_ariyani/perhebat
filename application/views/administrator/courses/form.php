<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-courses'));
	});
</script>
<?php } ?>
<h1>Courses Form</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."courses/".($mode == 'ADD' ? "add_handler" : "edit_handler/".$id.""); ?>" class="biz-form" enctype="multipart/form-data" id="form-courses">	
	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $courses['name'] : ""); ?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Description <span class="red">*</span><span class="block-ita">Huraian Kursus</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<textarea rows="6" cols="6" class="valuebox" name="description"><?= ($mode != 'ADD' ? $courses['description'] : ""); ?></textarea>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Objective <span class="red">*</span><span class="block-ita">Objektif</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<textarea rows="6" cols="6" class="valuebox" name="objective"><?= ($mode != 'ADD' ? $courses['objective'] : ""); ?></textarea>
		</div>
	</div>
	
	<div class="large-4 columns">
		<label>Trainer Code <span class="red">*</span><span class="block-ita">Kod Pengajar</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<select name="trainer" class="valuebox" style="width:150px;">
				<?php foreach($trainers as $trainer){ ?>
					<option value="<?= $trainer['trainer_id']; ?>" <?= $mode != 'ADD' && $trainer['trainer_id'] == $courses['trainer_id'] ? "selected" : ""; ?>><?= $trainer['code']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Courses Cost <span class="red">*</span><span class="block-ita">Biaya Kursus</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			RM <input class="valuebox" type="text" name="cost" value="<?= ($mode != 'ADD' ? $courses['cost'] : ""); ?>" />
		</div>
	</div>
	
	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'courses/edit/'.$courses['courses_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'courses'); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>