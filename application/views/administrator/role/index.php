<h1>Roles</h1>

<?php if($messages != NULL) { ?>
<div id="page" class="row">
	<p style="color:blue"><?php echo $messages;?></p>
</div>
<?php } ?>

<div id="page" class="row">
	<br/>
	<a href="<?= site_url(ADMIN_DIR.'roles/add'); ?>" class="button">Add New Role</a>
	<label>Page Type:</label>
	<select name="type" style="width:200px;display:inline" onchange="window.location.href=this.value">
		<?php foreach($page_type as $row){ ?>
			<option value="<?= base_url()."administrator/roles/index/".$row['type_id']; ?>" <?= ($type == $row['type_id'] ? "selected" : ""); ?>><?= $row['name']; ?></option>
		<?php } ?>
	</select>
	<center>
	<table class="biz-table" width="80%" cellpadding="5">
		<tr>
			<th rowspan="2" style="vertical-align:middle">Menu</th>
			<th colspan="<?=count($roles)?>">User Groups</th>
		</tr>
		<tr>
			<?php foreach($roles as $role){ ?>
				<th><?=$role['name']?> <a style="font-size:10px" href="<?= site_url(ADMIN_DIR.'roles/edit/'.$role['role_id']); ?>">(Edit)</a> - <a style="font-size:10px" onClick="return confirm_delete();" href="<?= site_url(ADMIN_DIR.'roles/delete/'.$role['role_id']); ?>">(Delete)</a></th>
			<?php } ?>
		</tr>
		<?php foreach($menus as $menu){ ?>
		<tr>
			<td><b><?=$menu['name']?></b></td>
			<?php if(count($menu['childs']) > 0){ ?>
				<td colspan="2"></td></tr>
				<?php foreach($menu['childs'] as $child){ ?>
					<tr>
						<td>- <?=$child['name']?></td>
						<?php foreach($child['access'] as $access){ ?>
							<td>
								<?=($access['view'] == 1 ? '- view<br/>' : '')?>
								<?=($access['create'] == 1 ? '- create<br/>' : '')?>
								<?=($access['edit'] == 1 ? '- edit' : '')?>
								<?=(!$access['view'] == 1 && !$access['create'] == 1 && !$access['edit'] == 1 ? 'No Access' : '')?>
							</td>
						<?php } ?>
					</tr>
				<?php } ?>
				<tr>
			<?php }else{ ?>
				<?php foreach($menu['access'] as $access){ ?>
					<td>
						<?=($access['view'] == 1 ? '- view<br/>' : '')?>
						<?=($access['create'] == 1 ? '- create<br/>' : '')?>
						<?=($access['edit'] == 1 ? '- edit' : '')?>
						<?=(!$access['view'] == 1 && !$access['create'] == 1 && !$access['edit'] == 1 ? 'No Access' : '')?>
					</td>
				<?php } ?>
			<?php } ?>
		</tr>
		<?php } ?>
	</table>
	</center>
</div>
