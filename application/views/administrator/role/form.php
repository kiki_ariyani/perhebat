<h1>Role Form</h1>
<div id="page" class="row">
<form method="post" class="biz-form" id="form-role" action="<?=site_url(ADMIN_DIR."roles/save")?>">
	<input type="hidden" name="role_id" value="<?=($mode != 'ADD' ? $role['role_id'] : 0)?>">
	<input type="hidden" name="mode" value="<?=$mode?>">
	<p style="color:blue"><?= $messages; ?></p>
	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $role['name'] : ""); ?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Description <span class="red">*</span><span class="block-ita">Deskripsi</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<textarea name="description"><?= ($mode != 'ADD' ? $role['description'] : ""); ?></textarea>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Page Type <span class="red">*</span><span class="block-ita">Tipe Halaman</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<select name="type" class="page_type">
				<?php foreach($page_type as $row){ ?>
					<option value="<?= $row['type_id']; ?>" <?= ($mode != 'ADD' ? ($role['type'] == $row['type_id'] ? "selected" : "") : ""); ?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Role Access <span class="red">*</span><span class="block-ita">Akses Role</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<table class="admin_page" style="<?= ($mode == 'ADD' ? '' : ($role['type'] == USER_PAGE ? 'display:none' : '')); ?>">
				<tr>
					<th>Menu</th>
					<th>View</th>
					<th>Create</th>
					<th>Edit</th>
				</tr>
				<?php foreach($admin_menus as $menu){?>
					<tr>
						<td><b><?=$menu['name']?></b></td>
						<?php if(count($menu['childs']) > 0){ ?>
							<input type="hidden" name="menu_id[]" value="<?=$menu['menu_id']?>">
							<td><input type="hidden" name="view[]" value="<?=($mode == 'ADD' ? 0 : $menu['view'])?>"><input type="checkbox" class="role-cbox view" <?=($mode != 'ADD' && $menu['view'] == 1 ? 'checked' : '')?> menu_id="<?= $menu['menu_id']; ?>"/></td>
							<td><input type="hidden" name="create[]" value="<?=($mode == 'ADD' ? 0 : $menu['create'])?>"><?php if($menu['menu_id'] != 5 && $menu['menu_id'] != 6){ ?><input type="checkbox" class="role-cbox create" <?=($mode != 'ADD' && $child['create'] == 1 ? 'checked' : '')?> /><?php } ?></td>
							<td><input type="hidden" name="edit[]" value="<?=($mode == 'ADD' ? 0 : $menu['edit'])?>"><?php if($menu['menu_id'] != 5 && $menu['menu_id'] != 6){ ?><input type="checkbox" class="role-cbox edit" <?=($mode != 'ADD' && $child['edit'] == 1 ? 'checked' : '')?>/><?php } ?></td>
							
							<?php foreach($menu['childs'] as $child){ ?>
								<tr>
									<td>- <?=$child['name']?> <input type="hidden" name="menu_id[]" value="<?=$child['menu_id']?>"></td>
									<td><input type="hidden" name="view[]" value="<?=($mode == 'ADD' ? 0 : $child['view'])?>"><input type="checkbox" class="role-cbox view" <?=($mode != 'ADD' && $child['view'] == 1 ? 'checked' : '')?> parent_id="<?= $menu['menu_id']; ?>"/></td>
									<td><input type="hidden" name="create[]" value="<?=($mode == 'ADD' ? 0 : $child['create'])?>"><?php if($child['parent_id'] != 5 && $child['parent_id'] != 6){ ?><input type="checkbox" class="role-cbox create" <?=($mode != 'ADD' && $child['create'] == 1 ? 'checked' : '')?>/><?php } ?></td>
									<td><input type="hidden" name="edit[]" value="<?=($mode == 'ADD' ? 0 : $child['edit'])?>"><?php if($child['parent_id'] != 5 && $child['parent_id'] != 6){ ?><input type="checkbox" class="role-cbox edit" <?=($mode != 'ADD' && $child['edit'] == 1 ? 'checked' : '')?>/><?php } ?></td>
								</tr>
							<?php } ?>
							<tr>
						<?php }else{ ?>
							<input type="hidden" name="menu_id[]" value="<?=$menu['menu_id']?>">
							<td><input type="hidden" name="view[]" value="<?=($mode == 'ADD' ? 0 : $menu['view'])?>"><input type="checkbox" class="role-cbox view" <?=($mode != 'ADD' && $menu['view'] == 1 ? 'checked' : '')?> /></td>
							<td><input type="hidden" name="create[]" value="<?=($mode == 'ADD' ? 0 : $menu['create'])?>"><input type="checkbox" class="role-cbox create" <?=($mode != 'ADD' && $menu['create'] == 1 ? 'checked' : '')?> /></td>
							<td><input type="hidden" name="edit[]" value="<?=($mode == 'ADD' ? 0 : $menu['edit'])?>"><input type="checkbox" class="role-cbox edit" <?=($mode != 'ADD' && $menu['edit'] == 1 ? 'checked' : '')?> /></td>
						<?php } ?>
					</tr>
				<?php } ?>
			</table>
			<!--User Table-->
			<table class="user_page" style="<?= ($mode == 'EDIT' && $role['type'] == USER_PAGE ? '' : 'display:none'); ?>">
				<tr>
					<th>Menu</th>
					<th>View</th>
					<th>Create</th>
					<th>Edit</th>
				</tr>
				<?php foreach($user_menus as $menu){?>
					<tr>
						<td><b><?=$menu['name']?></b></td>
						<?php if(count($menu['childs']) > 0){ ?>
							<input type="hidden" name="menu_id[]" value="<?=$menu['menu_id']?>">
							<td><input type="hidden" name="view[]" value="<?=($mode == 'ADD' ? 0 : $menu['view'])?>"><input type="checkbox" class="role-cbox view" <?=($mode != 'ADD' && $menu['view'] == 1 ? 'checked' : '')?> menu_id="<?= $menu['menu_id']; ?>"/></td>
							<td><input type="hidden" name="create[]" value="<?=($mode == 'ADD' ? 0 : $menu['create'])?>"><?php if($menu['menu_id'] != 5 && $menu['menu_id'] != 6){ ?><input type="checkbox" class="role-cbox create" <?=($mode != 'ADD' && $child['create'] == 1 ? 'checked' : '')?> /><?php } ?></td>
							<td><input type="hidden" name="edit[]" value="<?=($mode == 'ADD' ? 0 : $menu['edit'])?>"><?php if($menu['menu_id'] != 5 && $menu['menu_id'] != 6){ ?><input type="checkbox" class="role-cbox edit" <?=($mode != 'ADD' && $child['edit'] == 1 ? 'checked' : '')?>/><?php } ?></td>
							
							<?php foreach($menu['childs'] as $child){ ?>
								<tr>
									<td>- <?=$child['name']?> <input type="hidden" name="menu_id[]" value="<?=$child['menu_id']?>"></td>
									<td><input type="hidden" name="view[]" value="<?=($mode == 'ADD' ? 0 : $child['view'])?>"><input type="checkbox" class="role-cbox view" <?=($mode != 'ADD' && $child['view'] == 1 ? 'checked' : '')?> parent_id="<?= $menu['menu_id']; ?>"/></td>
									<td><input type="hidden" name="create[]" value="<?=($mode == 'ADD' ? 0 : $child['create'])?>"><?php if($child['parent_id'] != 5 && $child['parent_id'] != 6){ ?><input type="checkbox" class="role-cbox create" <?=($mode != 'ADD' && $child['create'] == 1 ? 'checked' : '')?>/><?php } ?></td>
									<td><input type="hidden" name="edit[]" value="<?=($mode == 'ADD' ? 0 : $child['edit'])?>"><?php if($child['parent_id'] != 5 && $child['parent_id'] != 6){ ?><input type="checkbox" class="role-cbox edit" <?=($mode != 'ADD' && $child['edit'] == 1 ? 'checked' : '')?>/><?php } ?></td>
								</tr>
							<?php } ?>
							<tr>
						<?php }else{ ?>
							<input type="hidden" name="menu_id[]" value="<?=$menu['menu_id']?>">
							<td><input type="hidden" name="view[]" value="<?=($mode == 'ADD' ? 0 : $menu['view'])?>"><input type="checkbox" class="role-cbox view" <?=($mode != 'ADD' && $menu['view'] == 1 ? 'checked' : '')?> /></td>
							<td><input type="hidden" name="create[]" value="<?=($mode == 'ADD' ? 0 : $menu['create'])?>"><input type="checkbox" class="role-cbox create" <?=($mode != 'ADD' && $menu['create'] == 1 ? 'checked' : '')?> /></td>
							<td><input type="hidden" name="edit[]" value="<?=($mode == 'ADD' ? 0 : $menu['edit'])?>"><input type="checkbox" class="role-cbox edit" <?=($mode != 'ADD' && $menu['edit'] == 1 ? 'checked' : '')?> /></td>
						<?php } ?>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>
	
	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'users/edit/'.$user['user_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'roles'); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>