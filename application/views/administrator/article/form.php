<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-article'));
	});
</script>
<?php } ?>
<h1>Article Form</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."articles/".($mode == 'ADD' ? "add_handler" : "edit_handler/".$id.""); ?>" class="biz-form" enctype="multipart/form-data" id="form-article">
	<div class="large-4 columns">
		<label>Title <span class="red">*</span><span class="block-ita">Judul</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="title" value="<?= ($mode != 'ADD' ? $article['title'] : ""); ?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Headline <span class="red">*</span><span class="block-ita">Tajuk</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="6" class="valuebox" name="headline"><?= ($mode != 'ADD' ? $article['headline'] : ""); ?></textarea>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Content <span class="red">*</span><span class="block-ita">Isi</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="6" class="valuebox <?= ($mode != 'VIEW' ? "mce_editor_small" : "mce_editor_small_readonly"); ?>" name="content"><?= ($mode != 'ADD' ? $article['content'] : ""); ?></textarea>
		</div>	
	</div>
	
	
	<div class="large-4 columns">
		<label>Category <span class="red">*</span><span class="block-ita">Kategori</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="category" style="width: 150px;">
			<?php foreach($categories as $category){ ?>
				<option value="<?= $category['category_id'] ?>" <?= ($mode != 'ADD' && $article['category_id'] == $category['category_id'] ? "selected" : ""); ?>><?= $category['name']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Publish <span class="red">*</span><span class="block-ita">Publish</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="publish" style="width: 150px;">
				<option value="1" <?= ($mode != 'ADD' && $article['publish'] == 1 ? "selected" : ""); ?>>Publish</option>
				<option value="0" <?= ($mode != 'ADD' && $article['publish'] == 0 ? "selected" : ""); ?>>Draft</option>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Priority <span class="red">*</span><span class="block-ita">Priority</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="priority" style="width: 150px;">
				<?php for($i=0;$i<10;$i++){ ?>
					<option value="<?= $i; ?>" <?= ($mode != 'ADD' && $article['priority'] == $i ? "selected" : ""); ?>><?= $i; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'articles/edit/'.$article['article_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'articles'); ?>" />Back</a>
	</div>
	<div class="large-8 columns">
	</div>
</form>
</div>