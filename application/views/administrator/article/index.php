<h1>Articles</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<!--<a href="<?= site_url(ADMIN_DIR.'articles/add'); ?>" id="add" class="button">Add New Article</a>-->
<?php if($role_setting['create'] == 1){ ?>
		<a href="<?= site_url(ADMIN_DIR.'articles/add'); ?>" id="add" class="button">Add New Article</a>
	<?php } ?>
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="">No.</th>
			<th width="300px">Title</th>
			<th>Category</th>
			<th width="300px">Headline</th>
			<th width="50px">Publish</th>
			<th width="50px">Priority</th>
			<!--<th width="70px">Featured</th>-->
			<th width="150">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($articles as $article){ ?>
		<tr>
			<td><?=$no?></td>
			<td><?=$article['title']?></a></td>
			<td><?=$article['category_name']?></a></td>
			<td><?=$article['headline']; ?></a></td>
			<td><?php if($article['publish'] != 0){ ?>
					<a href="<?=site_url(ADMIN_DIR.'articles/set_unpublish/'.$article['article_id']);?>" title="Unpublish" class="publish">Publish</a>
				<?php }else{ ?>
					<a href="<?=site_url(ADMIN_DIR.'articles/set_publish/'.$article['article_id']);?>" title="Publish" class="non-publish">Draft</a>
				<?php } ?>
			</td>
			<td><?= $article['priority']; ?></a></td>
			<!--<td><?=($article['publish'] == 1 ? "Publish" : "Draft"); ?></a></td>-->
			<!--<td><?php if($article['publish'] != 0 && $article['featured'] !=0){ ?>
					<a href="<?=site_url(ADMIN_DIR.'articles/set_unfeatured/'.$article['article_id']);?>" class="featured"></a>
				<?php }else if($article['publish'] != 0 && $article['featured'] == 0){ ?>
					<a href="<?=site_url(ADMIN_DIR.'articles/set_featured/'.$article['article_id']);?>" class="non-featured"></a>
				<?php } ?>
			</td>-->
			<td>
			<?php if($role_setting['edit'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'articles/edit/'.$article['article_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a>
			<?php } ?>
			<?php if($role_setting['view'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'articles/view/'.$article['article_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
			<?php } ?>
			<?php if($role_setting['create'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'articles/delete/'.$article['article_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
			<?php } ?>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
