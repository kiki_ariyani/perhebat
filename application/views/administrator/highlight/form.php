<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-highlight'));
	});
</script>
<?php } ?>
<h1>Entrepreneur Profile Form</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."highlights/".($mode == 'ADD' ? "add_handler" : "edit_handler/".$id.""); ?>" class="biz-form" enctype="multipart/form-data" id="form-highlight">
	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $highlight['name'] : ""); ?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Description <span class="red">*</span><span class="block-ita">Huraian</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<textarea style="min-height:200px" class="valuebox" name="description"><?= ($mode != 'ADD' ? $highlight['description'] : ""); ?></textarea>
		</div>
	</div>
	
	<div class="large-4 columns">
		<label>Photo Profile <span class="red">*</span><span class="block-ita">Photo Profile</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<?php if($mode != "ADD"){ 
					$thumb = file_exists(realpath(APPPATH . '../assets/attachment/content') . DIRECTORY_SEPARATOR . $highlight['highlight_id'] . ".jpg") ? BASE_URL() . "assets/attachment/content/" . $highlight['highlight_id'] . ".jpg?".rand() : ""; 
						if($thumb){ ?>
							<img src="<?= $thumb; ?>" style="width:100px"/><br/><br/>
			<?php 		} 
					} 
			if($mode != 'VIEW'){ ?>
				<input class="valuebox" type="file" name="photo"/>
			<?php } ?>
		</div>
	</div>
	
	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'highlights/edit/'.$highlight['highlight_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'highlights'); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>