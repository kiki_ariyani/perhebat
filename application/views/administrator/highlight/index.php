<h1>Entrepreneur Profile Highlights</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<?php $error = $this->session->flashdata('form_msg_err');
		if($error){ ?>
			<div id="alert-message-err" class="row"><?= $error; ?></div>
<?php } ?>

<div id="page" class="row">
<!--<a href="<?= site_url(ADMIN_DIR.'highlights/add'); ?>" id="add" class="button">Add New Category</a>-->
<?php if($role_setting['create'] == 1){ ?>
		<a href="<?= site_url(ADMIN_DIR.'highlights/add'); ?>" id="add" class="button">Add New Profile</a>
	<?php } ?>
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="20px">No.</th>
			<th>Photo</th>
			<th width="300px">Name</th>
			<th width="300px">Description</th>
			<th width="">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($highlights as $highlight){ ?>
		<tr>
			<td class="v_align_top"><?=$no?></td>
			<td><?php $thumb = file_exists(realpath(APPPATH . '../assets/attachment/content') . DIRECTORY_SEPARATOR . $highlight['highlight_id'] . ".jpg") ? BASE_URL() . "assets/attachment/content/" . $highlight['highlight_id'] . ".jpg?".rand() : ""; 
						if($thumb){ ?>
							<img src="<?= $thumb; ?>" style="width:100px"/><br/><br/>
			<?php 		} ?></td>
			<td class="v_align_top"><?=$highlight['name']?></td>
			<td class="v_align_top"><?= substr($highlight['description'],0,540)."..."; ?></td>
			<td class="v_align_top">
			<?php if($highlight['is_featured'] == 1){  ?>
					<a href="<?=site_url(ADMIN_DIR.'highlights/unfeatured/'.$highlight['highlight_id']);?>"><img src="<?=base_url()."assets/img/star.png"?>" title="Set Unfeatured"></a> 
			<?php }else{ ?>
					<a href="<?=site_url(ADMIN_DIR.'highlights/featured/'.$highlight['highlight_id']);?>"><img src="<?=base_url()."assets/img/star_none.png"?>" title="Set Featured"></a> 
			<?php } 
				if($role_setting['edit'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'highlights/edit/'.$highlight['highlight_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a> 
			<?php }
				if($role_setting['view'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'highlights/view/'.$highlight['highlight_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
			<?php } 
				if($role_setting['create'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'highlights/delete/'.$highlight['highlight_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
			<?php } ?>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
