<?php 
$sess = $this->session->userdata(ADMIN_SESSION);
if($sess){ ?>
<script>
	$(document).ready(function(){
		$(".dataTables").dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers"
		});
	});
</script>
<div class="box">
	<h1>Welcome!</h1>
</div>
<?php }else{ ?>
<script type="text/javascript">
	$(document).ready(function () {
		$("#content").after($('#open'));
		$("#content").hide();
		$("#rightside").hide();
		$("#head").hide();
		$("#footer").hide();
	});
</script>
	<div id="open" class="container-fluid">
		<div class="row-fluid-body">
		
			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2 style="color:rgba(233,65,53,1);">Welcome to Administrator Area</h2>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						Please login with your Username and Password.
					</div>
					<?php if($this->session->flashdata('login_msg') == 'false') { 
							echo "<div class='reg-msg-false'>Login Error! Username and password did not match.</div>"; 
					}?>
					<form method="post" action="<?=base_url().ADMIN_DIR."site/validate"?>">
						<fieldset>
							<div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="username" id="username" type="text" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" />
							</div>
							<div class="clearfix"></div>

							<!--<div class="input-prepend">
							<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>
							</div>
							<div class="clearfix"></div>-->

							<p class="center span5">
							<button type="submit" class="btn btn-primary">Login</button>
							</p>
						</fieldset>
					</form>
				</div><!--/span-->
			</div><!--/row-->
		</div><!--/fluid-row-->
		
	</div>
<?php } ?>