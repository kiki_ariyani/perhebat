<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form($('#form-category'));
	});
</script>
<?php } ?>
<h1>Category Form</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."categories/".($mode == 'ADD' ? "add_handler" : "edit_handler/".$id.""); ?>" class="biz-form" enctype="multipart/form-data" id="form-category">
	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $category['name'] : ""); ?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Description <span class="red">*</span><span class="block-ita">Huraian</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<textarea rows="6" cols="6" class="valuebox" name="description"><?= ($mode != 'ADD' ? $category['description'] : ""); ?></textarea>
		</div>
	</div>
	
	<div class="large-4 columns">
		<?php if($mode == 'VIEW'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'categories/edit/'.$category['category_id']); ?>" />Edit</a>
		<?php }else{ ?>
			<input class="button" type="submit" name="save" value="Save" />
		<?php } ?>
		<a class="button" href="<?= site_url(ADMIN_DIR.'categories'); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>