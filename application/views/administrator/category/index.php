<h1>Categories</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>

<div id="page" class="row">
<!--<a href="<?= site_url(ADMIN_DIR.'categories/add'); ?>" id="add" class="button">Add New Category</a>-->
<?php if($role_setting['create'] == 1){ ?>
		<a href="<?= site_url(ADMIN_DIR.'categories/add'); ?>" id="add" class="button">Add New Category</a>
	<?php } ?>
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="">No.</th>
			<th width="300px">Name</th>
			<th width="300px">Description</th>
			<th width="">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($categories as $category){ ?>
		<tr>
			<td><?=$no?></td>
			<td><?=$category['name']?></a></td>
			<td><?=$category['description']; ?></a></td>
			<td>
			<?php if($role_setting['edit'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'categories/edit/'.$category['category_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a> 
			<?php } ?>
			<?php if($role_setting['view'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'categories/view/'.$category['category_id']);?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
			<?php } ?>
			<?php if($role_setting['create'] == 1){ ?>
				<a href="<?=site_url(ADMIN_DIR.'categories/delete/'.$category['category_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
			<?php } ?>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
