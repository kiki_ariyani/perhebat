<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,"",$('#form-akaun'));
	});
</script>
<?php } ?>
<h1>AKAUN</h1>
<div id="page" class="row">
	<ul class="reg-step">
		<li class="title"><a>DATA >></a></li>
		<?php if($mode == 'ADD'){ ?>
			<li class="selected"><a href="#">AKAUN</a></li>
			<li><a href="#">MAKLUMAT DIRI</a></li>
			<li><a href="#">MAKLUMAT PERNIAGAAN</a></li>
			<li><a href="#">MAKLUMAT KURSUS</a></li>
			<li><a href="#">KOMEN</a></li>
		<?php }else{ 
				if($mode == "EDIT"){
					$form = "edit";
				}else{
					$form = "view";
				} ?>
			<li class="selected"><a href="<?=base_url().ADMIN_DIR."business/".$form."/akaun/".$detail['business_id']?>">AKAUN</a></li>
			<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_diri/".$detail['business_id']?>">MAKLUMAT DIRI</a></li>
			<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_perniagaan/".$detail['business_id']?>">MAKLUMAT PERNIAGAAN</a></li>
			<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_kursus/".$detail['business_id']?>">MAKLUMAT KURSUS</a></li>
			<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/comment/".$detail['business_id']?>">KOMEN</a></li>
		<?php } ?>
	</ul>
</div>
<div id="page" class="row">

<?=($this->session->flashdata('form_msg') == 'true' ? "<div class='reg-msg-true'><b>Your data has been saved.</b></div>":"")?>
<form method="post" action="<?=base_url().ADMIN_DIR."business/save/akaun"?>" class="biz-form" enctype="multipart/form-data" id="form-akaun">
	<input type="hidden" name="business_id" value="<?= ($mode != 'ADD' ? $detail['business_id'] : "");?>" />
	<input type="hidden" name="mode" value="<?= $mode;?>" />
	<div class="large-4 columns">
		<label>Username <span class="red">*</span><span class="block-ita">Username</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<?php if($mode == 'ADD'){ ?>
				<input class="valuebox" type="text" name="account_email" value="<?= ($mode != 'ADD' ? $detail['name'] : ""); ?>" />
			<?php }else{ 
				echo $user['email'];
			} ?>
		</div>
	</div>

	<?php if($mode != 'VIEW'){ ?>
	<div class="large-4 columns">
		<label>Password <span class="red">*</span><span class="block-ita">Password</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<?php if($mode == 'ADD'){ ?>
				<input class="valuebox" type="password" name="account_password" value="<?= ($mode != 'ADD' ? $detail['name'] : ""); ?>" />
			<?php }else{ ?>
				<a href="<?= base_url().ADMIN_DIR."business/change_password/".$user['business_id'] ?>">Change Password</a>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
	<br/><br/>
	<div class="large-4 columns">
		<?php if($mode == 'ADD'){ ?>
			<input class="button" style="padding: 4px 18px;" type="submit" name="save" value="Save" />
		<?php }else if($mode == 'EDIT'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/maklumat_diri/'.$detail['business_id']); ?>" />Next >></a>
		<?php }else if($mode == 'VIEW'){?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/akaun/'.$detail['business_id']); ?>" />Edit</a>
		<?php } ?>
	</div>
	<div class="large-8 columns"></div>
	
	
</form>
</div>