<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,"",$('#form-maklumat-diri'));
	});
</script>
<?php } ?>
<h1>MAKLUMAT DIRI</h1>
<div id="page" class="row">
<ul class="reg-step">
	<li class="title"><a>DATA >></a></li>
	<?php if($mode == 'ADD'){ ?>
		<li><a href="#">AKAUN</a></li>
		<li class="selected"><a href="#">MAKLUMAT DIRI</a></li>
		<li><a href="#">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="#">MAKLUMAT KURSUS</a></li>
		<li><a href="#">KOMEN</a></li>
	<?php }else{ 
			if($mode == "EDIT"){
				$form = "edit";
			}else{
				$form = "view";
			} ?>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/akaun/".$detail['business_id']?>">AKAUN</a></li>
		<li class="selected"><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_diri/".$detail['business_id']?>">MAKLUMAT DIRI</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_perniagaan/".$detail['business_id']?>">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_kursus/".$detail['business_id']?>">MAKLUMAT KURSUS</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/comment/".$detail['business_id']?>">KOMEN</a></li>
	<?php } ?>
</ul>
</div>

<?php if($this->session->flashdata('form_msg') == 'true') { ?>
<div id="alert-message" class="row">
	<b>Your data has been saved.</b>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."business/save/maklumat_diri"?>" class="biz-form" enctype="multipart/form-data" id="form-maklumat-diri">
	<input type="hidden" name="business_id" value="<?= ($detail != "" ? $detail['business_id'] : "");?>" />
	<input type="hidden" name="mode" value="<?= $mode;?>" />
	<div class="large-4 columns">
		<label>Business Logo <span class="red">*</span><span class="block-ita">Logo Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<?php if($mode != 'ADD'){ ?>
				<?=($detail['business_logo'] != NULL ?"<img class='company-logo' src='".base_url().PATH_TO_BUSINESS_LOGO ."/$detail[business_logo]' />": "No business logo")?><br/>
			<?php } ?>
			<?php if($mode != 'VIEW'){ ?>
				<input type="file" name="business_logo">
			<?php } ?>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $detail['name'] : ""); ?>"  id="person-name" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Category <span class="red">*</span><span class="block-ita">Kategori Perkhidmatan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="category" style="width:150px;">
				<option value="">- Select category -</option>
				<?php foreach($categories as $category){ ?>
					<option value="<?=$category['category_id']?>" <?=($mode != 'ADD' ? ($detail['category'] == $category['category_id']?'selected':'') : "")?>><?=$category['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>IC No. <span class="red">*</span><span class="block-ita">IC No.</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" id="ic_no" name="ic_no" value="<?=($mode != 'ADD' ? $detail['ic_no'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Age <span class="red">*</span><span class="block-ita">Umur</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" id="age" type="text" name="age" value="<?=($mode != 'ADD' ? $detail['age'] : "")?>" readonly="readonly" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Gender <span class="red">*</span><span class="block-ita">Jantina</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="gender" style="width:150px;" id="person-gender" <?=($mode != 'ADD' && ($detail['check_name'] == 'bin' || $detail['check_name'] == 'al')? 'disabled=disabled':'')?>>
				<option value="">- Select Gender -</option>
				<?php foreach($gender as $row){ ?>
					<option value="<?= $row['gender_id']; ?>" <?=($mode != 'ADD' ? ($row['gender_id'] == $detail['gender']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Religion <span class="red">*</span><span class="block-ita">Agama</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="religion" style="width:150px;" id="person-religion" <?=($mode != 'ADD' && $detail['check_name'] == 'bin'? 'disabled=disabled':'')?>>
				<option value="">- Select Religion -</option>
				<?php foreach($religion as $row){ ?>
					<option value="<?= $row['religion_id']; ?>" <?=($mode != 'ADD' ? ($row['religion_id'] == $detail['religion']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Education Level <span class="red">*</span><span class="block-ita">Tahap Pendidikan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="education_level" value="<?=($mode != 'ADD' ? $detail['education_level'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Service No. <span class="red">*</span><span class="block-ita">No. Tentara</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="army_no" value="<?=($mode != 'ADD' ? $detail['army_no'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Address <span class="red">*</span><span class="block-ita">Alamat</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea id="address-geo" rows="6" cols="23" name="address" ><?=($mode != 'ADD' ? $detail['address'] : "")?></textarea>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Telp No. <span class="red">*</span><span class="block-ita">No. Telefon</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="telp_no" value="<?=($mode != 'ADD' ? $detail['telp_no'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>State <span class="red">*</span><span class="block-ita">Negeri</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="state_id" style="width:150px;">
				<option value="">- Select State -</option>
				<?php foreach($states as $state){ ?>
					<option value="<?=$state['state_id']?>" <?=($mode != 'ADD' ? ($state['state_id'] == $detail['state_id']?'selected="selected"':'') : "")?>><?=$state['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Race <span class="red">*</span><span class="block-ita">Bangsa</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="race" style="width:150px;">
				<?php foreach($race as $row){ ?>
					<option value="<?= $row['race_id']; ?>" <?=($mode != 'ADD' ? ($row['race_id'] == $detail['race']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Status ATM <span class="red">*</span><span class="block-ita">Status ATM</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<div style="width:250px">
				<input style="width:20px" type="radio" name="status_atm" class="status_atm" value="<?= BAKAL_PESARA_ATM; ?>" <?= ($mode != 'ADD' ? ($detail['status_atm'] == BAKAL_PESARA_ATM ? "checked" : "") : ""); ?>>Bakal Pesara ATM
				<input style="width:20px" type="radio" name="status_atm" class="status_atm" value="<?= PESARA_ATM; ?>" <?= ($mode != 'ADD' ? ($detail['status_atm'] == PESARA_ATM ? "checked" : "") : ""); ?>>Pesara ATM
			</div>
		</div>
	</div>

	<div class="field status_pencen_wrap" style="display:<?= $mode !='ADD' ? ($detail['status_atm'] == PESARA_ATM ? "block" : "none") : "none"; ?>">
		<div class="large-4 columns">
			<label>Status Pencen <span class="red">*</span><span class="block-ita">Status Pencen</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<div style="width:250px">
					<input style="width:20px" type="radio" name="status_pencen" class="status_pencen" value="<?= TIDAK_BERPENCEN; ?>" <?= ($mode !='ADD' ? ($detail['status_pencen'] == TIDAK_BERPENCEN ? "checked" : "") : ""); ?>>Tidak Berpencen
					<input style="width:20px" type="radio" name="status_pencen" class="status_pencen" value="<?= BERPENCEN; ?>" <?= ($mode !='ADD' ? ($detail['status_pencen'] == BERPENCEN ? "checked" : "") : ""); ?>>Berpencen
				</div>
			</div>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Geographic Location <span class="red">*</span><span class="block-ita">Letak Geografis</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input id="geo-location" class="valuebox" type="text" name="geo_location" value="<?=($mode != 'ADD' ? $detail['geo_location'] : "")?>" />  <b><a style="text-decoration:underline" target="_blank" href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm">Find Geo Location</a></b>
		</div>
	</div>

	<div class="large-4 columns">
		<input class="button" type="submit" name="save" value="Save" />
		<?php if($mode == 'EDIT'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/maklumat_perniagaan/'.$detail['business_id']); ?>" />Next >></a>
		<?php }else if($mode == 'VIEW'){?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/maklumat_diri/'.$detail['business_id']); ?>" />Edit</a>
		<?php } ?>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>

<div class="detail_courses_wrap" style="display:none">

</div>