<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,"",$('#form-maklumat-kursus'));
	});
</script>
<?php } ?>
<h1>MAKLUMAT KURSUS</h1>
<div id="page" class="row">
<ul class="reg-step">
	<li class="title"><a>DATA >></a></li>
		<?php if($mode == 'ADD'){ ?>
		<li><a href="#">AKAUN</a></li>
		<li><a href="#">MAKLUMAT DIRI</a></li>
		<li><a href="#">MAKLUMAT PERNIAGAAN</a></li>
		<li class="selected"><a href="#">MAKLUMAT KURSUS</a></li>
		<li><a href="#">KOMEN</a></li>
	<?php }else{ 
			if($mode == "EDIT"){
				$form = "edit";
			}else{
				$form = "view";
			} ?>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/akaun/".$detail['business_id']?>">AKAUN</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_diri/".$detail['business_id']?>">MAKLUMAT DIRI</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_perniagaan/".$detail['business_id']?>">MAKLUMAT PERNIAGAAN</a></li>
		<li class="selected"><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_kursus/".$detail['business_id']?>">MAKLUMAT KURSUS</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/comment/".$detail['business_id']?>">KOMEN</a></li>
	<?php } ?>
</ul>
</div>

<?php if($this->session->flashdata('form_msg') == 'true') { ?>
<div id="alert-message" class="row">
	<b>Your data has been saved.</b>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."business/save/maklumat_kursus"?>" class="biz-form" enctype="multipart/form-data" id="form-maklumat-kursus">
	<input type="hidden" name="business_id" value="<?= ($detail != "" ? $detail['business_id'] : "");?>" />
	<input type="hidden" name="mode" value="<?= $mode;?>" />
		<div class="biz-form">
		<?php if($mode == 'VIEW'){ ?>
		<?php foreach($courses as $course){ ?>
			<div class="large-4 columns">
				<label>Courses Name <span class="red">*</span><span class="block-ita">Nama Kursus</span></label>
			</div>
			<div class="large-8 columns">
				<div class="panel">
					<?=$course['name']?>
				</div>
			</div>

			<div class="large-4 columns">
				<label>Trainer <span class="red">*</span><span class="block-ita">Penceramah</span></label>
			</div>
			<div class="large-8 columns">
				<div class="panel">
					<?=$course['trainer_name']?>
				</div>
			</div>

			<div class="large-4 columns">
				<label>Description <span class="red">*</span><span class="block-ita">Huraian Khusus</span></label>
			</div>
			<div class="large-8 columns">
				<div class="panel">
					<?=$course['description']?>
				</div>
			</div>

			<div class="large-4 columns">
				<label>Objective <span class="red">*</span><span class="block-ita">Jangkaan Outcome</span></label>
			</div>
			<div class="large-8 columns">
				<div class="panel">
					<?=$course['objective']?>
				</div>
			</div>
			<?=($course != end($courses) ? "<hr/><br/>":"")?>
		<?php } ?>
		<?php }else if(!$courses && $mode == 'VIEW'){ ?>
			<p>This Business not attend any courses</p>
		<?php }else{ ?>
			<div class="column">
				 <?php foreach($courses as $course){ ?>
					<a class="detail_courses"><input type="checkbox" name="courses[]" class="courses_id" value="<?= $course['courses_id']; ?>" <?php if($mode != 'ADD'){ foreach($business_courses as $row){ echo ($row['courses_id'] == $course['courses_id'] ? "checked" : ""); } } ?>><span title="Detail" style="cursor:pointer"><?= $course['name']; ?></span></a><br/>
		<?php	} ?>
			</div>
		<?php } ?>
		</div>
	</br></br>
	<div class="large-4 columns">&nbsp;</div>
	<div class="large-8 columns">&nbsp;</div>
	<div class="large-4 columns">&nbsp;</div>
	<div class="large-8 columns">&nbsp;</div>
	<div class="large-4 columns">
		<input class="button" type="submit" name="save" value="Save" />
		<?php if($mode == 'EDIT'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/comment/'.$detail['business_id']); ?>" />Next >></a>
		<?php }else if($mode == 'VIEW'){?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/maklumat_kursus/'.$detail['business_id']); ?>" />Edit</a>
		<?php } ?>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>

<div class="detail_courses_wrap" style="display:none">

</div>