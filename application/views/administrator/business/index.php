<h1>Business (Perniagaan)</h1>

<?php if($this->session->flashdata('form_msg') == 'true') { ?>
<div id="alert-message" class="row">
	<b>Your data has been saved.</b>
</div>
<?php } ?>
	
<div id="page" class="row">
	<?php $user = $this->admin_session->get();
	/*if($user['role_id'] == SUPER_ADMIN){?>
	<a href="<?= site_url(ADMIN_DIR.'business/add'); ?>" id="add" class="button">Add New User</a>
	<?php }*/ ?>
	<?php if($role_setting['create'] == 1){ ?>
		<a href="<?= site_url(ADMIN_DIR.'business/add'); ?>" id="add" class="button">Add New User</a>
	<?php } ?>
	<table class="dataTables biz-table" width="100%" cellpadding="5">
		<thead>
			<tr>
				<th width="">No.</th>
				<th width="">Business Name</th>
				<th width="">Person Name</th>
				<th width="">Kategori Perkhidmatan</th>				
				<th width="">Registration No.</th>
				<th width="">Email/Website</th>
				<th width="">State</th>
				<?php //if($user['role_id'] == SUPER_ADMIN){ ?>
				<th width="80px">Action</th>
				<?php //} ?>
			</tr>
		</thead>
		<tbody>
		<?php $no=1; foreach($business as $business){ ?>
			<tr>
				<td><?=$no?></td>
				<td><?=$business['business_name']?></td>
				<td><?=$business['person_name']?></td>
				<td><?php foreach($categories as $category){ echo ($category['category_id'] == $business['category'] ? $category['name'] : ""); } ?></td>
				<td><?=$business['registration_no']?></td>
				<td><?=$business['email_web']?></td>
				<td><?php foreach($states as $state){ echo ($state['state_id'] == $business['state_id'] ? $state['name']: ""); } ?></td>
				<?php //if($user['role_id'] == SUPER_ADMIN){ ?>
				<td>
				<?php if($role_setting['edit'] == 1){ ?>
					<a href="<?=site_url(ADMIN_DIR.'business/edit/akaun/'.$business['business_id']);?>"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a>
				<?php } ?>
				<?php if($role_setting['view'] == 1){ ?>
					<a target="_blank" style="text-decoration:underline" href="<?=base_url().ADMIN_DIR."business/view/akaun/".$business['business_id']?>"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
				<?php } ?>
				<?php if($role_setting['create'] == 1){ ?>
					<a href="<?=site_url(ADMIN_DIR.'business/delete/'.$business['business_id']);?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>
				<?php } ?>
				</td>
				<?php //} ?>
			</tr>
		<?php $no++; } ?>
		</tbody>
	</table>
</div>
