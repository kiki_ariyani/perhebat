<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,"",$('#form-comment'));
	});
</script>
<?php } ?>
<script>
	$(document).ready(function(){
		$("#add-comment").click(function(){
			$(".comment-wrapper").append('<div><a class="delete_comment" style="cursor:pointer">(-)Delete</a><br/><textarea class="valuebox" name="comment[]" rows="4" style="margin-top:10px;margin-bottom:6"></textarea></div>');
		});
		
		$('.delete_comment').live('click',function(){
			$(this).parent().remove();
		});
	});
</script>
<?php if($mode == 'VIEW'){ ?>
	<h1>COMMENT HISTORY</h1>
<?php }else{ ?>
	<h1>COMMENT</h1>
<?php } ?>

<div id="page" class="row">
<ul class="reg-step">
	<li class="title"><a>DATA >></a></li>
		<?php if($mode == 'ADD'){ ?>
		<li><a href="#">AKAUN</a></li>
		<li><a href="#">MAKLUMAT DIRI</a></li>
		<li><a href="#">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="#">MAKLUMAT KURSUS</a></li>
		<li class="selected"><a href="#">KOMEN</a></li>
	<?php }else{ 
			if($mode == "EDIT"){
				$form = "edit";
			}else{
				$form = "view";
			} ?>
		<a href="<?=base_url().ADMIN_DIR."business/".$form."/akaun/".$detail['business_id']?>"><li>AKAUN</a></li>
		<a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_diri/".$detail['business_id']?>"><li>MAKLUMAT DIRI</a></li>
		<a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_perniagaan/".$detail['business_id']?>"><li>MAKLUMAT PERNIAGAAN</a></li>
		<a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_kursus/".$detail['business_id']?>"><li>MAKLUMAT KURSUS</a></li>
		<a href="<?=base_url().ADMIN_DIR."business/".$form."/comment/".$detail['business_id']?>"><li class="selected">KOMEN</a></li>
	<?php } ?>
</ul>
</div>

<?php if($this->session->flashdata('form_msg') == 'true') { ?>
<div id="alert-message" class="row">
	<b>Your data has been saved.</b>
</div>
<?php } ?>

<div id="page" class="row">
<?php if($mode != 'VIEW'){ ?>
<form method="post" action="<?=base_url().ADMIN_DIR."business/save/comment"?>" class="biz-form" enctype="multipart/form-data" id="form-comment">
	<input type="hidden" name="business_id" value="<?= ($detail != "" ? $detail['business_id'] : "");?>" />
	<input type="hidden" name="mode" value="<?= $mode;?>" />
	<?php if($mode != 'VIEW'){ ?>
		<div class="large-4 columns">
			<label>Comment <span class="red">*</span><span class="block-ita">Komen</span></label>
		</div>
		<div class="large-8 columns">	
			<div class="panel">
				<a id="add-comment" style="cursor:pointer">(+)Add More</a>
				<div class="comment-wrapper">
					<?php if($comments == NULL){ ?>
						<div><textarea class="valuebox" name="comment[]" rows="4" style="margin-top:10px; margin-bottom:6px;"></textarea><br/></div>
					<?php }else{ 
							foreach($comments as $comment){ ?>
								<div>
									<textarea class="valuebox" name="comment[]" rows="4" style="margin-top:10px;margin-bottom:6;"><?= $comment['comment']; ?></textarea>
									<a class="delete_comment" style="cursor:pointer;">(-)Delete</a><br/><br/>
								</div>
					<?php 	}
						} ?>
				</div>
			</div>
		</div>
		<br/>
		<div class="large-4 columns">
			<input class="button" type="submit" name="save" value="Save" />
		</div>
	<?php } ?>
</form>
<?php } ?>
	<?php if($mode == 'VIEW'){ ?>
		<a class="button" id="add" href="<?= site_url(ADMIN_DIR.'business/edit/comment/'.$detail['business_id']); ?>" />Add Comment</a>
		<table class="dataTables biz-table" width="100%" cellpadding="5">
			<thead>
				<tr>
					<th width="20px">No</th>
					<th>Comment</th>
					<th width="100px">Posted Date</th>
				</tr>
			</thead>
			<tbody>
				<?php $no=1; foreach($comments as $comment){ ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?= $comment['comment']; ?></td>
						<td><?= $comment['posted_date']; ?></td>
					</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	<?php } ?>
</div>