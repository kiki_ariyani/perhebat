<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,"",$('#form-maklumat-diri'));
	});
</script>
<?php } ?>
<div id="page" class="row">
<?=($this->session->flashdata('form_msg') == 'true' ? "<div class='reg-msg-true'><b>Your data has been saved.</b></div>":"")?>
<form method="post" action="<?=base_url().ADMIN_DIR."business/save"?>" class="biz-form" enctype="multipart/form-data" id="form-maklumat-diri">
	<input type="hidden" name="business_id" value="<?= ($mode != 'ADD' ? $detail['business_id'] : "");?>" />
	<h2>AKAUN</h2>
	<div class="field">
		<div class="label-field">
			<label>Username <span class="mandatory">*</span></label>
			<label class="italic">Username</label>
		</div>
		<div class="value-field">
			<?php if($mode == 'ADD'){ ?>
				<input class="valuebox" type="text" name="account_email" value="<?= ($mode != 'ADD' ? $detail['name'] : ""); ?>" />
			<?php }else{ 
				echo $user['email'];
			} ?>
		</div>
	</div>
	<?php if($mode != 'VIEW'){ ?>
	<div class="field">
		<div class="label-field">
			<label>Password <span class="mandatory">*</span></label>
			<label class="italic">Password</label>
		</div>
		<div class="value-field">
			<?php if($mode == 'ADD'){ ?>
				<input class="valuebox" type="password" name="account_password" value="<?= ($mode != 'ADD' ? $detail['name'] : ""); ?>" />
			<?php }else{ ?>
				<a href="<?= base_url().ADMIN_DIR."business/change_password/".$user['business_id'] ?>">Change Password</a>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
	<h2>MAKLUMAT DIRI</h2>
	<div class="field">
		<div class="label-field">
			<label>Business Logo <span class="mandatory">*</span></label>
			<label class="italic">Logo Perniagaan</label>
		</div>
		<div class="value-field">
			<?php if($mode != 'ADD'){ ?>
				<?=($detail['business_logo'] != NULL ?"<img class='company-logo' src='".base_url().PATH_TO_BUSINESS_LOGO ."/$detail[business_logo]' />": "No business logo")?><br/>
			<?php } ?>
			<?php if($mode != 'VIEW'){ ?>
				<input type="file" name="business_logo">
			<?php } ?>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Name <span class="mandatory">*</span></label>
			<label class="italic">Nama</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" name="name" value="<?= ($mode != 'ADD' ? $detail['name'] : ""); ?>"  id="person-name" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Category <span class="mandatory">*</span></label>
			<label class="italic">Kategori Perkhidmatan</label>
		</div>
		<div class="value-field">
			<select name="category">
				<option value="">- Select category -</option>
				<?php foreach($categories as $category){ ?>
					<option value="<?=$category['category_id']?>" <?=($mode != 'ADD' ? ($detail['category'] == $category['category_id']?'selected':'') : "")?>><?=$category['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>IC No. <span class="mandatory">*</span></label>
			<label class="italic">IC No.</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" id="ic_no" name="ic_no" value="<?=($mode != 'ADD' ? $detail['ic_no'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Age <span class="mandatory">*</span></label>
			<label class="italic">Umur</label>
		</div>
		<div class="value-field">
			<input class="valuebox" id="age" type="text" name="age" value="<?=($mode != 'ADD' ? $detail['age'] : "")?>" readonly="readonly" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Gender <span class="mandatory">*</span></label>
			<label class="italic">Jantina</label>
		</div>
		<div class="value-field">
			<select name="gender" id="person-gender" <?=($mode != 'ADD' && ($detail['check_name'] == 'bin' || $detail['check_name'] == 'al')? 'disabled=disabled':'')?>>
				<option value="">- Select Gender -</option>
				<?php foreach($gender as $row){ ?>
					<option value="<?= $row['gender_id']; ?>" <?=($mode != 'ADD' ? ($row['gender_id'] == $detail['gender']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Religion <span class="mandatory">*</span></label>
			<label class="italic">Agama</label>
		</div>
		<div class="value-field">
			<select name="religion" id="person-religion" <?=($mode != 'ADD' && $detail['check_name'] == 'bin'? 'disabled=disabled':'')?>>
				<option value="">- Select Religion -</option>
				<?php foreach($religion as $row){ ?>
					<option value="<?= $row['religion_id']; ?>" <?=($mode != 'ADD' ? ($row['religion_id'] == $detail['religion']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Education Level <span class="mandatory">*</span></label>
			<label class="italic">Tahap Pendidikan</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" name="education_level" value="<?=($mode != 'ADD' ? $detail['education_level'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Service No. <span class="mandatory">*</span></label>
			<label class="italic">No. Tentera</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" name="army_no" value="<?=($mode != 'ADD' ? $detail['army_no'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Address <span class="mandatory">*</span></label>
			<label class="italic">Alamat</label>
		</div>
		<div class="value-field">
			<textarea id="address-geo" rows="6" cols="23" name="address" ><?=($mode != 'ADD' ? $detail['address'] : "")?></textarea>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Telp No. <span class="mandatory">*</span></label>
			<label class="italic">No. Telefon</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" name="telp_no" value="<?=($mode != 'ADD' ? $detail['telp_no'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>State <span class="mandatory">*</span></label>
			<label class="italic">Negeri</label>
		</div>
		<div class="value-field">
			<select name="state_id">
				<option value="">- Select State -</option>
				<?php foreach($states as $state){ ?>
					<option value="<?=$state['state_id']?>" <?=($mode != 'ADD' ? ($state['state_id'] == $detail['state_id']?'selected="selected"':'') : "")?>><?=$state['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Race <span class="mandatory">*</span></label>
			<label class="italic">Bangsa</label>
		</div>
		<div class="value-field">
			<select name="race">
				<?php foreach($race as $row){ ?>
					<option value="<?= $row['race_id']; ?>" <?=($mode != 'ADD' ? ($row['race_id'] == $detail['race']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Status ATM <span class="mandatory">*</span></label>
			<label class="italic">Status ATM</label>
		</div>
		<div class="value-field">
			<div style="width:250px">
				<input style="width:20px" type="radio" name="status_atm" class="status_atm" value="<?= BAKAL_PESARA_ATM; ?>" <?= ($mode != 'ADD' ? ($detail['status_atm'] == BAKAL_PESARA_ATM ? "checked" : "") : ""); ?>>Bakal Pesara ATM
				<input style="width:20px" type="radio" name="status_atm" class="status_atm" value="<?= PESARA_ATM; ?>" <?= ($mode != 'ADD' ? ($detail['status_atm'] == PESARA_ATM ? "checked" : "") : ""); ?>>Pesara ATM
			</div>
		</div>
	</div>
	<div class="field status_pencen_wrap" style="display:<?= $mode !='ADD' ? ($detail['status_atm'] == PESARA_ATM ? "block" : "none") : "none"; ?>">
		<div class="label-field">
			<label>Status Pencen <span class="mandatory">*</span></label>
			<label class="italic">Status Pencen</label>
		</div>
		<div class="value-field">
			<div style="width:250px">
				<input style="width:20px" type="radio" name="status_pencen" class="status_pencen" value="<?= TIDAK_BERPENCEN; ?>" <?= ($mode !='ADD' ? ($detail['status_pencen'] == TIDAK_BERPENCEN ? "checked" : "") : ""); ?>>Tidak Berpencen
				<input style="width:20px" type="radio" name="status_pencen" class="status_pencen" value="<?= BERPENCEN; ?>" <?= ($mode !='ADD' ? ($detail['status_pencen'] == BERPENCEN ? "checked" : "") : ""); ?>>Berpencen
			</div>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Geographic Location <span class="mandatory">*</span></label>
			<label class="italic">Letak Geografis</label>
		</div>
		<div class="value-field">
			<input id="geo-location" class="valuebox" type="text" name="geo_location" value="<?=($mode != 'ADD' ? $detail['geo_location'] : "")?>" />  <b><a style="text-decoration:underline" target="_blank" href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm">Find Geo Location</a></b>
		</div>
	</div>
	
	<h2>MAKLUMAT PERNIAGAAN</h2>
	<div class="field">
		<div class="label-field">
			<label>Work Status <span class="mandatory">*</span></label>
			<label class="italic">Status Pekerjaan</label>
		</div>
		<div class="value-field">
			<select name="work_status">
				<?php foreach($work_status as $row){ ?>
					<option value="<?= $row['status_id']; ?>" <?=($mode != 'ADD' ? ($row['status_id'] == $detail_business['work_status']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Registration No. <span class="mandatory">*</span></label>
			<label class="italic">No. Pendaftaran</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" name="registration_no" value="<?=($mode != 'ADD' ? $detail_business['registration_no'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Registration Date <span class="mandatory">*</span></label>
			<label class="italic">Tarikh Daftar</label>
		</div>
		<div class="value-field">
			<input readonly="readonly" class="valuebox datepicker" type="text" name="registration_date" value="<?=($mode != 'ADD' ? $detail_business['registration_date'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Business Name <span class="mandatory">*</span></label>
			<label class="italic">Nama Perniagaan</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" name="business_name" value="<?=($mode != 'ADD' ? $detail_business['business_name'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Business Address <span class="mandatory">*</span></label>
			<label class="italic">Alamat Perniagaan</label>
		</div>
		<div class="value-field">
			<textarea rows="6" cols="23" name="business_address"><?=($mode != 'ADD' ? $detail_business['business_address'] : "")?></textarea>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Email/Web <span class="mandatory">*</span></label>
			<label class="italic">Email/Web</label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="text" name="email_web" value="<?=($mode != 'ADD' ? $detail_business['email_web'] : "")?>" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Business Activity <span class="mandatory">*</span></label>
			<label class="italic">Aktiviti Perniagaan</label>
		</div>
		<div class="value-field">
			<?php foreach($activities as $activity){ ?>
				<input type="checkbox" name="business_activity[]" value="<?=$activity['activity_id']?>" <?php if($mode != 'ADD'){foreach($bus_activities as $bus_activity){ echo ($bus_activity['activity_id'] == $activity['activity_id'] ? "checked" : ""); } }?>><?=$activity['name']?></input><br/>
			<?php } ?>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Main Product <span class="mandatory">*</span></label>
			<label class="italic">Product Utama</label>
		</div>
		<div class="value-field">
			<textarea rows="6" cols="23" name="main_product"><?=($mode !='ADD' ? $detail_business['main_product'] : "")?></textarea>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Account Type <span class="mandatory">*</span></label>
			<label class="italic">Jenis Akaun</label>
		</div>
		<div class="value-field">
			<textarea rows="6" cols="23" name="account_type"><?=($mode != 'ADD' ? $detail_business['account_type'] : "")?></textarea>
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Financing By <span class="mandatory">*</span></label>
			<label class="italic">Pembiayaan Oleh</label>
		</div>
		<div class="value-field">
			<select name="financing_by">
				<option value="">- Select Financing -</option>
				<?php foreach($financing_bys as $financing_by){ ?>
					<option value="<?=$financing_by['financing_by_id']?>" <?=($mode != 'ADD' ? ($detail_business['financing_by'] == $financing_by['financing_by_id']?'selected':'') : "")?>><?=$financing_by['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<h2>MAKLUMAT KURSUS</h2><br/>
		<div class="biz-form">
		<?php if($mode == 'VIEW'){ ?>
		<?php foreach($courses as $course){ ?>
			<div class="field">
				<div class="label-field">
					<label>Courses Name</label>
					<label class="italic">Nama Kursus</label>
				</div>
				<div class="value-field"><?=$course['name']?></div>
			</div>
			<div class="field">
				<div class="label-field">
					<label>Trainer</label>
					<label class="italic">Penceramah</label>
				</div>
				<div class="value-field"><?=$course['trainer_name']?></div>
			</div>
			<div class="field">
				<div class="label-field">
					<label>Description</label>
					<label class="italic">Huraian Khusus</label>
				</div>
				<div class="value-field"><?=$course['description']?></div>
			</div>
			<div class="field">
				<div class="label-field">
					<label>Objective</label>
					<label class="italic">Jangkaan Outcome</label>
				</div>
				<div class="value-field"><?=$course['objective']?></div>
			</div>
			<?=($course != end($courses) ? "<hr/><br/>":"")?>
		<?php } ?>
		<?php }else if(!$courses && $mode == 'VIEW'){ ?>
			<p>This Business not attend any courses</p>
		<?php }else{ ?>
			<div class="column">
				 <?php foreach($courses as $course){ ?>
					<a class="detail_courses"><input type="checkbox" name="courses[]" class="courses_id" value="<?= $course['courses_id']; ?>" <?php if($mode != 'ADD'){ foreach($business_courses as $row){ echo ($row['courses_id'] == $course['courses_id'] ? "checked" : ""); } } ?>><span title="Detail" style="cursor:pointer"><?= $course['name']; ?></span></a><br/>
		<?php	} ?>
			</div>
		<?php } ?>
		</div>
		
		<h2>Comments History</h2>
		<?php foreach($comments as $comment){ ?>
			<div class="field" style="padding-left:0">
				<div class="value-field"><?=$comment['comment']?><br/><br/><i>Posted Date: <?= $comment['posted_date'] ?></i></div>
			</div>
		<?php } ?>
	<br/><br/>
	<div class="field">
		<input class="btn btn-primary" style="padding: 4px 18px;" type="submit" name="save" value="Save" />
		<a class="btn btn-primary" href="<?= site_url(ADMIN_DIR.'business'); ?>" />Back</a>
		<?php $user = $this->admin_session->get();
			if($user['role_id'] == SUPER_ADMIN && $mode == 'VIEW'){ ?>
			<a class="btn btn-primary" href="<?= site_url(ADMIN_DIR.'business/edit/akaun/'.$detail['business_id']); ?>" />Edit</a>
		<?php } ?>
	</div>
	
	
</form>
</div>

<div class="detail_courses_wrap" style="display:none">

</div>