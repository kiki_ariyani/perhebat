<h1>CHANGE PASSWORD</h1>
<div id="page" class="row">
<ul class="reg-step">
	<li class="title"><a>Data >> </a></li>
	<li class="selected"><a href="<?=base_url().ADMIN_DIR."business/edit/akaun/".$business_id; ?>">AKAUN</a></li>
	<li><a href="<?=base_url().ADMIN_DIR."business/edit/maklumat_diri/".$business_id; ?>">MAKLUMAT DIRI</a></li>
	<li><a href="<?=base_url().ADMIN_DIR."business/edit/maklumat_perniagaan/".$business_id; ?>">MAKLUMAT PERNIAGAAN</a></li>
	<li><a href="<?=base_url().ADMIN_DIR."business/edit/maklumat_kursus/".$business_id; ?>">MAKLUMAT KURSUS</a></li>
	<li><a href="<?=base_url().ADMIN_DIR."business/edit/comment/".$business_id; ?>">KOMEN</a></li>
</ul>
</div>

<?php if($form_success['message'] != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $form_success['message'];?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url()."administrator/business/change_password/".$business_id?>" class="biz-form" enctype="multipart/form-data" id="form-change-password">
	<!-- <input type="hidden" name="business_id" value="<?=$detail['business_id']?>" /> -->
	<div class="large-4 columns">
		<label>New Password <span class="red">*</span><span class="block-ita">New Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="new_password" id="new_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Confirm New Password <span class="red">*</span><span class="block-ita">Confirm New Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="confirm_new_password" id="confirm_new_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<input type="submit" name="save" class="button" value="Save" />
		<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/akaun/'.$business_id); ?>" />Back</a>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>