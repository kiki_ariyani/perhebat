<?php if($mode == 'VIEW'){ ?>
<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,"",$('#form-maklumat-perniagaan'));
	});
</script>
<?php } ?>
<h1>MAKLUMAT PERNIAGAAN</h1>
<div id="page" class="row">
<ul class="reg-step">
	<li class="title"><a>DATA >></a></li>
	<?php if($mode == 'ADD'){ ?>
		<li><a href="#">AKAUN</a></li>
		<li><a href="#">MAKLUMAT DIRI</a></li>
		<li class="selected"><a href="#">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="#">MAKLUMAT KURSUS</a></li>
		<li><a href="#">KOMEN</a></li>
	<?php }else{ 
			if($mode == "EDIT"){
				$form = "edit";
			}else{
				$form = "view";
			} ?>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/akaun/".$detail['business_id']?>">AKAUN</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_diri/".$detail['business_id']?>">MAKLUMAT DIRI</a></li>
		<li class="selected"><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_perniagaan/".$detail['business_id']?>">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/maklumat_kursus/".$detail['business_id']?>">MAKLUMAT KURSUS</a></li>
		<li><a href="<?=base_url().ADMIN_DIR."business/".$form."/comment/".$detail['business_id']?>">KOMEN</a></li>
	<?php } ?>
</ul>
</div>

<?php if($this->session->flashdata('form_msg') == 'true') { ?>
<div id="alert-message" class="row">
	<b>Your data has been saved.</b>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url().ADMIN_DIR."business/save/maklumat_perniagaan"?>" class="biz-form" enctype="multipart/form-data" id="form-maklumat-perniagaan">
	<input type="hidden" name="business_id" value="<?= ($detail != "" ? $detail['business_id'] : "");?>" />
	<input type="hidden" name="mode" value="<?= $mode;?>" />
	
	<div class="large-4 columns">
		<label>Work Status <span class="red">*</span><span class="block-ita">Status Pekerjaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="work_status" style="width:150px;">
				<?php foreach($work_status as $row){ ?>
					<option value="<?= $row['status_id']; ?>" <?=($mode != 'ADD' ? ($row['status_id'] == $detail_business['work_status']?'selected="selected"':'') : "")?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Registration No. <span class="red">*</span><span class="block-ita">No. Pendaftaran</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="registration_no" value="<?=($mode != 'ADD' ? $detail_business['registration_no'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Registration Date <span class="red">*</span><span class="block-ita">Tarikh Daftar</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input readonly="readonly" class="valuebox datepicker" type="text" name="registration_date" value="<?=($mode != 'ADD' ? $detail_business['registration_date'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Business Name <span class="red">*</span><span class="block-ita">Nama Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="business_name" value="<?=($mode != 'ADD' ? $detail_business['business_name'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Business Address <span class="red">*</span><span class="block-ita">Alamat Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="23" name="business_address"><?=($mode != 'ADD' ? $detail_business['business_address'] : "")?></textarea>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Email/Web <span class="red">*</span><span class="block-ita">Email/Web</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="email_web" value="<?=($mode != 'ADD' ? $detail_business['email_web'] : "")?>" />
		</div>
	</div>

	<div class="large-4 columns">
		<label>Business Activity <span class="red">*</span><span class="block-ita">Aktiviti Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<?php foreach($activities as $activity){ ?>
				<input type="checkbox" name="business_activity[]" value="<?=$activity['activity_id']?>" <?php if($mode != 'ADD'){foreach($bus_activities as $bus_activity){ echo ($bus_activity['activity_id'] == $activity['activity_id'] ? "checked" : ""); } }?>><?=$activity['name']?></input><br/>
			<?php } ?>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Main Product <span class="red">*</span><span class="block-ita">Product Utama</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="23" name="main_product"><?=($mode !='ADD' ? $detail_business['main_product'] : "")?></textarea>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Account Type <span class="red">*</span><span class="block-ita">Jenis Akaun</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="23" name="account_type"><?=($mode != 'ADD' ? $detail_business['account_type'] : "")?></textarea>
		</div>
	</div>

	<div class="large-4 columns">
		<label>Financing By <span class="red">*</span><span class="block-ita">Pembiayaan Oleh</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="financing_by" style="width:150px;">
				<option value="">- Select Financing -</option>
				<?php foreach($financing_bys as $financing_by){ ?>
					<option value="<?=$financing_by['financing_by_id']?>" <?=($mode != 'ADD' ? ($detail_business['financing_by'] == $financing_by['financing_by_id']?'selected':'') : "")?>><?=$financing_by['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="large-4 columns">
		<input class="button" type="submit" name="save" value="Save" />
		<?php if($mode == 'EDIT'){ ?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/maklumat_kursus/'.$detail['business_id']); ?>" />Next >></a>
		<?php }else if($mode == 'VIEW'){?>
			<a class="button" href="<?= site_url(ADMIN_DIR.'business/edit/maklumat_perniagaan/'.$detail['business_id']); ?>" />Edit</a>
		<?php } ?>
	</div>
	<div class="large-8 columns"></div>
</form>
</div>

<div class="detail_courses_wrap" style="display:none">

</div>