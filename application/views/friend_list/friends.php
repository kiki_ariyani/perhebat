	<h1>List Friends</h1>

	<?php if($this->session->flashdata('message_alert')){ ?>
	<div id="alert-message" class="row">
		<b><?=$this->session->flashdata('message_alert')?></b>
	</div>
	<?php } ?>

	<div id="tab" class="row">
		<ul class="reg-step">
			<li class="title"><a>List Friends</a></li>
			<li class="selected"><a href="<?=base_url()."friend_list/list_friend"?>">Friend List</a></li>
			<li><a href="<?=base_url()."friend_list/friend_request"?>">Friend Request</a></li>
			<li><a href="<?=base_url()."friend_list/pending_request"?>">Pending Request</a></li>
		</ul>
	</div>
	<div id="page" class="row no_table_label">
		<table class="dataTables biz-table message_list" cellpadding="5" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th>Name</th>
				</tr>
			</thead>
			<tbody>
			<?php $no=1; foreach($lists_friend as $list){ ?>
				<tr>
					<td><?=$no?></td>
					<td><?= $list['friend_name'] ?></td>
				</tr>
			<?php $no++; } ?>
			</tbody>
		</table>
	</div>