<h1>Friends List Request</h1>

	<?php if($this->session->flashdata('message_alert')){ ?>
<div id="alert-message" class="row">
	<b><?=$this->session->flashdata('message_alert')?></b>
</div>
<?php } ?>
	<div id="tab" class="row">
		<ul class="reg-step">
			<li class="title"><a>List Friends</a></li>
			<li><a href="<?=base_url()."friend_list/list_friend"?>">Friend List</a></li>
			<li class="<?= $type == 'friend_request' ? 'selected':''?>"><a href="<?=base_url()."friend_list/friend_request"?>">Friend Request</a></li>
			<li class="<?= $type == 'pending_request' ? 'selected':''?>"><a href="<?=base_url()."friend_list/pending_request"?>">Pending Request</a></li>
		</ul>
	</div>
	<div id="page" class="row no_table_label">
		<table class="dataTables biz-table message_list" cellpadding="5" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th>From</th>
					<th>To</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
			<?php $no=1; foreach($list_requests as $request){ ?>
				<tr>
					<td><?=$no?></td>
					<td><?= $request['inviter'] == $user_id ? "<b>You</b>" : $request['inviter_name'] ?></td>
					<td><?= $request['invitee'] == $user_id ? "<b>You</b>" : $request['invitee_name'] ?></td>
					<td>
						<?php if($request['inviter'] == $user_id && $request['status'] == 0){?>
						Pending Request
						<?php }else{?> 
						<a href="<?= base_url()?>friend_list/confirm/<?= $request['friend_list_id']?>" onClick="return confirm_friend();">Confirm</a>
						<a href="<?= base_url()?>friend_list/ignore/<?= $request['friend_list_id']?>" onClick="return ignore_friend();">Ignore</a>
						<?php }?>
					</td>
				</tr>
			<?php $no++; } ?>
			</tbody>
		</table>
	</div>