<?php $sess = $this->session->userdata('perniagaan_user');?>

<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,<?=$sess['business_id']?>,$('#form-change-password'),"<?= ($role_setting['view'] == 1 && $role_setting['edit'] == 0 ? 'view' : ''); ?>");
	});
</script>
<h1>CHANGE PASSWORD</h1>
<div id="page" class="row">
	<ul class="reg-step">
		<li class="title"><a>DATA</a></li>
		<li><a href="<?=base_url()."business/maklumat_diri/".$business_id?>">MAKLUMAT DIRI</a></li>
		<li><a href="<?=base_url()."business/maklumat_perniagaan/".$business_id?>">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="<?=base_url()."business/maklumat_kursus/".$business_id?>">MAKLUMAT KURSUS</a></li>
		<?php if($sess['business_id'] == $detail['business_id']){ ?>
		<li class="selected"><a href="#">CHANGE PASSWORD</a></li>
		<?php }else{ } ?>
	</ul>
</div>

<?php if($form_success['success'] == 'true') { ?>
<div id="alert-message" class="row">
	<?= $form_success['message']; ?>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url()."business/change_password/".$business_id?>" class="biz-form" enctype="multipart/form-data" id="form-change-password">
	<!-- <input type="hidden" name="business_id" value="<?=$detail['business_id']?>" /> -->
	<div class="large-4 columns">
		<label>Old Password <span class="red">*</span><span class="block-ita">Old Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="old_password" id="old_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>New Password <span class="red">*</span><span class="block-ita">New Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="new_password" id="new_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Confirm New Password <span class="red">*</span><span class="block-ita">Confirm New Password</span></label>
	</div>
	<div class="large-8 columns">	
		<div class="panel">
			<input class="valuebox" type="password" name="confirm_new_password" id="confirm_new_password" />
		</div>
	</div>
	<div class="large-4 columns">
		<input type="submit" name="save" class="button" value="Save" />	
	</div>
	<div class="large-8 columns"></div>
</form>
</div>