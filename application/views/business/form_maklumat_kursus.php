<?php $sess = $this->session->userdata('perniagaan_user');?>
<h1>MAKLUMAT KURSUS</h1>
<div id="page" class="row">
	<ul class="reg-step">
		<li class="title"><a>DATA</a></li>
		<li><a href="<?=base_url()."business/maklumat_diri/".$business_id?>">MAKLUMAT DIRI</a></li>
		<li><a href="<?=base_url()."business/maklumat_perniagaan/".$business_id?>">MAKLUMAT PERNIAGAAN</a></li>
		<li  class="selected"><a href="#">MAKLUMAT KURSUS</a></li>
		<?php if($sess['business_id'] == $business_id){ ?>
		<li><a href="<?=base_url()."business/change_password/".$business_id?>">CHANGE PASSWORD</a></li>
		<?php }else{ } ?>
	</ul>
</div>
<div id="page" class="row">
	<form>
	<div class="biz-form">
	<?php if($courses){
		foreach($courses as $course){ ?>
		<div class="large-4 columns">
			<label>Courses Name <span class="red">*</span><span class="block-ita">Nama Kursus</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel"><b><?=$course['name']?></b></div>
		</div>
		<div class="large-4 columns">
			<label>Trainer <span class="red">*</span><span class="block-ita">Penceramah</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel"><?=$course['trainer_name']?></div>
		</div>
		<div class="large-4 columns">
			<label>Description <span class="red">*</span><span class="block-ita">Huraian Khusus</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel"><?=$course['description']?></div>
		</div>
		<div class="large-4 columns">
			<label>Objective <span class="red">*</span><span class="block-ita">Jangkaan Outcome</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel"><?=$course['objective']?></div>
		</div>
		<div class="large-4 columns"><br></div>
		<div class="large-8 columns"><br></div>
		<!-- <div class="large-4 columns">	
			<a href="<?=base_url()."business/change_password/".$business_id?>"><input type="submit" name="next" class="button" value="Next >>" /></a>
		</div>
		<div class="large-8 columns"></div> -->
		<?=($course != end($courses) ? "<hr/><br/>":"")?>
	<?php }
	}else{
		echo "* This Business not attend any courses";
	}?>
	</div>
	</form>
</div>
