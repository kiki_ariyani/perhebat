<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,<?=$sess['business_id']?>,$('#form-maklumat-perniagaan'),"<?= ($role_setting['view'] == 1 && $role_setting['edit'] == 0 ? 'view' : ''); ?>");
	});
</script>
<h1>MAKLUMAT PERNIAGAAN</h1>
<div id="page" class="row">
	<ul class="reg-step">
		<li class="title"><a>DATA</a></li>
		<li><a href="<?=base_url()."business/maklumat_diri/".$detail['business_id']?>">MAKLUMAT DIRI</a></li>
		<li class="selected"><a href="#">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="<?=base_url()."business/maklumat_kursus/".$detail['business_id']?>">MAKLUMAT KURSUS</a></li>
		<?php if($sess['business_id'] == $detail['business_id']){ ?>
		<li><a href="<?=base_url()."business/change_password/".$detail['business_id']?>">CHANGE PASSWORD</a></li>
		<?php }else{ } ?>
	</ul>
</div>

<?php if($this->session->flashdata('form_msg') == 'true') { ?>
<div id="alert-message" class="row">
	<b>Your data has been saved.</b>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url()."business/save_maklumat_perniagaan"?>" class="biz-form" id="form-maklumat-perniagaan">
	<input type="hidden" name="business_id" value="<?=$detail['business_id']?>" />
	<div class="large-4 columns">
		<label>Work Status <span class="red">*</span><span class="block-ita">Status Pekerjaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="work_status" style="width:150px;">
				<?php foreach($work_status as $row){ ?>
					<option value="<?= $row['status_id']; ?>" <?=($row['status_id'] == $detail['work_status']?'selected="selected"':'')?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Registration No. <span class="red">*</span><span class="block-ita">No. Pendaftaran</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="registration_no" value="<?=$detail['registration_no']?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Registration Date <span class="red">*</span><span class="block-ita">Tarikh Daftar</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input readonly="readonly" class="valuebox datepicker" type="text" name="registration_date" value="<?=$detail['registration_date']?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Business Name <span class="red">*</span><span class="block-ita">Nama Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="business_name" value="<?=$detail['business_name']?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Business Address <span class="red">*</span><span class="block-ita">Alamat Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="23" name="business_address"><?=$detail['business_address']?></textarea>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Email/Web <span class="red">*</span><span class="block-ita">Email/Web</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="email_web" value="<?=$detail['email_web']?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Business Activity <span class="red">*</span><span class="block-ita">Aktiviti Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<?php foreach($activities as $activity){ ?>
				<input type="checkbox" name="business_activity[]" value="<?=$activity['activity_id']?>" <?php foreach($bus_activities as $bus_activity){ echo ($bus_activity['activity_id'] == $activity['activity_id'] ? "checked" : ""); }?>><?=$activity['name']?></input><br/>
			<?php } ?>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Main Product <span class="red">*</span><span class="block-ita">Product Utama</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="23" name="main_product"><?=$detail['main_product']?></textarea>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Account Type <span class="red">*</span><span class="block-ita">Jenis Akaun</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea rows="6" cols="23" name="account_type"><?=$detail['account_type']?></textarea>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Financing By <span class="red">*</span><span class="block-ita">Pembiayaan Oleh</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="financing_by" style="width:150px;">
				<option value="">- Select Financing -</option>
				<?php foreach($financing_bys as $financing_by){ ?>
					<option value="<?=$financing_by['financing_by_id']?>" <?=($detail['financing_by'] == $financing_by['financing_by_id']?'selected':'')?>><?=$financing_by['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns"><br></div>
	<div class="large-8 columns"><br></div>
	<div class="large-4 columns">
		<input type="submit" name="save"  class="button" value="Save" />	
		<input type="submit" name="next" class="button" value="Next >>" />	
	</div>
	<div class="large-8 columns"></div>
</form>
</div>