<script>
	$(document).ready(function(){
		disable_form(<?=$detail['business_id']?>,<?=$sess['business_id']?>,$('#form-maklumat-diri'),"<?= ($role_setting['view'] == 1 && $role_setting['edit'] == 0 ? 'view' : ''); ?>");
	});
</script>
<h1>MAKLUMAT DIRI</h1>
<div id="page" class="row">
	<ul class="reg-step">
		<li class="title"><a>DATA</a></li>
		<li class="selected"><a href="#">MAKLUMAT DIRI</a></li>
		<li><a href="<?=base_url()."business/maklumat_perniagaan/".$detail['business_id']?>">MAKLUMAT PERNIAGAAN</a></li>
		<li><a href="<?=base_url()."business/maklumat_kursus/".$detail['business_id']?>">MAKLUMAT KURSUS</a></li>
		<?php if($sess['business_id'] == $detail['business_id']){ ?>
		<li><a href="<?=base_url()."business/change_password/".$detail['business_id']?>">CHANGE PASSWORD</a></li>
		<?php }else{ } ?>
	</ul>
</div>

<?php if($this->session->flashdata('form_msg') == 'true') { ?>
<div id="alert-message" class="row">
	<b>Your data has been saved.</b>
</div>
<?php } ?>

<div id="page" class="row">
<form method="post" action="<?=base_url()."business/save_maklumat_diri"?>" class="biz-form" enctype="multipart/form-data" id="form-maklumat-diri">
	<input type="hidden" name="business_id" value="<?=$detail['business_id']?>" />
	<div class="large-4 columns">
		<label>Business Logo <span class="red">*</span><span class="block-ita">Logo Perniagaan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<?=($detail['business_logo'] != NULL ?"<img class='company-logo' style='max-width:400px' src='".base_url().PATH_TO_BUSINESS_LOGO ."/$detail[business_logo]' />": "No business logo <br/>")?>
			<?php if($detail['business_id'] == $sess['business_id']){ ?> <input type="file" name="business_logo"> <?php } ?>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Name <span class="red">*</span><span class="block-ita">Nama</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="name" value="<?=$detail['name']?>" id="person-name" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Category <span class="red">*</span><span class="block-ita">Kategori Perkhidmatan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="category" style="width:150px;">
				<option value="">- Select Category -</option>
				<?php foreach($categories as $category){ ?>
					<option value="<?=$category['category_id']?>" <?=($detail['category'] == $category['category_id']?'selected':'')?>><?=$category['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns">
		<label>IC No. <span class="red">*</span><span class="block-ita">IC No.</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" id="ic_no" name="ic_no" value="<?=$detail['ic_no']?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Age <span class="red">*</span><span class="block-ita">Umur</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" id="age" name="age" value="<?=$detail['age']?>" readonly="readonly" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Gender <span class="red">*</span><span class="block-ita">Jantina</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="gender" style="width:150px;" id="person-gender" <?=($detail['check_name'] == 'bin' || $detail['check_name'] == 'al'? 'disabled=disabled':'')?>>
				<option value="">- Select Gender -</option>
				<?php foreach($gender as $row){ ?>
					<option value="<?= $row['gender_id']; ?>" <?=($row['gender_id'] == $detail['gender']?'selected="selected"':'')?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Religion <span class="red">*</span><span class="block-ita">Agama</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="religion" style="width:150px;" id="person-religion" <?=($detail['check_name'] == 'bin'? 'disabled=disabled':'')?>>
				<option value="">- Select Religion -</option>
				<?php foreach($religion as $row){ ?>
					<option value="<?= $row['religion_id']; ?>" <?=($row['religion_id'] == $detail['religion']?'selected="selected"':'')?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Education Level <span class="red">*</span><span class="block-ita">Tahap Pendidikan</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="education_level" value="<?= $detail['education_level'];?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Service No. <span class="red">*</span><span class="block-ita">No. Tentara</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="army_no" value="<?=$detail['army_no']?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>Address <span class="red">*</span><span class="block-ita">Alamat</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<textarea id="address-geo" rows="6" cols="23" name="address"><?=$detail['address']?></textarea>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Telp No. <span class="red">*</span><span class="block-ita">No. Telefon</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input class="valuebox" type="text" name="telp_no" value="<?=$detail['telp_no']?>" />
		</div>
	</div>
	<div class="large-4 columns">
		<label>State <span class="red">*</span><span class="block-ita">Negeri</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="state_id">
				<option value="">- Select State -</option>
				<?php foreach($states as $state){ ?>
					<option value="<?=$state['state_id']?>" <?=($state['state_id'] == $detail['state_id']?'selected="selected"':'')?>><?=$state['name']?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Race <span class="red">*</span><span class="block-ita">Bangsa</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<select name="race" style="width:150px;">
				<?php foreach($race as $row){ ?>
					<option value="<?= $row['race_id']; ?>" <?=($row['race_id'] == $detail['race']?'selected="selected"':'')?>><?= $row['name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Status ATM <span class="red">*</span><span class="block-ita">Status ATM</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<div style="width:250px">
				<input style="width:20px" type="radio" name="status_atm" class="status_atm" value="<?= BAKAL_PESARA_ATM; ?>" <?= ($detail['status_atm'] == BAKAL_PESARA_ATM ? "checked" : ""); ?>>Bakal Pesara ATM
				<input style="width:20px" type="radio" name="status_atm" class="status_atm" value="<?= PESARA_ATM; ?>" <?= ($detail['status_atm'] == PESARA_ATM ? "checked" : ""); ?>>Pesara ATM
			</div>
		</div>
	</div>
	<div class="field status_pencen_wrap" style="display:<?= $detail['status_atm'] == PESARA_ATM ? "block" : "none"; ?>">
		<div class="large-4 columns">
			<label>Status Pencen <span class="red">*</span><span class="block-ita">Status Pencen</span></label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<div style="width:250px">
					<input style="width:20px" type="radio" name="status_pencen" class="status_pencen" value="<?= TIDAK_BERPENCEN; ?>" <?= ($detail['status_pencen'] == TIDAK_BERPENCEN ? "checked" : ""); ?>>Tidak Berpencen
					<input style="width:20px" type="radio" name="status_pencen" class="status_pencen" value="<?= BERPENCEN; ?>" <?= ($detail['status_pencen'] == BERPENCEN ? "checked" : ""); ?>>Berpencen
				</div>
			</div>
		</div>
	</div>
	<div class="large-4 columns">
		<label>Geographic Location <span class="red">*</span><span class="block-ita">Letak Geografis</span></label>
	</div>
	<div class="large-8 columns">
		<div class="panel">
			<input id="geo-location" class="valuebox" type="text" name="geo_location" value="<?=$detail['geo_location']?>" /> <b><a style="text-decoration:underline" target="_blank" href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm">Find Geo Location</a></b>
		</div>
	</div>
	
	<div class="large-4 columns"><br></div>
	<div class="large-8 columns"><br></div>
	
	<div class="large-4 columns">
		<input type="submit" name="save" class="button" value="Save" />	
		<input type="submit" name="next" class="button" value="Next >>" />	
	</div>
	<div class="large-8 columns"></div>
</form>
</div>