<?php echo$map['js'];?>
<h1>Search</h1>
<div id="page" class="row">
	<div class="box-search">
		<form method="post">
			<div class="category_wrap">
				<div class="inp">
					<input type="text" name="key" class="span6 typeahead"></br>
					<select name="state" style="width:250px;">
						<option value="0">All</option>
						<?php foreach ($states as $state){ ?>
							<option <?php echo ($datasearch['state']==$state->state_id)?"SELECTED":"";?> value="<?php echo $state->state_id; ?>"><?php echo $state->name; ?></option>
						<?php } ?>
					</select>
					<input value="Search" name="submit" type="submit" class="searchbottom btn btn-primary" >
				</div>
			</div>
		</form>
	</div>

	<div class="warper">
		<?php echo $map['html']; ?>
		<div class="clear"></div>
	</div>
</div>