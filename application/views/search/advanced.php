<h1>Advanced Search Business (Perniagaan)</h1>
<div id="page" class="row">
<form method="POST" class="filter-form">
	<div class="inline_box">
	<h3>Filter by :</h3>
		<div class="large-4 columns">
			<label>Category</label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<select name="s_category" style="width:250px;">
					<option value="">- ALL CATEGORY -</option>
					<?php foreach($categories as $category){?>
						<option value="<?=$category['category_id']?>" <?=($category['category_id'] == $form['s_cat']?'selected':'')?>><?=$category['name']?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="large-4 columns">
			<label>State</label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<select name="s_state" style="width:250px;">
					<option value="">- ALL STATE -</option>
					<?php foreach($states as $state){?>
						<option value="<?=$state['state_id']?>" <?=($state['state_id'] == $form['s_st']?'selected':'')?>><?=$state['name']?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="large-4 columns">
			<label>Financing By</label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<select name="s_financing" style="width:250px;">
					<option value="">- ALL TYPE -</option>
					<?php foreach($financing_bys as $financing_by){?>
						<option value="<?=$financing_by['financing_by_id']?>" <?=($financing_by['financing_by_id'] == $form['s_finc']?'selected':'')?>><?=$financing_by['name']?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="large-4 columns">
			<label>Work Status</label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<select name="s_work_status" style="width:250px;">
					<option value="">- ALL TYPE -</option>
					<?php foreach($work_status as $w_status){?>
						<option value="<?=$w_status['status_id']?>" <?=($w_status['status_id'] == $form['s_stat']?'selected':'')?>><?=$w_status['name']?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="large-4 columns">
			<label>Business Activity</label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<div class="checkboxes">
					<input type="checkbox" class="activity-option all" name="s_activity[]" value="0" <?=($form['s_act'] && in_array(0,$form['s_act']) ?'checked':(!$form['s_act'] ? 'checked':''))?>>ALL BUSINESS ACTIVITY</input><br/>
					<?php foreach($activities as $activity){?>
						<input type="checkbox" class="activity-option" name="s_activity[]" value="<?=$activity['activity_id']?>" <?=($form['s_act'] && in_array($activity['activity_id'],$form['s_act']) ?'checked':'')?>><?=$activity['name']?></input><br/>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="large-4 columns">
			<label>Trainer Code</label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<select name="s_trainer" style="width:250px;">
					<option value="">- ALL CODE -</option>
					<?php foreach($trainers as $trainer){?>
						<option value="<?=$trainer['trainer_id']?>" <?=($trainer['trainer_id'] == $form['s_train']?'selected':'')?>><?=$trainer['code']?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="large-4 columns">
			<label>Keyword</label>
		</div>
		<div class="large-8 columns">
			<div class="panel">
				<input type="text" name="s_keyword" value="<?=$form['s_key']?>" />
			</div>
		</div>
		<input type="submit" value="SEARCH" name="search" class="button" />
</form>

<br/><br/>
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th>No.</th>
			<th>Business Name</th>
			<th>Person Name</th>
			<th>Category</th>				
			<th>Registration No.</th>
			<th>Email/Website</th>
			<th>State</th>
			<th>Training Provider</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php $no=1; foreach($business as $business){ ?>
		<tr>
			<td><?=$no?></td>
			<td><?=$business['business_name']?></td>
			<td><?=$business['person_name']?></td>
			<td><?php foreach($categories as $category){ echo ($category['category_id'] == $business['category'] ? $category['name'] : ""); } ?></td>
			<td><?=$business['registration_no']?></td>
			<td><?=$business['email_web']?></td>
			<td><?php foreach($states as $state){ echo ($state['state_id'] == $business['state_id'] ? $state['name']: ""); } ?></td>
			<td><?=$business['trainers']?></td>
			<td><?php if($type == 'admin'){ ?>
				 <a target="_blank" href="<?= base_url().ADMIN_DIR."business/edit/".$business['business_id'];?>">Edit</a> | 
				<?php } ?>
				<a target="_blank" href="<?=($type=='admin' ? base_url().ADMIN_DIR."business/view/".$business['business_id'] : base_url()."business/maklumat_diri/".$business['business_id'])?>">View Detail</a>
			</td>
		</tr>
	<?php $no++; } ?>
	</tbody>
</table>
</div>
