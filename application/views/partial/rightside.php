<style type="text/css">
.right{
	float: right;
	width: 225px;
}
</style>
<?php $sess = $this->session->userdata('perniagaan_user');?>
<?php if($sess){ ?>
<div class="right">
	<div class="box_right">
		<h3 style="margin:0;color: rgb(23, 135, 202);">Announcements</h3>
		<br>
		<ul>
			<?php 
			$i = 0;
			foreach ($announcements as $announ) {
				if($i < 5){ ?>
					<li><a href="<?=base_url()."site/article/".$announ['article_id']?>"><?=$announ['title'];?></a></li>
				<?php } 
				$i++; ?>
			<?php } ?>
		</ul>
	</div>
	<div class="box_right">
		<h3 style="margin:0;color: rgb(23, 135, 202);">Program Highlights</h3>
		<br>
		<ul>
			<?php 
			$i = 0;
			foreach ($program_highs as $high) {
				if($i < 5){ ?>
					<li><a href="<?=base_url()."site/article/".$high['article_id']?>"><?=$high['title'];?></a></li>
				<?php } 
				$i++; ?>
			<?php } ?>
		</ul>
	</div>
</div>
<?php }else{ ?>
<div class="right">
	<div class="box_right">
		<div class="box-wrap">
			<h3 style="margin:0;color: rgb(23, 135, 202);">Sign In</h3>
			<div class="row-fluid" style="margin: 10px 0 0 0;">
				<?php if($this->session->flashdata('login_msg') == 'false') { 
							echo "<div class='reg-msg-false' style='background-color: rgba(226, 192, 155, 0.26);padding: 2px 2px 2px 3px;border-color: rgb(150, 8, 8);border-style: solid;border-width: 1px;'>
									Login Error! Username and password did not match or maybe your account not activated (suspended).
								  </div>"; 
					}?>
				<div id="panel" class="well span5 login-box" style="margin:0;width: 226px;height: 130px;z-index: 100;">
					
					<form method="post" action="<?=base_url()."site/login"?>">
						<fieldset>
							<div class="input-prepend" title="Username" data-rel="tooltip" style="height: 20px;margin-top: 5px;">
								<span class="add-on"><i class="icon-user"></i></span><input autofocus class="input-large span10" name="username" id="username" type="text" value="admin" placeholder="username" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" value="admin123456" placeholder="password" />
							</div>
							<div class="clearfix"></div>

							<!--<div class="input-prepend">
							<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>
							</div>
							<div class="clearfix"></div>-->

							<p class="span5" style="width: 50px;float: right;margin-top: -19px;">
							<button type="submit" class="btn btn-primary">Login</button>
							</p>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="box_right">
		<h3 style="margin:0;color: rgb(23, 135, 202);">Announcements</h3>
		<br>
		<ul>
			<?php 
			$i = 0;
			foreach ($announcements as $announ) {
				if($i < 5){ ?>
					<li><a href="<?=base_url()."site/article/".$announ['article_id']?>"><?=$announ['title'];?></a></li>
				<?php } 
				$i++; ?>
			<?php } ?>
		</ul>
	</div>
	<div class="box_right">
		<h3 style="margin:0;color: rgb(23, 135, 202);">Program Highlights</h3>
		<br>
		<ul>
			<?php 
			$i = 0;
			foreach ($program_highs as $high) {
				if($i < 5){ ?>
					<li><a href="<?=base_url()."site/article/".$high['article_id']?>"><?=$high['title'];?></a></li>
				<?php } 
				$i++; ?>
			<?php } ?>
		</ul>
	</div>
</div>

<?php } ?>