<div class="boxmg">
				<?php $sess = $this->session->userdata('perniagaan_user');?>
				<?php if($sess){ 
					$username = explode("@", $sess['email']); ?>
					<!-- Menu -->
					<span style="color:white" class="desk"><img src="<?=base_url()."assets/img/images.jpg"?>" class="va-top"><?=$username[0]?></span>
					<div class="log-navi box">
						<!-- <img src="<?=base_url()."assets/img/images.jpg"?>" class="va-top"> -->
						<ul>
							<li><a href="<?=base_url()."message/inbox"?>" class="inbox">Inbox <span class="notif_inbox"></span></a></li>
							<li><a href="<?=base_url()."message/outbox"?>" class="outbox">Outbox</a></li>
							<?php foreach($menu as $row){ 
									$row['url'] = str_ireplace(array('[user_id]'),array($sess['business_id']),$row['url']); ?>
								<!--<a href="<?=base_url()."business/maklumat_diri/".$sess['business_id']?>">Edit Data</a>-->
									<li><a href="<?=base_url().$row['url']; ?>" class="edit"><?= $row['name']; ?></a></li>
							<?php } ?>
							<li><a href="<?=base_url()."friend_list/friend_request"?>" class="request_friend">Friend Request <span class="notif_friend_request"></span></a></li>
							<li><a href="<?=base_url()."site/logout"?>" class="logout">Logout</a></li>
						</ul>

					</div>
				<?php }else{ ?>
					<span style="color:white" class="desk">Login</span>
					<?php if($messages){ ?>
						<span style="background:white;text-transform:none !important" class="desk">Login Error! Username and Password did not match.</span>
					<?php } ?>
					<ul class="box">
						<li>
						<form method="post" action="<?=base_url()."site/login"?>" class="row">
							<input name="username" id="username" type="text" placeholder="username" />
							<input name="password" id="password" type="password" placeholder="password"/>
							<button type="submit" class="button postfix">Sign In</button>
						</form>
						<li>
					</ul>
				<?php } ?>
			</div>
			</div>

			<div id="aside">
			<span class="desk">Announcements</span>

			<ul class="box">
				<li>
					<?php 
					$i = 0;
					foreach ($announcements as $announ) {
						if($i < 5){ ?>
							<li><a href="<?=base_url()."site/article/".$announ['article_id']?>" class="message"><?=$announ['title'];?></a></li>
						<?php } 
						$i++; ?>
					<?php } ?>
				</li>
			</ul>
			</div>

			<div id="aside">
			<span class="desk">Program Highlights</span>

			<ul class="box">
				<li>
					<?php 
					$i = 0;
					foreach ($program_highs as $high) {
						if($i < 5){ ?>
							<li><a href="<?=base_url()."site/article/".$high['article_id']?>" class="message"><?=$high['title'];?></a></li>
						<?php } 
						$i++; ?>
					<?php } ?>
				</li>
			</ul>
		</div>