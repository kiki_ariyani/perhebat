<?php $sess = $this->session->userdata('perniagaan_user');?>
<?php if($sess){ 
	$username = explode("@", $sess['email']); ?>
	<!-- Menu -->
	<div id="menu">
		<div class="nav row">
			<img class="header-per" src="<?=base_url()."assets/img/perhebat.png"?>" style="float:left">
			<ul class="menu log-navi">
				<li><a href="#"><?=$username[0]?></a></li>
				<li style="margin-right:10px;"><img src="<?=base_url()."assets/img/images.jpg"?>"></li>
				<li><a href="<?=base_url()."site/logout"?>" class="logout"><img src="<?=base_url()."assets/img/out.png"?>"></a>
				</li>
			</ul>
		</div>
	</div>
	<div id="navigation">
		<div class="nav row">
			<a href="<?=base_url()."message/inbox"?>" class="button login small">Inbox</a>
			<a href="<?=base_url()."message/outbox"?>" class="button login small">Outbox</a>

			<!-- <a href="<?=base_url()."site/logout"?>" class="button alert small right">Logout</a> -->
			<a href="<?=base_url()."business/maklumat_diri/".$sess['business_id']?>" class="button success small right">Edit Data</a>
		</div>
	</div>
<?php }else{ ?>
	<div id="menu">
		<div class="nav row">
			<img class="header-per" src="<?=base_url()."assets/img/perhebat.png"?>" style="float:left">
			<div class="perhebat_link">
				<!--<a href="<?=base_url()?>">Join we us!</a>-->
				<?php foreach($menu as $row){ 
						$row['url'] = str_ireplace(array('[user_id]'),array($sess['business_id']),$row['url']); ?>
						<a href="<?=base_url().$row['url']; ?>" class="button login"><?= $row['name']; ?></a>
				<?php } ?>
			</div>
		</div>
	</div>
	<div id="navigation">
		<div class="nav row">
			<h2 style="margin:0;font-weight:normal;font-style:italic;text-align:center;">Please, login first</h2>
		</div>
	</div>
<?php } ?>
	<!-- /menu -->