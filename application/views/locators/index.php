<?php echo$map['js'];?>
<h1>Business Locator</h1>
<div id="page" class="row">
	<div class="box-search">
		<form method="post" class="form-locator">
			<div class="category_wrap">
				<div class="inp">
					<input type="text" name="key" class="span6 typeahead"></br>
					<select name="state" style="width:220px;height:25px">
						<option value="0">All</option>
						<?php foreach ($states as $state){ ?>
							<option <?php echo ($datasearch['state']==$state->state_id)?"SELECTED":"";?> value="<?php echo $state->state_id; ?>"><?php echo $state->name; ?></option>
						<?php } ?>
					</select>
					<input value="Search" name="submit" type="submit" class="button" style="height: 25px;padding-top: 6px;">
				</div>
			</div>
		</form>
	</div>
	<div class="legend_wrap">
		<img src="<?= base_url()."assets/img/map_icon/red.png"; ?>">Manufacturing / Pembuatan
		<img src="<?= base_url()."assets/img/map_icon/green.png"; ?>">Services / Perkhidmatan
		<img src="<?= base_url()."assets/img/map_icon/blue.png"; ?>">Agriculture / Pertanian
		<img src="<?= base_url()."assets/img/map_icon/yellow.png"; ?>">Trade / Perdagangan
		<img src="<?= base_url()."assets/img/map_icon/grey.png"; ?>">Livestock / Penternakan
		<img src="<?= base_url()."assets/img/map_icon/red-square.png"; ?>">UTC
		<img src="<?= base_url()."assets/img/map_icon/green-tri.png"; ?>">RTC
		<img src="<?= base_url()."assets/img/map_icon/yellow-tri.png"; ?>">Kedai 1 Malaysia
	</div>
	<div id="legend"></div>
	<br/>
	<div class="warper">
		<?php echo $map['html']; ?>
		<div class="clear"></div>
	</div>
</div>