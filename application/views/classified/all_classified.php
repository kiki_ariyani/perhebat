<h1>Classifieds Center</h1>

<div id="page" class="row no_table_label">
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="3%">No. </th>
			<th>Company Name</th>
			<th>Type</th>
			<th>Title</th>
			<th>Desciption</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$no = 1;
		foreach($classifieds as $classified){ ?>
			<tr>
				<td><?=$no?></td>
				<td><?=$classified['name']?></td>
				<td><?=($classified['type'] == LOOKING_FOR ? 'Looking For' : 'Offering')?></td>
				<td><?=$classified['title']?></td>
				<td><?=$classified['description']?></td>
				<td><a href="<?= site_url('classifieds/view/'.$classified['classified_id']); ?>" class="view" title="View Detail"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a></td>
			</tr>
		<?php $no++;} ?>
	</tbody>
</table>
</div>
