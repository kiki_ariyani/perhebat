<h1>Classifieds</h1>

<?php if($messages != NULL) { ?>
<div id="alert-message" class="row">
	<?php echo $messages;?>
</div>
<?php } ?>
<br/>
<?php
if($classified_count >= 2){
	echo "<div style='color:red;font-weight:bold;margin-bottom:20px;'>You cannot add new products data, you only can add 2 classified</div>";
}else{
	echo "<a href='".site_url('classifieds/add/')."' class='button' style='margin:0 !important'>Add Classified</a>";
} ?>

<div id="page" class="row no_table_label">
<table class="dataTables biz-table" width="100%" cellpadding="5">
	<thead>
		<tr>
			<th width="3%">No. </th>
			<th width="20%">Title</th>
			<th>Type</th>
			<th width="20%">Contact Person</th>
			<th width="20%">Created Date</th>
			<th width="10%">Status</th>
			<th width="13%">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
		$no = 1;
		foreach($classifieds as $classified){ ?>
			<tr>
				<td><?= $no; ?></td>
				<td><?= $classified['title']; ?></td>
				<td><?php foreach($type as $row){ echo ($classified['type'] == $row['value'] ? $row['label'] : ""); } ?></td>
				<!--<td><?= $classified['looking_for']; ?></td>-->
				<td><?= $classified['contact_person']; ?></td>
				<td><?= $classified['created_date']; ?></td>
				<td><?=(date('Y-m-d') >= $classified['end_date'] ? "<label style='color:red'>Expired</label>" : "-")?></td>
				<td><?php if (date('Y-m-d') < $classified['end_date']) {?>
						<a href="<?= site_url('classifieds/edit/'.$classified['classified_id']); ?>" class="edit" title="Edit"><img src="<?=base_url()."assets/img/edit1.png"?>" title="Edit"></a>
					<?php } else {?>
						<a href="" class="edit" onclick="alert('You cannot edit classified, classified has expired')" title="Edit"></a>
					<?php } ?>
						<a href="<?= site_url('classifieds/view/'.$classified['classified_id']); ?>" class="view" title="View"><img src="<?=base_url()."assets/img/view.png"?>" title="View"></a>
						<!--<a href="<?= site_url('classifieds/delete/'.$classified['classified_id']); ?>" onClick="return confirm_delete();"><img src="<?=base_url()."assets/img/dumb.png"?>" title="Delete"></a>-->
				</td>
			</tr>
		<?php $no++;} ?>
	</tbody>
</table>
</div>
