<h2>Edit Profile</h2>
<?php if($this->session->flashdata('update_msg') == 'false') { 
	echo "<div class='reg-msg-false'>Your old password is wrong!</div>"; 
}elseif($this->session->flashdata('update_msg') == 'true'){
	echo "<div class='reg-msg-true'>Your profile updated.</div>"; 
} ?>
<form method="post" class="biz-form" id="user-form">
	<div class="field">
		<div class="label-field">
			<label>Old Password <span class="mandatory">*</span></label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="password" name="old_password" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>New Password <span class="mandatory">*</span></label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="password" name="new_password" />
		</div>
	</div>
	<div class="field">
		<div class="label-field">
			<label>Confirm New Password <span class="mandatory">*</span></label>
		</div>
		<div class="value-field">
			<input class="valuebox" type="password" name="confirm_password" />
		</div>
	</div>
	<div class="field">
		<label></label>
		<input type="submit" name="save" value="Save" />
	</div>
</form>