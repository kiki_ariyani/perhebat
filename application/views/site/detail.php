<h1>Detail Company</h1>
<div id="page" class="row">
	<br/>
	<form method="post" class="biz-form" id="user-form">
		<div class="large-4 columns">
			<label>Name</label>
		</div>
		<div class="large-8 columns">	
			<div class="panel"><?= $person['name']; ?></div>
		</div>
		<div class="large-4 columns">
			<label>Address</label>
		</div>
		<div class="large-8 columns">	
			<div class="panel"><?= $person['address']; ?></div>
		</div>
		<div class="large-4 columns">
			<label>State</label>
		</div>
		<div class="large-8 columns">	
			<div class="panel"><?= $state['name']; ?></div>
		</div>
		<div class="large-4 columns">
			<label>Business Name</label>
		</div>
		<div class="large-8 columns">	
			<div class="panel">
				<?php
					if($business['business_name'] == null){
						echo "-";
					}else{
						echo $business['business_name'];
					}
				?>
			</div>
		</div>
		<div class="large-4 columns">
			<label>Business Activity</label>
		</div>
		<div class="large-8 columns">	
			<div class="panel">
				<?php
					if($activities == null){
						echo "-";
					}else{
						foreach($activities as $row){
							echo "- ".$row."<br/>";
						}
					}
				?>
			</div>
		</div>
		<div class="large-4 columns">
			<a class="button" href="<?= base_url(); ?>" />Back</a>
		</div>
		<div class="large-8 columns"></div>
	</form>
</div>