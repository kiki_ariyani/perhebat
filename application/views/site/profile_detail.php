<h1><?php echo $highlight['name'];?></h1>
<div id="page" class="row">
	<div class="inline_box">
		<?php $thumb = file_exists(realpath(APPPATH . '../assets/attachment/content') . DIRECTORY_SEPARATOR . $highlight['highlight_id'] . ".jpg") ? BASE_URL() . "assets/attachment/content/" . $highlight['highlight_id'] . ".jpg?".rand() : ""; 
			if($thumb){ ?>
				<img src="<?= $thumb; ?>" class="profile-image"/>
		<?php 		} ?>
		<p class="profile-desc" style="padding-top:10px !important"><?= $highlight['description']; ?></p>
	</div>
	<div class="profile-button-wrap"><a href="<?= base_url(); ?>" class="button">Back</a></div>
</div>