<script>
	$(document).ready(function(){
		$(".dataTables").dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"iDisplayLength" : 20,
			"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
		});
	});
</script>
<?php $sess = $this->session->userdata('perniagaan_user');
if($sess){ ?>
	<script type="text/javascript" src="<?= base_url()?>assets/js/site/friend_list.js"></script>
	<h1>Business List</h1>
	<?php if($this->session->flashdata('message_alert')){ ?>
	<div id="alert-message" class="row">
		<b><?=$this->session->flashdata('message_alert')?></b>
	</div>
	<?php } ?>
	<div id="page" class="row no_table_label">		
		<table class="dataTables biz-table" cellpadding="5">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th>Business Name</th>
					<th>Person Name</th>
					<th>Kategori Perkhidmatan</th>
					<th>Registration No.</th>
					<!-- <th>Email Website</th> -->
					<!-- <th>Business Address</th> -->
					<th>Main Product</th>
					<th>State</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php $no=1; foreach($business as $business){ ?>
				<tr>
					<td><?=$no?></td>
					<td><?=$business['business_name']?></td>
					<td><a href="<?=base_url()."business/maklumat_diri/".$business['business_id']?>"><?=$business['person_name']?></a></td>
					<td><?php foreach($categories as $category){ echo ($category['category_id'] == $business['category'] ? $category['name']: ""); } ?></td>
					<td><?=$business['registration_no']?></td>
					<!-- <td><?=$business['email_web']?></td> -->
					<!-- <td><?=$business['business_address']?></td> -->
					<td><?=$business['main_product']?></td>
					<td><?php foreach($states as $state){ echo ($state['state_id'] == $business['state_id'] ? $state['name']: ""); } ?></td>
					<td width="9%">
						<a href="<?=base_url()."message/send/".$business['user_id']?>" class="send_message float_l" title="Send Message"></a>
						<a href="<?=base_url()."friend_list/invite/".$business['user_id']?>" title="Add to Friends List" class="add_friend <?= $business['friend_status'] == -1 ? '' :'hidden' ?>"></a>
						<div class="pending_friend <?= $business['friend_status'] == 0 ? '' :'hidden' ?>">&nbsp;</div>
						<div class="friend <?= $business['friend_status'] == 1 ? '' :'hidden' ?>">&nbsp;</div>
					</td>
				</tr>
			<?php $no++; } ?>
			</tbody>
		</table>
	</div>
<?php }else{ ?>
		<?php if($program_highlight){ ?>
			<div class="highlight-profile-wrap">
			<img src="<?=base_url()."assets/img/perhebat.png"?>" style="width:130px;margin:10px;">
			<div class="profile-row">
				<?php foreach($program_highlight as $highlight){ ?>
					<div class="profile-column">
						<div class="profile-title"><a href="<?= base_url()."site/profile/".$highlight['highlight_id'] ?>"><?= $highlight['name']; ?></a></div>
						<?php $thumb = file_exists(realpath(APPPATH . '../assets/attachment/content') . DIRECTORY_SEPARATOR . $highlight['highlight_id'] . ".jpg") ? BASE_URL() . "assets/attachment/content/" . $highlight['highlight_id'] . ".jpg?".rand() : ""; 
							if($thumb){ ?>
								<img src="<?= $thumb; ?>" class="profile-image"/>
						<?php 		} ?>
						<p class="profile-desc"><?= substr($highlight['description'],0,550); ?></p>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
		<h1 style="margin-top:20px">Business List</h1>
		<div id="page" class="row no_table_label">
			<table class="dataTables  biz-table" width="100%" cellpadding="5">
				<thead>
					<tr>
						<th width="5%">No.</th>
						<th>Name</th>
						<th>Kategori Perkhidmatan</th>
						<th>State</th>
					</tr>
				</thead>
				<tbody>
				<?php $no=1; foreach($business as $business){ ?>
					<tr>
						<td><?=$no?></td>
						<td><a href="<?=base_url()."site/detail/".$business['business_id']?>" target="_blank" title="Detail Company"><?=$business['person_name']?></a></td>
						<td><?php foreach($categories as $category){ echo ($category['category_id'] == $business['category'] ? $category['name']: ""); } ?></td>
						<td><?php foreach($states as $state){ echo ($state['state_id'] == $business['state_id'] ? $state['name']: ""); } ?></td>
					</tr>
				<?php $no++; } ?>
				</tbody>
			</table>
		</div>
<?php } ?>