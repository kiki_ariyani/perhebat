<?php


if ( ! function_exists('time_now'))
{
	function time_now()
	{
		$CI =& get_instance();

		if (strtolower($CI->config->item('time_reference')) == 'gmt')
		{
			$now = time();
			$system_time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));

			if (strlen($system_time) < 10)
			{
				$system_time = time();
				log_message('error', 'The Date class could not set a proper GMT timestamp so the local time() value was used.');
			}

			return $system_time;
		}
		else
		{
			return time();
		}
	}
}

/**Elapsed Time */

function elapsed_time($seconds) {
	  if (!is_numeric($seconds)) {
		$seconds = strtotime($seconds);
	  }
	  
	  $time_now 	= strtotime(gmdate("Y-m-d H:i:s", time()+60*60*8));
	  $seconds 		= $time_now - $seconds;
	  
	  $days = floor($seconds / 86400);
	  if ($days > 0) {
		  if ($days == 1) {
			  return '1 day';
		  }
		return $days . ' days';
	  }
	  
	  $hours = floor($seconds / 3600);
	  if ($hours > 0) {
		if ($hours == 1) {
		  return '1 hour';
		}
		return $hours . ' hours';
	  }
	  
	  $minutes = floor($seconds / 60);
	  if ($minutes > 0) {
		if ($minutes == 1) {
		  return '1 minute ';
		}
		return $minutes . ' minutes';
	  }
	  
	  if ($seconds <= 1) {
		return '1 second';
	   } else {
		return $seconds . ' seconds';
	  }
} 