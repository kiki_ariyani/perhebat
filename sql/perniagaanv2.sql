-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 26, 2013 at 08:55 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `perniagaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`activity_id`, `name`) VALUES
(1, 'Manufacturing'),
(2, 'Services'),
(3, 'Agriculture');

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE IF NOT EXISTS `business` (
  `business_id` int(11) NOT NULL,
  `registration_no` varchar(256) NOT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `business_name` varchar(256) NOT NULL,
  `business_address` mediumtext NOT NULL,
  `email_web` varchar(256) NOT NULL,
  `main_product` mediumtext NOT NULL,
  `account_type` mediumtext NOT NULL,
  `financing_by` int(1) NOT NULL COMMENT '1=''SENDIRI'', 2=''TEKUN''',
  PRIMARY KEY (`business_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`business_id`, `registration_no`, `registration_date`, `business_name`, `business_address`, `email_web`, `main_product`, `account_type`, `financing_by`) VALUES
(1, 'SA0191451-V', '2013-02-26 07:37:52', 'VISI RS ENTERPRISE', 'NO. 33, JALAN MUHIBAH 25/7, SEKSYEN 25 TAMAN SRI MUDA, 40400 SHAH ALAM, SELANGOR', 'vcrs02@yahoo.com', 'KONTRAKTOR BINAAN, KAWALAN SERANGGA, MEMBERSIH KAWASAN & MENGECAT', 'MAYBANK', 1),
(2, 'MP0001576-U', '2013-02-26 03:43:27', 'FOUR CIRCLE RESOURCES', 'NO 11 JALAN ANGGERIK, PRIMA BERUNTUNG, 48300 RAWANG, SELANGOR', 'www.razicoenterprise.com', 'UBAT GIGI BERUBAT: EKSTRAK GAMAT & CENGKIH', 'MUAMALAT\r\n', 1),
(3, '001436958-M', '0000-00-00 00:00:00', 'DULENA ENTERPRISE', 'LOT 822-B, JALAN BESTARI JAYA, BATU 4 1/2, KAMPUNG KUANTAN, 45000 KUALA SELANGOR, SELANGOR\r\n', 'azizali201@yahoo.com', 'PEMBUATAN & PERKHIDMATAN\r\n', 'CIMB BANK\r\n', 2),
(4, '', '2013-02-26 03:49:06', '', '', '', '', '', 0),
(5, 'MP0002275-V', '2013-02-26 03:49:06', 'JALIL JAYA ENTERPRISE', 'LOT 42 JALAN HUTAN LIPUR BUKIT LAGONG, JALAN BUKIT IDAMAN, 68100 BATU CAVES, SELANGOR\r\n', '', 'PERKHIDMATAN PENGURUSAN TANAMAN, LADANG, TAMAN HUTAN DAN LADANG HUTAN', 'MAYBANK\r\n', 1),
(6, 'AS0353759-A', '2013-02-26 03:59:42', 'MOHD ZAMRI BIN RIJAN', 'NO 28,LORONG 23 TMN GUAR PERAHU, 14400 BUKIT MERTAJAM, P.PINANG\r\n', '', 'AISKRIM GOYANG, MAKANAN RINGAN\r\n', '', 1),
(7, '', '2013-02-26 03:59:42', 'V. RAJENDRAN VEERASAMY', '47 LRG SENTUL 13 TMN SENTUL JAYA JURU 14100 BUKIT MERTAJAM,P.PINANG\r\n', '', '', '', 0),
(8, '', '2013-02-26 04:01:23', 'ALIAS BIN MAT DESA', '12 LORONG GUAR PERAHU 39, TAMAN GUAR PERAHU, 14000 B.MERTAJAM ,P.PINANG\r\n', '', '', '', 0),
(9, '', '2013-02-26 04:01:23', 'JAAFAR BIN SAAD', 'NO. 6125 ALOR MERAH, SUNGAI DUA, 13800 BUTTERWORTH P.PINANG\r\n', '', '', '', 0),
(10, '', '2013-02-26 04:01:40', 'ZAMRI BIN AHMAD', 'NO. 89 LORONG KTC 2/4, TAMAN KTC FASA 2, 09000 KULIM, KEDAH\r\n', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `business_activity`
--

CREATE TABLE IF NOT EXISTS `business_activity` (
  `business_activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  PRIMARY KEY (`business_activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `business_activity`
--

INSERT INTO `business_activity` (`business_activity_id`, `business_id`, `activity_id`) VALUES
(2, 2, 1),
(3, 3, 1),
(4, 3, 2),
(5, 5, 3),
(6, 6, 1),
(7, 6, 2),
(9, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `business_person`
--

CREATE TABLE IF NOT EXISTS `business_person` (
  `business_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `category` int(11) NOT NULL,
  `ic_no` varchar(256) NOT NULL,
  `age` int(3) NOT NULL,
  `army_no` varchar(256) NOT NULL,
  `address` mediumtext NOT NULL,
  `telp_no` varchar(30) NOT NULL,
  `state_id` int(11) NOT NULL,
  `geo_location` varchar(256) NOT NULL,
  `business_logo` mediumtext NOT NULL,
  PRIMARY KEY (`business_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `business_person`
--

INSERT INTO `business_person` (`business_id`, `name`, `category`, `ic_no`, `age`, `army_no`, `address`, `telp_no`, `state_id`, `geo_location`, `business_logo`) VALUES
(1, 'AB RAHMAN BIN AHMAD', 1, '560502-02-5589', 57, '530243', 'NO. 33, JALAN MUHIBAH 25/7, SEKSYEN 25 TAMAN SRI MUDA, 40400 SHAH ALAM, SELANGOR', '017-2631190', 10, '', ''),
(2, 'ABD ALI BIN MUSTAFFA', 1, '621218-02-5131', 51, '1052152', 'NO 11 JALAN ANGGERIK, PRIMA BERUNTUNG, 48300 RAWANG, SELANGOR', '019-5278727', 10, '', ''),
(3, 'ABDUL AZIZ BIN MOHD ALI', 1, '741109-05-5071', 38, '1097090', 'NO. 31 JALAN SIRAMBAI 4 TAMAN SIRAMBAI, 45000 KUALA SELANGOR, SELANGOR', '019-6005674', 10, '', ''),
(4, 'ABDUL HARIS BIN HUSAIN', 1, '591115-10-6057', 53, '3001321', 'NO. 18 JALAN 11/23B DANAU KOTA, SETAPAK, SELANGOR', '', 10, '', ''),
(5, 'ABDUL JALIL BIN AHMAD', 2, '330522-04-5073', 80, '1087', 'LOT 42 JALAN HUTAN LIPUR BUKIT LAGONG, JALAN BUKIT IDAMAN, 68100 BATU CAVES, SELANGOR', '013-2966251', 10, '', ''),
(6, 'MOHD ZAMRI BIN RIJAN', 1, '550915-08-6161', 57, '139323', 'NO 28,LORONG 23 TMN GUAR PERAHU, 14400 BUKIT MERTAJAM, P.PINANG\r\n', '012-5756042', 9, '', ''),
(7, 'V. RAJENDRAN A/L VEERASAMY', 1, '580630-07-5255', 55, '1028116', '47 LRG SENTUL 13, TMN SENTUL JAYA, 14100 BUKIT MERTAJAM,P.PINANG\r\n', '016-4159490', 9, '', ''),
(8, 'ALIAS BIN MAT DESA', 2, '610723-08-6425', 51, '711231', '12 LORONG GUAR PERAHU 39, TAMAN GUAR PERAHU, 14000 B.MERTAJAM ,P.PINANG\r\n', '019-6998166', 9, '', ''),
(9, 'JAAFAR BIN SAAD', 2, '531010-02-5221', 59, '705256', 'NO. 6125 ALOR MERAH, SUNGAI DUA, 13800 BUTTERWORTH P.PINANG\r\n', '019-4977175', 9, '', ''),
(10, 'ZAMRI BIN AHMAD', 1, '660623-07-5089', 47, 'T3005606', 'NO. 89 LORONG KTC 2/4, TAMAN KTC FASA 2, 09000 KULIM, KEDAH\r\n', '017-4396071', 9, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(46) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('b7edeabd1bdbc1fbde580ea7650f2009', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22', 1361865145, 'a:1:{s:15:"perniagaan_user";a:6:{s:7:"user_id";s:1:"1";s:7:"role_id";s:1:"2";s:11:"business_id";s:1:"1";s:5:"email";s:15:"zamri@gmail.com";s:8:"password";s:32:"ae2b1fca515949e5d54fb22b8ed95575";s:17:"activation_status";s:1:"1";}}');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE IF NOT EXISTS `state` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`state_id`),
  KEY `name` (`name`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `name`) VALUES
(1, 'Johor'),
(2, 'Kedah'),
(3, 'Kelantan'),
(4, 'Melaka'),
(5, 'Negeri Sembilan'),
(6, 'Pahang'),
(7, 'Perak'),
(8, 'Perlis'),
(9, 'Pulau Pinang'),
(10, 'Selangor'),
(11, 'Terengganu'),
(12, 'Sabah'),
(13, 'Serawak'),
(14, 'Wilayah Persekutuan Kuala Lumpur'),
(15, 'Wilayah Persekutuan Labuan'),
(16, 'Wilayah Persekutuan Putrajaya');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(1) NOT NULL COMMENT '1=''ADMIN'', 2=''USER''',
  `business_id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` mediumtext NOT NULL,
  `activation_status` tinyint(1) NOT NULL COMMENT '0=''SUSPEND'', 1=''ACTIVE''''',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `role_id`, `business_id`, `email`, `password`, `activation_status`) VALUES
(1, 2, 1, 'zamri@gmail.com', 'ae2b1fca515949e5d54fb22b8ed95575', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
