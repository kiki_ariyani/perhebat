$(document).ready(function() {
	$("#form-change-password").validate({
		rules: {
			old_password : 'required',
			new_password : 'required',
			confirm_new_password:{
				equalTo : '#new_password',
				required : true
			}
		},
        errorElement: "div",
		  errorPlacement: function(error, element) {
                error.appendTo(element.parent());
				error.addClass("note_field");
            },
         highlight: function(element, errorClass, validClass) {
			$(element).parents(".form_field").addClass("error_field");
			$(element).addClass(errorClass).removeClass(validClass);
			$(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents(".form_field").removeClass("error_field");
			$(element).removeClass(errorClass).addClass(validClass);
			$(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
		} 
    });

	/*MAKLUMAT DIRI FORM VALIDATION *************************************************************************/
	$("#form-maklumat-diri").validate({
        rules: {
			'name' : {
				required: true
			},
			'category' : {
				required: true,
			},
			'ic_no' : {
				required: true
			},
			'age' : {
				required: true
			},
			'army_no' : {
				required: true
			},
			'address' : {
				required: true
			},
			'telp_no' : {
				required: true
			},
			'state_id' : {
				required: true,
			},
			'geo_location' : {
				required: true
			}
		}
	});
	/* ***********************************************************************************************/
	
	$("#form-maklumat-perniagaan").validate({
        rules: {
			'registration_no' : {
				required: true
			},
			'business_name' : {
				required: true,
			},
			'business_address' : {
				required: true
			},
			'email_web' : {
				required: true
			},
			'business_activity[]' : {
				required: true
			},
			'main_product' : {
				required: true
			},
			'account_type' : {
				required: true
			},
			'financing_by' : {
				required: true,
			}
		},
		errorPlacement: function(error,element) {			
			if(element.attr("name") == "business_activity[]" && element.attr("type") == "checkbox"){
				error.insertBefore(element.parent());
			}else{
				error.insertAfter(element);
			}
		}
	});
	
	$("#form-classified").validate({	
		rules: {
			'title' :{
				required: true
			},
			'description':{
				required: true
			},
			'start_date':{
				required: true
			},
			'contact':{
				required: true
			},
			'contact_no':{
				required: true,
				number: true,
				maxlength: 12
			},
			'looking_for':{
				required: true
			},	
			'offering':{
				required: true
			},
        },
	});
});

