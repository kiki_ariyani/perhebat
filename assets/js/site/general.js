$(document).ready(function(){
	$(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd"});
	initPersonGenderAndReligion();
	calculate_age();
	status_atm_dom_handler();
	initGeoLocation();
	initDataTables();
	initConfirmDelete();
});

function disable_form(form_business_id, session_business_id, form, mode){
	mode = mode || "edit"; 
	if((form_business_id != session_business_id) || mode == "view"){
		form.find('input,textarea,select').attr('disabled', 'disabled');
		form.find('input[type=submit], .add-label, .delete-label').hide();
	}
}

function initPersonGenderAndReligion(){
	$("#person-name").blur(function(){
		$.post(base_url+'ajax/get_person_gender_and_religion', { "person_name": $(this).val() }, function(data){
			$("#person-gender").val(data.gender);
			(data.gender == 1 ? $("#person-gender").attr('disabled','disabled') : $("#person-gender").removeAttr('disabled','disabled'));
			$("#person-religion").val(data.religion);	
			(data.religion == 1 ? $("#person-religion").attr('disabled','disabled') : $("#person-religion").removeAttr('disabled','disabled'));			
		}, "json");
	});
}

function calculate_age(){
	$('#ic_no').keyup(function(){
		var ic_no = $(this).val().substr(0, 2),
		year_of_birth = "19"+ic_no,
		currentYear = (new Date).getFullYear(),
		age = currentYear - year_of_birth;
		if(ic_no != "" && $(this).val().length > 2){
			$('#age').val(age);
		}else{
			$("#age").val("");
		}
	});
}

function status_atm_dom_handler(){
	$('.status_atm').click(function(){
		if($(this).val() == 2){
			$('.status_pencen_wrap').show();
			$('.status_pencen').removeAttr('checked');
		}else{
			$('.status_pencen_wrap').hide();
		}
	});
}

function initGeoLocation(){
	$("#address-geo").blur(function(){
		$.post(base_url+"ajax/get_geo_location", { 'address': $(this).val() }, function(data) {
			if(data.status == true){
				$("#geo-location").val(data.latitude+","+data.longitude);
			}else{
				$("#geo-location").attr("placeholder",data.message);
			}
		},"json");
	});
}

/*delete confirmation*/
function confirm_delete() {
	if(confirm('Are you sure you want to delete?') == false) {
		return false;
	}
}

/*confim friend confirmation*/
function confirm_friend(){
	if(confirm('Are you sure confirm this user to your friend list?') == false){
		return false;
	}
}

/*ignore friend confirmation*/
function ignore_friend(){
	if(confirm('Are you sure ignore this friend request?') == false){
		return false;
	}
}

function initDataTables(){
	$(".dataTables").dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"iDisplayLength" : 20,
		"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]]
	});
}

function initConfirmDelete(){
	function confirm_delete() {
	if(confirm('Are you sure you want to delete?') == false) {
		return false;
	}
}
}