$(document).ready(function(){
	var notif_text = $('.notif_inbox').text();
	if(notif_text == ''){
		$('.notif_inbox').hide()
	}else{
		$('.notif_inbox').show()
	}
	get_new_message();
});

function get_new_message(){
	 $.ajax({
				type: "POST",
				url: base_url+"message/new_inbox",
				data: $.param({}),
				dataType:'json',
				success: function(response){
						if(response > 0){
							$('.notif_inbox').text(response).fadeIn();
						}
						timer = setTimeout("get_new_message()",5000);
					}
			});
}