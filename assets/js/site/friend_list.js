$(document).ready(function(){
	var notif_text = $('.notif_friend_request').text();
	if(notif_text == ''){
		$('.notif_friend_request').hide()
	}else{
		$('.notif_friend_request').show()
	}
	get_new_friend_request();
});

function get_new_friend_request(){
	 $.ajax({
				type: "POST",
				url: base_url+"friend_list/new_request",
				data: $.param({}),
				dataType:'json',
				success: function(response){
						if(response > 0){
							$('.notif_friend_request').text(response).fadeIn();
						}
						timer = setTimeout("get_new_friend_request()",5000);
					}
			});
}