$(document).ready(function(){
	$(function(){
		$(".role-cbox").change(function(){
			if($(this).is(':checked')){
				$(this).closest('td').find('input[type=hidden]').val(1);
			}else{
				$(this).closest('td').find('input[type=hidden]').val(0);
			}
		});
		
		$(".role-cbox.create").click(function(){
			if($(this).is(':checked')){
				$(this).closest('tr').find('input[type=checkbox]').not('.create').attr('checked','checked');
			}else{
				$(this).closest('tr').find('input[type=checkbox]').not('.create').removeAttr('checked');
			}
			$(".role-cbox").trigger('change');
		});
		
		$(".role-cbox.edit").click(function(){
			if($(this).is(':checked')){
				$(this).closest('tr').find('input[type=checkbox].view').attr('checked','checked');
			}else{
				if($(this).closest('tr').find('input[type=checkbox].view').is(':checked')){
					$(this).closest('tr').find('input[type=checkbox].create').removeAttr('checked');
				}
				$(this).closest('tr').find('input[type=checkbox].view').removeAttr('checked');
			}
			$(".role-cbox").trigger('change');
		});
		
		$(".role-cbox.view").click(function(){
			var menu_id = $(this).attr('menu_id');
			var parent_id = $(this).attr('parent_id');
			if($(this).is(':checked')){
				if($(this).attr('menu_id')){
					$(".panel").find('.role-cbox.view[parent_id='+menu_id+']').attr('checked','checked');
				}else{
					$(".panel").find('.role-cbox.view[menu_id='+parent_id+']').attr('checked','checked');
				}
			}else{
				if($(this).attr('menu_id')){
					$(".panel").find('.role-cbox.view[parent_id='+menu_id+']').removeAttr('checked','checked');
				}else{
					if($(".panel").find('.role-cbox.view[parent_id='+parent_id+']:checked').length == 0){
						$(".panel").find('.role-cbox.view[menu_id='+parent_id+']').removeAttr('checked','checked');
					}
				}
			}
			$(".role-cbox").trigger('change');
		});	
		
		$(".page_type").change(function(){
			var page_id = $(this).val();
			if(page_id == 0){
				$('.admin_page').show();
				$('.user_page').hide();
			}else{
				$('.admin_page').hide();
				$('.user_page').show();
			}
		});
	});
})