function drawChart(chartSWF, strXML, chartdiv,chartname,large_width) {
	var width = 800;
	if(large_width != ""){
		width = large_width;
	}
	var chart = new FusionCharts(chartSWF, chartname, width, "400", "0", "0"); 
	chart.setDataXML(strXML);
	chart.render(chart1div);
}

$(document).ready(function(){
	function example(param){
		var response = "<graph caption='State Report ' xAxisName='States' yAxisName='Number of Company' numberPrefix='' decimalPrecision='0' formatNumberScale='1'  ><set  value='104' name='Johor'  /><set  value='0' name='Kedah'  /><set  value='0' name='Kelantan'  /><set  value='129' name='Melaka'  /><set  value='0' name='Negeri Sembilan'  /><set  value='0' name='Pahang'  /><set  value='64' name='Perak'  /><set  value='0' name='Perlis'  /><set  value='113' name='Pulau Pinang'  /><set  value='79' name='Selangor'  /><set  value='64' name='Terengganu'  /><set  value='0' name='Sabah'  /><set  value='0' name='Serawak'  /><set  value='0' name='Wilayah Persekutuan Kuala Lumpur'  /><set  value='0' name='Wilayah Persekutuan Labuan'  /><set  value='0' name='Wilayah Persekutuan Putrajaya'  /></graph>";
		drawChart("/perniagaan/assets/swf/FCF_Column3D.swf", response, "wrap-cat"+param, "chart-cat"+param,"");
	}
	
	function state_report(param){
		$('#report-state').ajaxForm({
						//dataType : "xml",
						success : function(response){
							drawChart("/perniagaan/assets/swf/FCF_Column3D.swf", response, "wrap-cat"+param, "chart-cat"+param,"");
						}
				}).submit();
	}
	var state_num = 1;
	$('#state').click(function(){
		state_num++;
		state_report(state_num);
	});
	
	function status_report(param){
		$('#report-status').ajaxForm({
						//dataType : "xml",
						success : function(response){
							drawChart("/perniagaan/assets/swf/FCF_MSColumn3D.swf", response, "wrap-stat"+param, "chart-stat"+param,"");
						}
				}).submit();
	}
	var stat_num = 1;
	$('#status').click(function(){
		stat_num++;
		status_report(stat_num);
	});
	
	function finance_report(param){
		$('#report-finance').ajaxForm({
						//dataType : "xml",
						success : function(response){
							drawChart("/perniagaan/assets/swf/FCF_Pie3D.swf", response, "wrap-stat"+param, "chart-stat"+param,"");
						}
				}).submit();
	}
	var stat_num = 1;
	$('#finance').click(function(){
		stat_num++;
		finance_report(stat_num);
	});
	
	function category_report(param){
		$('#report-category').ajaxForm({
						//dataType : "xml",
						success : function(response){
							drawChart("/perniagaan/assets/swf/FCF_MSColumn3D.swf", response, "wrap-stat"+param, "chart-stat"+param, 930);
						}
				}).submit();
	}
	var stat_num = 1;
	$('#category').click(function(){
		stat_num++;
		category_report(stat_num);
	});
	
	function activity_report(param){
		$('#report-activity').ajaxForm({
						//dataType : "xml",
						success : function(response){
							drawChart("/perniagaan/assets/swf/FCF_MSColumn3D.swf", response, "wrap-stat"+param, "chart-stat"+param, 930);
						}
				}).submit();
	}
	var stat_num = 1;
	$('#activity').click(function(){
		stat_num++;
		activity_report(stat_num);
	});
	
	var content_type= $('#content-chart').attr('content_type');
	if(content_type == 'state'){
		$('.state-value').first().attr('checked','checked');
		$('.state-value').not(':first').attr('disabled','disabled');
		state_report();
		//category option
		$('.state-value').click(function(){
			var state_opt = $(this).val();
			if(state_opt != 0){
				if($(this).is(':checked')){
					$('.state-value').first().attr('disabled','disabled');
					$('.state-value').first().removeAttr('checked');
				}else{
					var selected_option_state = $('.state-value').not(':first').filter(':checked').length;
					if(selected_option_state == 0){
						$('.state-value').first().removeAttr('disabled');
					}
				}
			}else{
				if($(this).is(':checked')){
					$('.state-value').not(':first').attr('disabled','disabled');
				}else{
					$('.state-value').not(':first').removeAttr('disabled');
				}
			}
		});
	}else if(content_type == 'status'){
		$('.status-value').first().attr('checked','checked');
		$('.status-value').not(':first').attr('disabled','disabled');
		status_report();
		//status option
		$('.status-value').click(function(){
				var status_opt = $(this).val();
				if(status_opt != 0){
					if($(this).is(':checked')){
						$('.status-value').first().attr('disabled','disabled');
						$('.status-value').first().removeAttr('checked');
					}else{
						var selected_option_stat = $('.status-value').not(':first').filter(':checked').length;
						if(selected_option_stat == 0){
							$('.status-value').first().removeAttr('disabled');
						}
					}
				}else{
					if($(this).is(':checked')){
						$('.status-value').not(':first').attr('disabled','disabled');
					}else{
						$('.status-value').not(':first').removeAttr('disabled');
					}
				}
			});
	}else if(content_type == 'finance'){
		$('.status-value').first().attr('checked','checked');
		$('.status-value').not(':first').attr('disabled','disabled');
		finance_report();
		//status option
		$('.status-value').click(function(){
				var status_opt = $(this).val();
				if(status_opt != 0){
					if($(this).is(':checked')){
						$('.status-value').first().attr('disabled','disabled');
						$('.status-value').first().removeAttr('checked');
					}else{
						var selected_option_stat = $('.status-value').not(':first').filter(':checked').length;
						if(selected_option_stat == 0){
							$('.status-value').first().removeAttr('disabled');
						}
					}
				}else{
					if($(this).is(':checked')){
						$('.status-value').not(':first').attr('disabled','disabled');
					}else{
						$('.status-value').not(':first').removeAttr('disabled');
					}
				}
			});
	}else if(content_type == 'category'){
		$('.category-value').first().attr('checked','checked');
		$('.category-value').not(':first').attr('disabled','disabled');
		category_report();
		//category option
		$('.category-value').click(function(){
				var category_opt = $(this).val();
				if(category_opt != 0){
					if($(this).is(':checked')){
						$('.category-value').first().attr('disabled','disabled');
						$('.category-value').first().removeAttr('checked');
					}else{
						var selected_option_cat = $('.category-value').not(':first').filter(':checked').length;
						if(selected_option_cat == 0){
							$('.category-value').first().removeAttr('disabled');
						}
					}
				}else{
					if($(this).is(':checked')){
						$('.category-value').not(':first').attr('disabled','disabled');
					}else{
						$('.category-value').not(':first').removeAttr('disabled');
					}
				}
			});
	}else if(content_type == 'activity'){
		$('.activity-value').first().attr('checked','checked');
		$('.activity-value').not(':first').attr('disabled','disabled');
		activity_report();
		//activity option
		$('.activity-value').click(function(){
				var category_opt = $(this).val();
				if(category_opt != 0){
					if($(this).is(':checked')){
						$('.activity-value').first().attr('disabled','disabled');
						$('.activity-value').first().removeAttr('checked');
					}else{
						var selected_option_cat = $('.activity-value').not(':first').filter(':checked').length;
						if(selected_option_cat == 0){
							$('.activity-value').first().removeAttr('disabled');
						}
					}
				}else{
					if($(this).is(':checked')){
						$('.activity-value').not(':first').attr('disabled','disabled');
					}else{
						$('.activity-value').not(':first').removeAttr('disabled');
					}
				}
			});
	}else if(content_type == 'example'){
		example();
	}
});