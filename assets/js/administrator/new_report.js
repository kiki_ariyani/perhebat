
$(function () {
    $(document).ready(function() {
		checkbox_handler();
		$('#export-status').click(function(){
			submitForm("report-status",base_url+'administrator/reports/status_handler',base_url+'administrator/reports_export_excel/status_handler');
		});
		provider_combobox_handler();
		
		$('#export-state').click(function(){
			submitForm("report-state",base_url+'administrator/reports/state_handler',base_url+'administrator/reports_export_excel/state_handler');
		});
		
		$('#export-category').click(function(){
			submitForm("report-category",base_url+'administrator/reports/category_handler',base_url+'administrator/reports_export_excel/category_handler');
		});
		
		$('#export-work-status').click(function(){
			submitForm("report-work-status",base_url+'administrator/reports/work_status_handler',base_url+'administrator/reports_export_excel/work_status_handler');
		});
		
		$('#export-gender').click(function(){
			submitForm("report-gender",base_url+'administrator/reports/gender_handler',base_url+'administrator/reports_export_excel/gender_handler');
		});
		
		$('#export-age').click(function(){
			submitForm("report-age",base_url+'administrator/reports/age_handler',base_url+'administrator/reports_export_excel/age_handler');
		});
		
		$('#export-activity').click(function(){
			submitForm("report-activity",base_url+'administrator/reports/activity_handler',base_url+'administrator/reports_export_excel/activity_handler');
		});
		
		$('#export-trainer').click(function(){
			submitForm("report-trainer",base_url+'administrator/reports/provider_handler',base_url+'administrator/reports_export_excel/trainer_handler');
		});
		
		$('#export-finance').click(function(){
			submitForm("report-finance",base_url+'administrator/reports/finance_handler',base_url+'administrator/reports_export_excel/finance_handler');
		});

		$('#export-provider-amount').click(function(){
			submitForm("report-provider-amount",base_url+'administrator/reports/provider_amount_handler',base_url+'administrator/reports_export_excel/provider_amount_handler');
		});

		$('#export-course-all-amount').click(function(){
			submitForm("report-course-all-amount",base_url+'administrator/reports/course_all_amount_handler',base_url+'administrator/reports_export_excel/course_all_amount_handler');
		});
		
		$('#state').click(function(){
			state();
		});		
		$('#status').click(function(){
			status();
		});		
		$('#category').click(function(){
			category();
		});		
		$('#activity').click(function(){
			activity();
		});		
		$('#finance').click(function(){
			finance();
		});		
		$('#age').click(function(){
			age();
		});		
		$('#work-status').click(function(){
			work_status();
		});		
		$('#gender').click(function(){
			gender();
		});		
		$('#trainer').click(function(){
			trainer();
		});		
		$('#provider-amount').click(function(){
			provider_amount();
		});		
		$('#course-all-amount').click(function(){
			all_amount_pie_donut();
		});
		
		var content_type= $('#content-chart').attr('content_type');
		if(content_type == 'state'){
			state();
		}else if(content_type == 'status'){
			status();
		}else if(content_type == 'category'){
			category();
		}else if(content_type == 'activity'){
			activity();
		}else if(content_type == 'finance'){
			finance();
		}else if(content_type == 'age'){
			age();
		}else if(content_type == 'work_status'){
			work_status();
		}else if(content_type == 'gender'){
			gender();
		}else if(content_type == 'trainer'){
			trainer();
		}else if(content_type == 'course_all_amount'){
			all_amount_pie_donut();
		}
		
    });
});

function checkbox_handler(){
	$('.state-value').first().attr('checked','checked');
	$('.state-value').not(':first').attr('disabled','disabled');

	$('.state-value').click(function(){
		var state_opt = $(this).val();
		if(state_opt != 0){
			if($(this).is(':checked')){
				$('.state-value').first().attr('disabled','disabled');
				$('.state-value').first().removeAttr('checked');
			}else{
				var selected_option_state = $('.state-value').not(':first').filter(':checked').length;
				if(selected_option_state == 0){
					$('.state-value').first().removeAttr('disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$('.state-value').not(':first').attr('disabled','disabled');
			}else{
				$('.state-value').not(':first').removeAttr('disabled');
			}
		}
	});
}

function provider_combobox_handler(){
	$("#provider-combobox").change(function(){
		if($(this).val() != 0){
			$("#state-wrapper").show();
			$("#state-wrapper .state-value").removeAttr('checked');
			$('.state-value').first().attr('checked','checked');
			$('.state-value').first().removeAttr('disabled');
			$('.state-value').not(':first').attr('disabled','disabled');
		}else{
			$("#state-wrapper").hide();
		}
	});
}

/*Trainer Report*/
function trainer(){
$('#report-trainer').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-trainer","Provider",response,"provider");
					remove_highcharts_copyright();
				}
		}).submit();
}

function provider_amount(){
$('#report-provider-amount').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-provider-amount","Provider Amount (RM)",response,"provider_amount");
					remove_highcharts_copyright();
				}
		}).submit();
}

function all_amount_pie_donut(){
$('#report-course-all-amount').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_pie_donut_type("container-course-all-amount","Provider Amount",response,"count_amount");
					remove_highcharts_copyright();
				}
		}).submit();
}

function draw_pie_donut_type(container,title,result,type){
	//load dialog
	load_dialog();
	
	var colors 		= Highcharts.getOptions().colors,
		categories 	= result.categories,
		name 		= 'Courses Amount',
		data		= result.data;		
    
        var browserData 	= [];
        var versionsData 	= [];
        for (var i = 0; i < data.length; i++) {
            browserData.push({
                name	: categories[i],
                y		: data[i].y,
                color	: colors[i],
				id		: data[i].id,
				seri	: "",
            });
			
            for (var j = 0; j < data[i].drilldown.data.length; j++) {
                var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
                versionsData.push({
                    name	: data[i].drilldown.categories[j],
                    y		: data[i].drilldown.data[j],
                    color	: Highcharts.Color(colors[i]).brighten(brightness).get(),
					id		: data[i].drilldown.id,
					seri	: data[i].drilldown.seri[j],
                });
            }
        }
    
        // Create the chart
		chart = new Highcharts.Chart({
            chart: {
				renderTo: container,
                type: 'pie'
            },
            title: {
                text: title
            },
            yAxis: {
                title: {
                    text: 'Courses Amount'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%'],
					point:{
						events:{
							click: function(){
								var state_id = this.id;
								$.get(base_url+"administrator/reports/get_person/"+this.id+"/"+type+"/"+this.seri,function(response){
									var table = $('.dataTables_custom').dataTable();
									table.fnClearTable();
									table.fnDestroy();
									
									var content = "", no=1;
									for(i=0;i<response.length;i++){
										content += "<tr><td>"+no+"</td><td>"+response[i].business_name+"</td><td>"+response[i].person_name+"</td><td>"+response[i].category_name+"</td><td>"+response[i].state_name+"</td></tr>";
									no++;}
									
									$('#report-dialog').find('table').css('width','100%');
									$('#people-dialog').append(content);
									$(".dataTables_custom").dataTable({
										"bJQueryUI": true,
										"sPaginationType": "full_numbers",
										"iDisplayLength": 20,
										"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
									});
									$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
									$('.dataTables_wrapper').addClass('delete-before');
									$("#report-dialog").dialog('open');
								},"json");
							}
						}
					} 
                }
            },
            tooltip: {
        	    valueSuffix: ' RM'
            },
            series: [{
                name: 'Amount',
                data: browserData,
                size: '60%',
                dataLabels: {
                    formatter: function() {
                        return this.y > 5 ? this.point.name : null;
                    },
                    color: 'white',
                    distance: -30
                }
				}, {
					name: 'Amount',
					data: versionsData,
					size: '80%',
					innerSize: '60%',
					dataLabels: {
						style : {width : "400px"},
						formatter: function() {
							return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +' RM'  : null;
						}
					}
				}]
        });
}

/*Gender Report*/
function gender(){
$('#report-gender').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-gender","Gender",response,"gender");
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Work Status Report*/
function work_status(){
$('#report-work-status').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-work-status","Work Status",response,"work_status");
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Age Report*/
function age(){
$('#report-age').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-age","Age",response,"age");
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Finance report*/
function finance(){
$('#report-finance').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_pie_type("container-finance","Pembiayaan",response,"finance");
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Activity Report*/
function activity(){
$('#report-activity').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-activity","Activity",response);
					remove_highcharts_copyright();
				}
		}).submit();
}

function draw_pie_type(container,title,result,type){
	//load dialog
	load_dialog();
	
 chart = new Highcharts.Chart({
		chart: {
			renderTo: container,
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
		},
		title: {
			text: title
		},
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y} Usaha, {point.percentage}%</b>',
			percentageDecimals: 1
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					color: '#000000',
					connectorColor: '#000000',
					formatter: function() {
						return '<b>'+ this.point.name +'</b>: '+this.y+' Usaha, '+ Math.round(this.percentage) +' %';
					}
				},
				point:{
					events:{
						click: function(){
							var state_id = this.id;
							$.get(base_url+"administrator/reports/get_person/"+this.id+"/"+type+"/"+this.seri,function(response){
								var table = $('.dataTables_custom').dataTable();
								table.fnClearTable();
								table.fnDestroy();
								
								var content = "", no=1;
								for(i=0;i<response.length;i++){
									content += "<tr><td>"+no+"</td><td>"+response[i].business_name+"</td><td>"+response[i].person_name+"</td><td>"+response[i].category_name+"</td><td>"+response[i].state_name+"</td></tr>";
								no++;}
								
								$('#report-dialog').find('table').css('width','100%');
								$('#people-dialog').append(content);
								$(".dataTables_custom").dataTable({
									"bJQueryUI": true,
									"sPaginationType": "full_numbers",
									"iDisplayLength": 20,
									"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
								});
								$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
								$('.dataTables_wrapper').addClass('delete-before');
								$("#report-dialog").dialog('open');
							},"json");
						}
					}
				} 
			}
		},
		series: [{
			type: 'pie',
			name: 'Jumlah Usaha',
			data: result
		}]
	});
}

/*Activity Report*/
function activity(){
$('#report-activity').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-activity","Activity",response,"activity");
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Category Report*/
function category(){
$('#report-category').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-category","Category",response,"category");
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Status Report*/
function status(){
$('#report-status').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container-status","Status",response,"status");
					remove_highcharts_copyright();
				}
		}).submit();
}

function draw_category_type(container,title,result,type){
	//load dialog
	load_dialog();
		
	 chart = new Highcharts.Chart({
		chart: {
			renderTo: container,
			type: 'column'
		},
		title: {
			text: title
		},
		xAxis: {
			categories: result.title,
			title: {
				text: 'State'
			},
			labels: {
				style: {
					"font-size" : "11px"
				}
			},
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Number of Company'
			},
		},
		exporting: {
			enabled: true,
		},
		legend: {
			layout: 'horizontal',
			backgroundColor: '#FFFFFF',
			align: 'left',
			verticalAlign: 'bottom',
			x: 30,
			y: 14,
			floating: true,
			shadow: true,
		},
		/*tooltip: {
			formatter: function() {
				return ''+
					this.x +': '+ this.y;
			}
		},*/
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0,
				dataLabels: {
					enabled: true,
					color: '#000',
					formatter: function() {
						return this.y;
					}
				},
				point:{
					events:{
						click: function(){
							var state_id = this.id;
							$.get(base_url+"administrator/reports/get_person/"+this.id+"/"+type+"/"+this.seri,function(response){
								var table = $('.dataTables_custom').dataTable();
								table.fnClearTable();
								table.fnDestroy();
								
								var content = "", no=1;
								for(i=0;i<response.length;i++){
									content += "<tr><td>"+no+"</td><td>"+response[i].business_name+"</td><td>"+response[i].person_name+"</td><td>"+response[i].category_name+"</td><td>"+response[i].state_name+"</td></tr>";
								no++;}
								
								$('#report-dialog').find('table').css('width','100%');
								$('#people-dialog').append(content);
								$(".dataTables_custom").dataTable({
									"bJQueryUI": true,
									"sPaginationType": "full_numbers",
									"iDisplayLength": 20,
									"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
								});
								$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
							//	$('#report-dialog').dialog('option', 'title', 'Detail for '+response.detail_title);
								$('.dataTables_wrapper').addClass('delete-before');
								$("#report-dialog").dialog('open');
							},"json");
						}
					}
				} 
			}
		},
			series: result.category
	});
}

/*State Report*/
function state(){
$('#report-state').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_state(response);
					remove_highcharts_copyright();
				}
		}).submit();
}

function draw_state(result){
	var chart;
	var colors = Highcharts.getOptions().colors,
		categories = result.title,
		name = "State",
		data = result.value;
		
		/*for(var i=0;i<result.value.length;i++){
			data.push({y: result.value[i]});
		}*/

	function setChart(name, categories, data, color) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			name: name,
			data: data,
			color: color
		}, false);
		chart.redraw();
	}
	
	//load dialog
	load_dialog();

	chart = new Highcharts.Chart({
		chart: {
			renderTo: 'container_state',
			type: 'column'
		},
		title: {
			text: 'State Report'
		},
		xAxis: {
			categories: categories,
			title: {
				text: 'States'
			}
		},
		yAxis: {
			title: {
				text: 'Number of Company'
			}
		},
		tooltip: {
			formatter: function() {
				var point = this.point,
					s = this.x +':<b>'+ this.y +'</b><br/>';
				return s;
			}
		},
		legend:{
			enabled :false,
		},
		plotOptions: {
			column: {
				colorByPoint: true,
				dataLabels: {
					enabled: true,
					color: '#000',
					formatter: function() {
						return this.y;
					}
				},
				point:{
					events:{
						click: function(){
							$('#people-dialog').html("");
							var state_id = this.id;
						
							$.get(base_url+"administrator/reports/get_person/"+this.id+"/state",function(response){
								var table = $('.dataTables_custom').dataTable();
								table.fnClearTable();
								table.fnDestroy();
								
								var content = "", no=1;
								for(i=0;i<response.length;i++){
									content += "<tr><td>"+no+"</td><td>"+response[i].business_name+"</td><td>"+response[i].name+"</td><td>"+response[i].category_name+"</td><td>"+response[i].state_name+"</td></tr>";
								no++;}
								
								$('#report-dialog').find('table').css('width','100%');
								$('#people-dialog').append(content);
								$(".dataTables_custom").dataTable({
									"bJQueryUI": true,
									"sPaginationType": "full_numbers",
									"iDisplayLength": 20,
									"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
								});
								$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
								$('.dataTables_wrapper').addClass('delete-before');
								$("#report-dialog").dialog('open');
								
							},"json");
						}
					}
				} 
			}			
		},
		series: [{
			name: name,
			data: data,
			color: 'white'
		}],
		exporting: {
			enabled: true
		}
	});
}
function remove_highcharts_copyright(){
	$('tspan, .highcharts-container span').each(function(i, item){ 
		if ($(this).text() === 'Highcharts.com')
			$(this).remove();
	});
}

function submitForm(form_id,url,url_export){
       document.getElementById(form_id).action = url_export;
       document.getElementById(form_id).submit();
       document.getElementById(form_id).action = url;
}

function load_dialog(){
	$("#report-dialog").dialog({
		  title: "Detail",
		  autoOpen	: false,
		  resizable: false,
		  width:900,
		  height:500,
		  modal: false,
		  //draggable : false,
		  //position: "top",
		   position: [300,20],
		  buttons: {
			"OK" : function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}