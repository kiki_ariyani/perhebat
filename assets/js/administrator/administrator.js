/*delete confirmation*/
function confirm_delete() {
	if(confirm('Are you sure you want to delete?') == false) {
		return false;
	}
}

$(document).ready(function(){
	$(".dataTables").dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
	});
	
	$(".dataTables_custom").dataTable({
		"bJQueryUI": true,
		"bSort": false,
		"bPaginate": false,
		"bInfo": false,
		
	});
	
	$(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd"});
	
	/*ADVANCED SEARCH FILTER FORM*/
	$(".activity-option").click(function(){
		if($(this).val() != 0){
			$(this).siblings('.activity-option.all').removeAttr('checked');
		}else{
			$(this).siblings('.activity-option').removeAttr('checked');
		}
	});
	show_detail_courses();
});

function disable_form(form){
   form.find('input,textarea,select').attr('disabled', 'disabled');
}

function show_detail_courses(){
	$(".detail_courses_wrap").dialog({
	  title: "Detail Courses",
	  autoOpen	: false,
	  resizable: false,
	  width:600,
	  height:500,
	  modal: false,
	  buttons: {
		"OK" : function() {
		  $( this ).dialog( "close" );
		}
	  }
	});
	
	$('.detail_courses span').click(function(){
		var courses_id = $(this).parent().find('.courses_id').val();
		$.post(base_url+'administrator/courses/get_detail/'+courses_id,"",function(response){
			var content = '<div class="field"><div class="label-field"><label><b>Courses Name</b></label><label class="italic"><b>Nama Kursus</b></label></div><div class="value-field">'+response.name+'</div></div><br/><div class="field"><div class="label-field"><label><b>Trainer</b></label><label class="italic"><b>Penceramah</b></label></div><div class="value-field">'+response.trainer_name+'</div></div><br/><div class="field"><div class="label-field"><label><b>Description</b></label><label class="italic"><b>Huraian Khusus</b></label></div><div class="value-field">'+response.description+'</div></div><br/><div class="field"><div class="label-field"><label><b>Objective</b></label><label class="italic"><b>Jangkaan Outcome</b></label></div><div class="value-field">'+response.objective+'</div></div>';
			$('.detail_courses_wrap').html(content);
			$(".detail_courses_wrap").dialog('open');
			 var styles = {
				"margin-left" : "-8px",
				"margin-top" : "-8px"
			  };
			$(".ui-dialog").find('.ui-icon').css(styles);
		},"json");
	});
}