$(document).ready(function() {
	//User Form Validation
	$("#form-user").validate({
        rules: {
			'email' : {
				required: true
			},
			'password' : {
				required: true,
			},
			'retype_password' : {
				required: true,
				equalTo : "#password",
			},
			'role_id' : {
				required: true
			},
		}
	});
});

