$(function(){
	tinyMCE.init({
		mode : "specific_textareas",
		editor_selector : "mce_editor",
		theme : "advanced",
		plugins : "imageupload,autolink,lists,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,|,undo,redo,|,formatselect,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,anchor,image,imageupload,cleanup,code,|,forecolor,backcolor",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		width : "800",
		maxwidth: "100%",
		height: "400",
		paste_retain_style_properties :"margin, padding, width, height, font-size, font-weight, font-family, color, text-align, ul, ol, li, text-decoration, border, background, float, display",
		relative_urls : false
	});
	
	tinyMCE.init({
		mode : "specific_textareas",
		editor_selector : "mce_editor_small",
		theme : "advanced",
		plugins : "imageupload,autolink,lists,spellchecker,pagebreak,style,layer,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,|,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "undo,redo,|,bullist,numlist,|,outdent,indent,|,link,unlink,anchor,image,imageupload,cleanup,code",
		theme_advanced_buttons3	: "forecolor,backcolor,|,sub,sup,|,charmap",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		maxwidth: "100",
		height: "250",
		paste_retain_style_properties : "margin, padding, width, height, font-size, font-weight, font-family, color, text-align, ul, ol, li, text-decoration, border, background, float, display",
		relative_urls : false
	});
	
	tinyMCE.init({
		mode : "specific_textareas",
		editor_selector : "mce_editor_small_readonly",
		theme : "advanced",
		readonly: true,
	});
	
	tinyMCE.init({
		mode : "specific_textareas",
		editor_selector : "mce_editor_readonly",
		theme : "advanced",
		readonly: true,
		width : "800",
		height : "400",
	});
});