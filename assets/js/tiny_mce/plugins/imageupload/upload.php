<?php
$URI = explode("/", $_SERVER['REQUEST_URI']);
if (isset($_FILES["image"]) && is_uploaded_file($_FILES["image"]["tmp_name"])) {
  //@todo Change base_dir!
  $base_dir = $_SERVER['DOCUMENT_ROOT'] ."/". $URI[1] . '/img/content/';
  //@todo Change image location and naming (if needed)
  $image = $_FILES["image"]["name"];
  move_uploaded_file($_FILES["image"]["tmp_name"], $base_dir . $image);
  //die;
?>
<input type="text" id="src" name="src" />
<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
<script>
  var ImageDialog = {
    init : function(ed) {
      ed.execCommand('mceInsertContent', false, 
        tinyMCEPopup.editor.dom.createHTML('img', {
          src : 'http://mynef.com/assets/img/content/<?=$image?>'
        })
      );
      
      tinyMCEPopup.editor.execCommand('mceRepaint');
      tinyMCEPopup.editor.focus();
      tinyMCEPopup.close();
    }
  };
  tinyMCEPopup.onInit.add(ImageDialog.init, ImageDialog);
</script>
<?php  } else {?>
<form name="iform" action="" method="post" enctype="multipart/form-data">
  <input id="file" accept="image/*" type="file" name="image" onchange="this.parentElement.submit()" />
</form>
<?php }?>